\subsection{Softwareentwicklung}

In keiner der in Kapitel \ref{kap:verwandte_anwendungen} besprochenen Veröffentlichungen wurde diskutiert, \textit{wie} die jeweilige softwaretechnische Umsetzung auszusehen hat. Das ist keine Schande, denn diese Thematik steht eben oft nicht im Fokus der Forschung. Entwickler nutzen am liebsten einfach die Technologie, die ihnen bekannt ist und nicht die, die ihnen unbekannt, aber gut geeignet für die angedachte Aufgabe ist, was eine aktuelle Studie bestätigt \cite{sync:2019_lieblingssprache}. Ein Fokus dieser Arbeit, passend zur Fragestellung, \textit{wie} das System zu implementieren sei, soll auch die Perspektive aus der Softwareentwicklung sein. Da Sevilimedus Thesis bereits projektplanerische Ansichten in Betracht zieht, ergänzen sich diese Arbeiten in dieser Hinsicht passgenau, da hier aus technischer Perspektive die Softwareentwicklung angegangen wird.\\

\textbf{Wie sollte die Implementierung aussehen?}\\

Um zu rekapitulieren: Die in Kapitel \ref{kap:zielsetzung} dafür gesetzten Rahmenbedingungen in Bezug auf die Gesichtserkennung und (Netzwerk-)Infrastruktur lauten:

\begin{itemize}
	\item flexibel,
	\item robust und
	\item performant sowie
	\item unter Rücksichtnahme gängiger Praxis aus der IT-Sicherheit.
\end{itemize}

\textit{Flexibel} soll bedeuten, dass a) die Software bzw. der Quelltext sich zur Weiterentwicklung bzw. Anpassung eignet; Wiederverwendbarkeit spielt folglich auch eine Rolle, insofern dass b) die Gesichtserkennung (einfach) austauschbar ist (durch z.B. neuere / bessere Modelle) und c) ein praktikables Maß an Plattformunabhängigkeit besteht.

Unter \textit{Robustheit} wird in der Softwareentwicklung verstanden, bei fehlerhaften Eingaben (z.B. des Nutzers) nicht 'abzustürzen', sondern stattdessen verständliche Fehlermeldungen zu liefern und die Nichtverfügbarkeit der Anwendung zu minimieren \cite{man1:softwareentwicklung_kompass}.

Eine \textit{performante} Implementierung soll sicherstellen, dass die Software auf dem Zielsystem(en) lauffähig und \textit{responsive} ist. Algorithmen sollen nicht verschwenderisch mit Rechenressourcen umgehen.

Was gängige Praxis im Bereich \textit{IT-Sicherheit} ist, sei in Kapitel \ref{kap:sicherheit} zu erörtern.

Diese Rekapitulation soll als Erinnerung zur Rücksichtnahme beim Lesen dieses Kapitels gelten. Erst im späteren Methodikteil finden sich Details zur Umsetzung.

\subsubsection{Frameworks}
\label{kap:frameworks}

\begin{sidewaystable}[]
	\footnotesize
	\begin{tabular}{|l|l|l|l|}
		\hline
		\textbf{Name} & \textbf{\begin{tabular}[c]{@{}l@{}}Sprache der\\ Implementierung\end{tabular}} & \textbf{API-Sprache} & \textbf{Internetpräsenz} \\ \hline
		DLib \cite{man1:dlib09} & C++ & C++, Python & \url{http://dlib.net/} \\ \hline
		OpenCV \cite{fw:opencv_library} & C, C++ & C++, Python & \url{https://opencv.org/} \\ \hline
		caffe \cite{fw:jia2014caffe} & C++ & C++, Python & \url{http://caffe.berkeleyvision.org/} \\ \hline
		Torch \cite{fw:torch}& C & Lua & \url{http://torch.ch/} \\ \hline
		TensorFlow \cite{fw:tensorflow2015-whitepaper} & C++, Python & Python, C & \url{https://www.tensorflow.org/} \\ \hline
		MXNet \cite{fw:mxnet} & C++, Python, \dots & C++, Python, \dots & \url{http://mxnet.apache.org/} \\ \hline
		Eclipse Deeplearning4j & Java, C++ & Java & \url{https://deeplearning4j.org/} \\ \hline
		MATLAB & MATLAB & MATLAB & \url{https://www.mathworks.com/products/matlab.html} \\ \hline
		Microsoft CognitiveToolkit & C++ & .NET Sprachen & \url{https://www.microsoft.com/en-us/cognitive-toolkit/} \\ \hline
		OpenNN & C++ & C++, Python & \url{http://www.opennn.net/} \\ \hline
		scikit-learn \cite{fw:scikit-learn} & Python, C++, C & Python & \url{http://scikit-learn.org/} \\ \hline
		Microsoft ML NET  & C\#, C++ & .NET Sprachen & \url{https://dotnet.microsoft.com/apps/machinelearning-ai/ml-dotnet} \\ \hline
		Keras & Python & Python & \url{https://keras.io/} \\ \hline
		Theano \cite{fw:theano} & Python, C & Python & \url{http://deeplearning.net/software/theano/} \\ \hline
	\end{tabular}
	\caption{Übersicht über das Rechercheergebnis nach \ac{KNN}-Frameworks.}
	\label{tab:frameworks}
\end{sidewaystable}

Warum folgt das Kapitel über Programmiersprachen (Kapitel \ref{kap:programmiersprachen}) auf Kapitel über Frameworks und nicht umgekehrt? Sind Programmiersprachen nicht notwendig, um daraus Frameworks aufzubauen?

Doch, das ist korrekt, aber \ac{KI}-Frameworks nutzen nur eine Teilmenge aller existierender Programmiersprachen, weshalb es Sinn macht, ausschließlich über die Sprachen zu reden, die auf Grund des Vorhandenseins eines Frameworks auch für das Projekt einsetzbar sind.

Wenn man Quelltextwiederverwendung maximieren möchte, sollte sogar vor der Framework-Auswahl ein bestimmtes neuronales Netz festgelegt werden. Liegt der Quelltext für die Anwendung des Netzes öffentlich bereit, kann dieser genutzt werden, wodurch man automatisch auch ein Framework gewählt hätte, mit Hilfe dessen das Netz implementiert worden ist. Das Framework (und damit der Quelltext) nutzt eine bestimmte Programmiersprache, womit folglich auch diese Wahl entschieden wäre. Die Entscheidungsfolge nach dieser Methode zusammengefasst:

Neuronales Netz \textrightarrow Framework \textrightarrow Programmiersprache.\\

Welche Frameworks stehen für die Implementierung neuronaler Netze zur Verfügung?

Eine zusammenfassende Übersicht zeigt Tabelle \ref{tab:frameworks}, die das Ergebnis der Recherchetätigkeit darstellt\footnote{Es wurde sich bemüht, jeweilige Veröffentlichungen in Textform der Frameworks zu zitieren, welche nicht in allen Fällen gegeben sind; sollten diese übersehen worden sein, sei um Nachsicht gebeten.} und auf Recherchen von 2017 \cite{sonstiges:framework_survey} und 2018 \cite{sonstiges:frameworks_open_source} aufbaut.

Die Bibliotheken unterscheiden sich voneinander nicht nur in der Programmiersprache (bzw. den Programmiersprachen) der Implementierung und \ac{API}, sondern auch in Sachen Funktionalität und Performanz.

\acp{CNN} werden von praktisch allen gezeigten Frameworks unterstützt, für \acp{RNN} sieht es etwas anders aus: Bei DLib und OpenNN fehlt hier noch die Unterstützung, wobei zu beachten ist, dass diese Information schnell veralten kann, da sich genannte Programmbibliotheken in aktiver Entwicklung befinden, für DLib beispielsweise wurden 2018 alleine acht größere Aktualisierungen veröffentlicht (Versionen 19.9 bis 19.16, jeweils einschließlich), indes sich die Mehrzahl der Neuerungen auf den Ausbau der Maschinenlernfunktionen und der Python-Schnittstelle bezieht.\footnote{\label{f:dlib_release_notes}Vgl. Sektion \textit{Release Notes} auf DLibs offizieller Webseite.} Es sollten sich demgemäß alle \acp{CNN} in allen Frameworks realisieren lassen, während rekurrente neuronale Netze in allen Bibliotheken bis auf DLib und OpenNN momentan verwirklicht werden können. Abgesehen davon, könnten auch unübliche Aktivierungsfunktionen oder Loss-Layer die Auswahl auf eine Teilmenge der aufgelisteten Frameworks beschränken: Hier geschieht die Entwicklung jedoch so rasant, dass ein Vergleich nur kurze Zeit gültig wäre, weshalb er sich kaum anbietet.\footnotemark[\value{footnote}]

Da viele neuronale Netze so komplexe Strukturen (sowie große Mengen an Lerndaten) haben, dass ihr Anlernprozess selbst auf moderner Hardware eine lange Zeit (wenigstens Stunden \cite{face_reco:2017_thesis_cnn_optimization}) in Anspruch nimmt, bemühen sich die Autoren der Bibliotheken, Parallelisierung für aktuelle Systeme auf \ac{CPU} und / oder \ac{GPU} in den Anlernprozess zu integrieren. Das zeigt sich an der Unterstützung für entsprechende Frameworks: Für \ac{CPU}-seitiges Multi-Threading wird OpenMP genutzt \cite{fw:openmp}, das von allen Frameworks außer TensorFlow unterstützt wird, sowie \ac{BLAS} \cite{fw:blas}, eine stark optimierte Programmbibliothek für lineare Algebra, von der beispielsweise die OpenBLAS-Implementierung Multi-Threading-fähig ist.

Während 2010 OpenCL noch $16\%$ bis $67\%$ langsamer in Benchmarks abschnitt als CUDA \cite{fw:benchmark_cuda_opencl_2010}, welches übrigens keine Abkürzung, sondern ein eingetragener Name der Firma NVidia ist, konnte OpenCL zwischenzeitlich stark aufholen: Eine Veröffentlichung von 2017 zitiert eine kontinuierliche Angleichung in der Leistung der beiden Bibliotheken, nach der schließlich OpenCL nur noch unwesentlich langsamer als CUDA abschneidet \cite{fw:benchmark_cuda_opencl_2017}. Für Anwender ist zu beachten, dass CUDA nur auf Systemen mit Grafikprozessoren der Firma NVidia lauffähig ist sowie eine Registrierung zum Akquirieren der Entwicklungs-Software vorausgesetzt wird, während für das von der Non-Profit-Organisation Khronos Group herausgegebene OpenCL kein Vendor-Lock-in besteht und die Software gleichzeitig frei erhältlich ist. Der derzeit noch verbleibende Nachteil von OpenCL ist, dass es von kaum einem Deep-Learning-Framework unterstützt wird. Manche, wie z.B. DLib, stellen eine Implementierung in OpenCL gar nicht erst in Aussicht,\footnote{Vgl. eigene Angaben des Autors auf  \url{https://github.com/davisking/dlib/issues/652}.} andere experimentieren damit.\footnote{Vgl. die experimentelle OpenCL-Implementierung von Caffe: erreichbar unter \url{https://github.com/amd/OpenCL-caffe}.} Ein Grund für die schleppende Adaption von OpenCL mag wohl dessen höherer Aufwand für die Integration wegen seiner komplexeren Schnittstelle sein, was Programmierer deutlich mehr Arbeit kostet \cite{fw:benchmark_cuda_opencl_2017}.

Abgesehen von der erwähnten Auslagerung auf mehrere Recheneinheiten hat \ac{BLAS} enorme Optimierungen für einige Rechenoperationen neuronaler Netze implementiert, d.h. selbst nicht entwickelt, aber theoretische Forschungsergebnisse praktisch umgesetzt: Beispielsweise kann die Komplexität des Algorithmus zur Matrixmultiplikation von $O(n^3)$ (bis 1969) über mehrere Veröffentlichungen hinweg bis auf $O(n^{2,3728639})$ im Jahr 2014 gesenkt werden \cite{man1:matrix_product_2014}. Auch prinzipielle Schwächen von Fließkommazahlen adressiert \ac{BLAS} und nutzt zur Verhinderung von Auslöschung (z.B. beim Skalarprodukt, wo 'viele' Produkte - und damit deren Fehler - sich aufsummieren) den Summenalgorithmus von Kahan \cite{man1:kahan_algorithmus}.

Der Funktionsumfang, i.e. die Unterstützung aller Layer-Arten des zu implementierenden Netzes, von Frameworks ist vermutlich in vielen Projekten auswahlentscheidend. Eine nette Eigenschaft einiger Bibliotheken sind eingebaute Interoperabilitäten, die es enorm erleichtern können, bestimmte Funktionalitäten zu kombinieren. Hier seien nur einige Beispiele als Inspiration genannt, bei Bedarf muss im individuellen Fall selbstverständlich selbst geschaut werden, welche Interoperabilitäten wünschenswert wären:

\begin{itemize}
	\item DLib kann von OpenCVs Bildformat (\texttt{cv::Mat}) in C++ in dessen eigenes hin- und herkonvertieren \cite{fw:dlib_opencv_comparasion}.
	\item OpenCV, eigentlich nicht als Deep-Learning-Framework konzipiert, sondern eine Bibliothek zum Umgang mit Bilddaten, kann Modelle neuronaler Netze aus Caffe, TensorFlow, Torch und Theano importieren und expandiert seit einiger Zeit selbst in den Bereich Maschinenlernen \cite{buecher:2017_learning_opencv_3}.
\end{itemize}


\subsubsection{Programmiersprachen} 
\label{kap:programmiersprachen}

Wie Tabelle \ref{tab:frameworks} zeigt, sind in den Frameworks die Programmiersprachen Python und C++ zahlenmäßig am stärksten vertreten. Das gilt sowohl für Implementierungen als auch \acp{API}. Woran liegt das? Anhand ihrer hohen Nutzung lässt sich schließen, dass beide Sprachen ausgezeichnet für Maschinenlernanwendungen geeignet sind. Das Grundgerüst (Backend) der hier behandelten Frameworks besteht häufig aus C++, während die Anwenderschnittstelle (gemeint ist die \ac{API}) oft beides anbietet: C++ und Python. Da diese beiden Sprachen so stark dominieren, sollen mögliche Gründe dafür erörtert und gleichzeitig erschlossen werden, worin deren Stärken liegen, sodass diese für die Nutzung ausgeschöpft werden können.

C++ ist keine einfache Programmiersprache. Studenten empfinden Python im Vergleich einfacher zu erlernen, Syntax und Semantik leichter verständlich \cite{lang_libs:comparasion_python_vs_cpp_learn}. Auch sind Programme in Python in der Regel etwas schneller fertig geschrieben und kürzer vom Quelltext her als derselbe Algorithmus in C++ (oder z.B. Java) \autocite{lang_libs:comparasion_7_programming_languages,unsortiert:2018_iot_programming}. Das liegt vielleicht daran, dass C++

\begin{itemize}
	\item sowohl systemnahe (es ist beispielsweise möglich, direkt im C++ Quelltext Assemblerbefehle einzubauen)
	\item als auch hoch abstrahierte Programmierung ermöglicht,
	\item das sogenannte \ac{TMP} beinhaltet, welches eine in sich abgeschlossene, eingebettete, Touring-vollständige Programmiersprache innerhalb C++ darstellt,
	\item Präprozessorbefehle von C geerbt hat,
	\item eine große zu C abwärtskompatible Befehlsstruktur erhalten hat,
	\item wie z.B. manuelle Speicherverwaltung (mit \texttt{new} kann dynamischer Speicher allokiert und mittels \texttt{delete} deallokiert werden, auch \texttt{malloc} sowie \texttt{free} aus C sind in C++ vorhanden) unterstützt (jedoch nicht erzwingt, sogar von dessen Nutzung davon abgeraten wird),
    \item \ac{RAII} anstatt von Garbage Collection verwendet, welche in vielen kontemporären Sprachen wie Python, Java oder C\# üblich ist und
	\item der vollständige, aktuelle Standard C++17 aus weit über 1500 Seiten für Laien wohl unverständlich formuliertem Text besteht \autocite{buecher:2017_clean_cpp_book,man1:isocpp17}.
\end{itemize}

Trotz höherer Komplexität schreiben C++-Programmierer nicht automatisch Quelltext mit mehr Bugs oder Issues als in anderen Sprachen \cite{lang_libs:comparasion_scripting_compiled_fu}.

%% Eigenschaften
In ihrer aktuellen Version unterstützen sowohl Python als auch C++ objektorientierte sowie funktionale Programmierung. Die zwei vielleicht wichtigsten Unterschiede der beiden Sprachen ist die Typisierung und Ausführung: C++ ist eine statisch typisierte Sprache, während Python dynamische Typisierung verwendet. In C++ müssen alle Variablen mit einem unveränderlichen Datentypen versehen werden, dem sie bis an ihr Lebensende angehören, in Python kann dieselbe Variable beliebig ihren Datentypen wechseln \cite{buecher:2016_programming_languages_concepts}. Beides hat gravierende Auswirkungen, Vor- und Nachteile, welche vielleicht gern unter Programmieren mehr subjektiv als objektiv diskutiert werden und daher leider in dieser Arbeit keinen Platz finden.

Bei kompilierten Sprachen wird der Quelltext direkt in Maschinenkode (Assembler) übersetzt und unmittelbar vom Betriebssystem ausgeführt. Interpretierte Sprachen werden nicht kompiliert, sondern der Quelltext wird von einem Interpreter, der als Mittler zwischen Kode und Betriebssystem arbeitet, Zeile für Zeile ausgeführt \cite{buecher:2016_programming_languages_concepts}. Ursprünglich war dies von Pythons Autoren als Werkzeug gedacht, für Plattformunabhängigkeit zu sorgen. Auf Systemen, für die der Python-Interpreter zur Verfügung steht, hat das auch geklappt, leider leiden interpretierte Sprachen generell unter Problemen mit der Performanz, was auch Python betrifft, welches um Größenordnungen langsamer ist als kompilierter Kode. Einer der Hauptgründe dafür bei Python ist ein \ac{GIL},\footnote{Vgl. dazu auch \textit{GlobalInterpreterLock} im offiziellen Python-Wiki: \url{https://wiki.python.org/moin/GlobalInterpreterLock}.} eine Art globale \ac{Mutex}, der dazu führt, dass der Interpreter praktisch nur auf einem \ac{CPU}-Kern laufen kann \autocite{lang_libs:comparasion_7_programming_languages,lang_libs:comparasion_scripting_compiled_fu}. 

Der anfängliche Trend, beim Design von Programmiersprachen immer weiter zu abstrahieren, findet seine Grenzen aufgezeigt im sogenannten \textit{Law of Leaky Abstractions}, welches 2004 vom Softwareentwickler Joel Spolsky verfasst wurde. Es besagt, dass Abstraktionen nicht nur problemanfällig sind vom Gesichtspunkt der Performanz her betrachtet, sondern auch über technische Sachverhalte hinwegtäuschen können und dazu führen, dass diese zum Vorschein treten (hierauf wird mit \textit{leaky} angespielt), obwohl sie 'wegabstrahiert' sein sollten \autocite{man1:law_of_leaky_abstractions}.

%%% Evolution, Standardisierung

C++ ist seit der Erstveröffentlichung bzw. ersten Standardisierung im Jahr 1998 stark evolviert, sogar so stark, dass einige Entwickler C++ erst ab der Version C++11 und neuer als C++ ansehen und dessen Vorläufer als ,,historisches C++'' oder auch als ,,Legacy'' bezeichnen. Trotzdem ist stets auf Abwärtskompatibilität geachtet worden \autocite{man1:evolving_cpp,buecher:2016_programming_languages_concepts,buecher:2017_clean_cpp_book}.  Alle geläufigen Compiler unterstützen Einstellungen, mit denen sich der zu verwendende C++-Standard festsetzen lässt, wie bspw. die Option \texttt{-c++11} beim C++ Frontend \acp{GCC}. Der Unterschied zu Python liegt hier darin, dass um ältere Python-Versionen nutzen zu können, mehrere Interpreter gleichzeitig (vollständig) installiert sein müssen, was Speicherplatz kostet, sonst aber keine weiteren Nachteile bringt.

Eine häufig auftretende Fehlvorstellung ist, dass C++ nur eine Erweiterung von C ist, was man beispielsweise an der Fülle von Beiträgen auf der Plattform Stackoverflow erkennt, die dies implizieren.\footnote{Vgl. Fragen auf der Plattform Stackoverflow mit gleichzeitigem Tag C und C++, Query: \url{https://stackoverflow.com/questions/tagged/c\%2b\%2b\%20c}.} Wichtig ist es hervorzuheben, dass C++ und C zwei verschiedene Programmiersprachen sind, die zwar Mitglied derselben Sprachfamilie, in vielen Punkten aber grundlegend verschieden sind: C++ verfügt über eine Fülle von Möglichkeiten, die in C nicht bestehen, wie z.B. Templates, Namespaces und Klassen. Gleichzeitig bestehen Quelltextambiguitäten, d.h. gleicher Quelltext, der entweder von beiden Sprachen unterschiedlich interpretiert wird oder in einer valide und in der anderen nicht valide ist. Als Beispiel:\footnote{David R. Tribble. \textit{Incompatibilities Between ISO C and ISO C++}. Abgerufen am 14.7.2019. \url{http://david.tribble.com/text/cdiffs.htm}}

\begin{itemize}
    \item\texttt{static x = 0;} ist in C90 valide, aber nicht in C++. In C90 sind Variablen ohne Typdeklaration automatisch \texttt{int}, in C++ muss ein Datentyp explizit oder mindestens \texttt{auto} angegeben werden.
    \item C++ erlaubt sogenannte funktionale Typumwandlungen, wie z.B. \texttt{f = float(x);} C nicht.
    \item\texttt{sizeof('c');} hat sehr wahrscheinlich unterschiedliche Werte in beiden Sprachen.
    \item Der Ausdruck \texttt{int y = (int)(abs(0.6) + 0.5);} wird in C++ als 1 evaluiert, in C als 0, da in C keine überladene Funktion \texttt{abs} (absolut / Betrag) für Gleitkommazahlen besteht, stattdessen müsste für diesen Fall \texttt{fabs(float)} genutzt werden.
\end{itemize}

Eine von einer internationalen Institution wie \ac{ISO} standardisierte Programmiersprache wie C++ ist im Gegensatz zu einer nicht standardisierten Sprache wie Python gewissermaßen weniger 'flexibel', das zeigt sich am langwierigen Standardisierungsprozess: Der allererste begann 1989, wurde aber erst 1998 abgeschlossen. Auch C++11 wurde verspätet standardisiert, ursprünglich hieß diese Version C++0z, wobei der Name impliziert, dass sie vor 2010 veröffentlicht werden sollte \autocite{buecher:2016_programming_languages_concepts,man1:evolving_cpp}.

\paragraph{Python}\mbox{}

Python als nicht standardisierte Sprache hat den Vorteil, dass Erweiterungen und Modifikationen bürokratielos und damit in zeitlich kürzeren Abständen veröffentlicht werden können - einfach online als Update der Referenzimplementierung. Das hat den Nachteil, da Abwärtskompatibilität bei Python einen geringeren Stellenwert hat, dass neuere Versionen mitunter zu altem Quelltext nicht mehr kompatibel sind. Das ist insbesondere der Fall bei Versionen von Python 2 zu Python 3, die sich sprachlich stärker unterscheiden. Schade ist, dass viele in Python 2 geschriebene Programme dem Wegfall des Supports für diese Version unterliegen, den die Entwickler im Jahr 2020 planen einzustellen \cite{man1:pep373}.

Unter der Problematik von Pythons Interversionskompatibilität leiden nicht nur Anwender, welche in einigen Fällen gezwungen sind, mehrere Python-Versionen parallel installiert zu haben, sondern auch Modulentwickler, wie beispielsweise Davis King von DLib, welcher dies in einem Interview als Hauptproblem bei der Verwaltung seiner Open-Source-Maschinenlernbibliothek nennt \cite{man1:interview_davis_king}:

\blockquote{
    \textit{
        ,,The core of dlib is easy to build. But the Python bindings? Not so straightforward. [...] there are a huge number of Python versions out there that are all incompatible in the sense that extension modules built for one Python won’t work with another. [...] What makes this problem particularly bad is that it’s common [...] So there are all these ways of installing Python, boost.python, and dlib, and there are no standards or anything.''
    }
}

Trotz einiger Probleme zeigt es sich, dass sich Python als eine weitere über C++ liegende Abstraktionsebene eignet und gern auch von Nicht-Softwareentwicklern angenommen wird als eine schnell zu erlernende Skriptsprache \cite{lang_libs:comparasion_python_vs_cpp_learn}. Wohl aus dieser Tatsache ergaben sich Aufrufe in Veröffentlichungen wie z.B. von Oliphant \cite{lang_libs:python_for_scientific} oder Mannila et al. \cite{sonstiges:2006_learn_python_in_schools} an Bildungseinrichtungen gerichtet, Python verstärkt zu lehren, dem sicherlich viele folgten, woraus sich (vielleicht teilweise) Pythons wachsende Popularität \cite{sync:2015_programming_language_use} erklären lässt. 

C++ ist inzwischen zu einer vielseitigen, mächtigen Sprache angewachsen, die im Backend-Bereich des Maschinenlernens (bzw. Deep-Learnings) offensichtlich nicht wegzudenken ist und nicht zwingenderweise durch eine Oberfläche in Python ergänzt werden  muss, so aber zumindest eine höhere Produktivität verspricht \cite{lang_libs:comparasion_scripting_compiled_fu}.

%\cite{lang_libs:python_for_scientific}
%\cite{lang_libs:comparasion_python_vs_cpp_learn}
%\cite{sonstiges:2013_api_design_scikit_learn}
%\cite{buecher:2015_effective_modern_cpp}
%\cite{buecher:2017_clean_cpp_book}
%\cite{buecher:2016_programming_languages_concepts}
%\cite{unsortiert:2001_efficient_embedded_cpp}
%\cite{unsortiert:exploring_raspi_with_cpp}
%\cite{unsortiert:cpp_problem_analysis_malik}
%\cite{lang_libs:comparasion_scripting_compiled_fu}
%\cite{lang_libs:comparasion_7_programming_languages}
%\cite{lang_libs:comparasion_programming_languages_nsl}
%\cite{sync:2019_lieblingssprache}
%\cite{sync:2015_programming_language_use}
%\cite{sync:2013_language_popularity}
%\cite{sonstiges:2006_learn_python_in_schools}

