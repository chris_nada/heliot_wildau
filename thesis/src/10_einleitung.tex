\section{Einleitung}

Im Zuge der digitalen Revolution haben sich Technologien etabliert, die insbesondere im Zusammenwirken
mit anderen technologischen Trends neues Potenzial entwickeln. Zu Synergien kommt es beispielsweise auch bei \ac{KI} 
im Zusammenspiel mit \ac{IoT}. Abgesehen von den Einsatzbereichen Smart Home, Smart Factory oder 
Smart City, allesamt Beispiele derzeitiger Trends, welche die beiden Technologien kombinieren,
lassen sich \ac{IoT}-Geräte auch schon von Hobby-Anwendern mit \ac{KI} gekoppelt beispielsweise zur 
Erkennung von Personen einsetzen \cite{unsortiert:handbook_of_face_recogn}. 
Automatisierte Gesichtserkennung ist kein ganz frisches Forschungsthema mehr, 
1973 wurde der wahrscheinlich erste Ansatz dazu von Kanade vorgestellt 
\cite{man1:kanade_processing1973}.

Die Gesichtserkennung in Videos wird heutzutage auch zu banaleren Zwecken als zur
Terrorismusbekämpfung von staatlicher Seite aus eingesetzt, wie z.B. zur Zuschauerzählung bei Events,
für automatisches Tagging von Youtube-Videos oder die Erkennung von Personen durch in Roboter
integrierte \ac{IoT} \cite{face_tracking:2017_security_eval_face_reco,face_detect:2010_robot_vision}.

Einen ähnlichen Anwendungsfall zieht auch die Technische Hochschule Wildau, sozusagen der Patron dieser Thesis, in Betracht,
in deren Bibliothek sich seit September 2016 das Robotermodell \textit{Pepper} bewegt. Um individuell auf Fragen der
Bibliotheksbesucher reagieren zu können, müsste dieser diese quasi \textit{kennen}. Das Stichwort \textit{soziale
Interaktion} spielt hier eine wichtige Rolle: Nachdem anfänglich sogar
seine Spracherkennung kaum praktisch genutzt wurde und der Roboter hauptsächlich über ein Tablet bedient werden musste,
ist die Vision, dass Pepper bloß anhand des Gesichts einem Studenten ohne vorherige Nutzerinteraktion seinem Studiengang entsprechende 
Buchempfehlungen geben kann \cite{wildau:mohnke_library_robot}. Ob und inwiefern dieses Konzept in die Realität umgesetzt wird, 
soll nicht Gegenstand dieser Arbeit sein.

\subsection{Zielsetzung}
\label{kap:zielsetzung}

Der Pepper-Roboter spielt trotzdem eine nicht unwichtige Rolle bei der Ausarbeitung der Fragestellung,
um die sich diese Thesis drehen soll. Wenn auch eine direkte Arbeit mit und an der Robotik hier nicht stattfinden
wird, so soll ein technisch ebenbürtiges Gerät als Repräsentant dienen - der Einplatinencomputer Raspberry Pi. 
Technisch ähnlich ist er aus folgenden Gründen:
Er besitzt in Version 1.6 mit einem x86\_64 Vierkern-\ac{CPU} für mobile Anwendungen eine vergleichbare Performanz
wie die aktuelle Version 3+ des Raspberry Pi, dessen Vierkern-\ac{CPU} allerdings auf der \ac{RISC}-Architektur \ac{ARM} basiert. 
Beide Plattformen nutzen Linux-Derivate als Betriebssystem. Die Raspberry-Pi-Plattform soll als \ac{IoT}-Gerät
nicht nur als Analogon zur Robotik, sondern auch als Repräsentant einer Vielzahl von möglichen \ac{IoT}-Geräten dienen.
Damit ein Transfer einer Anwendung zur Gesichtserkennung von einem Gerät auf andere möglich wird, muss diese
plattformunabhängig entwickelt werden. Da selbst plattformunabhängige Anwendungen in vielen Fällen einer
Anpassung bedürfen, soll dies hier als erste Anforderung an die Ausarbeitung als \textit{Flexibilität} festgehalten werden.
Ein Transfer einer Software auf ein anderes System sollte umso problemloser funktionieren, je stärker sich die Systeme ähneln.
Mit der Verwendung von Quelltext und Programmiersprache, wo keine systemspezifischen Strukturen enthalten sind,
lässt sich diese Kompatibilität noch erhöhen \cite{man1:cross_platform}.

Dem Wunsch der Hochschule, zukünftig den Roboter Pepper zur Gesichtserkennung zu nutzen, entsprechen keine formellen
Anforderungen in Form eines Lastenhefts, daher sollen hier nur grob und informell Anforderungen aufgestellt werden,
um den Rahmen der Erkundung, wie mobile Geräte als Klienten zur Gesichtserkennung eingesetzt werden können, 
einzugrenzen. Festzuhalten gilt es noch, dass beim Pepper Sicherheitsaspekte nicht vernachlässigt werden dürfen, diese sich also auch in der \ac{IT}-Infrastruktur widerspiegeln sollen \cite{wildau:wildauer_bibsymposium}.

Prinzipiell existieren selbst schon Ansätze zur Gesichtserkennung für mobile Anwendungen, auch solche, die vollständig
auf Roboterhardware laufen. Das Framework OpenCV als Beispiel, welches dank seiner Plattformunabhängigkeit auch auf robotischen
Anwendungen und \ac{IoT}-Geräten mit Windows- oder Linux-Betriebssystem lauffähig ist, wird sogar mit 
zur Anwendung fertigen Gesichtserkennungsalgorithmen geliefert. ,,Zur Anwendung fertig'' bedeutet, dass nicht etwa
der Aufbau eines neuronalen Netzes durch den Programmierer notwendig ist oder das manuelle Transformieren von Daten.
Der Algorithmus nutzt als Eingangsdaten Bilder in geläufigen Dateiformaten \cite{buecher:2012_mastering_opencv}.
Ein mehrfach genannter
Kritikpunkt ist jedoch die Performanz solcher Algorithmen, sowohl von der Rechenleistung her als auch von der
Erkennungsrate. Diese Punkte werden dann als Empfehlung zur weiteren Untersuchung ausgesprochen 
\autocite{face_detect:multi_method,robotik:person_recognition,robotik:diplom_robot_bluetooth}.

Zusammengefasst wird folgende Fragestellung in dieser Arbeit behandelt:

\begin{quote}
	Wie lässt sich eine möglichst flexible, robuste und performante Gesichtserkennung in Klient-Server-Architektur mit zusätzlicher Rücksichtnahme auf \ac{IT}-Sicherheit implementieren?
\end{quote}

Das Augenmerk liegt auf den Adjektiven \textit{flexibel}, \textit{robust}, \textit{performant}
sowie der Restriktion, eine Klient-Server-Architektur zu verwenden, die den geläufigen und aktuellen Praktiken der
IT-Sicherheit genügt anstatt Schneiers Gesetz zu befolgen.\footnote{\textit{,,Anyone, from the most clueless amateur to the best cryptographer, can create an algorithm that he himself can't break.''} Aus: Memo to the Amateur Cipher Designer, 15.10.1998,
Crypto-Gram; online: https://www.schneier.com/crypto-gram/archives/1998/1015.html}
Für diese genannten Entwicklungsprinzipien wird in den folgenden Kapiteln dargestellt, wie sie sich mit
dem aktuellsten Stand der Technik, ohne dabei die beste Praxis (engl. best practices) außen vorzuenthalten, umsetzen lassen.
Parallel zur Ausarbeitung der theoretischen Konzipierung des Systems wird auf dessen Grundlage eine prototypische
Softwareentwicklung stattfinden, die jedoch aus praktischen Gründen für dessen Bereitstellung
an der TH-Wildau in der Implementierung vom Konzept leicht abweichen kann. Die Entwicklung findet
unter der Projektbezeichnung \textit{Heliot} quelloffen statt.\footnote{$https://www.gitlab.com/chris\_nada/heliot\_wildau$}

% TODO Überblick über die folgenden Kapitel (später)

\subsection{Begriffsklärung}

Leider können einige in der Thematik dieser Arbeit gebräuchliche Begriffe missverständlich sein,
teilweise weil sie im Englischen anders gebraucht werden oder in nicht wissenschaftlichen Medien verwechselt werden.
Daher soll nun kurz festgehalten werden, wie diese hier zu verstehen sind.

Im Englischen existieren die Begriffe \textit{Face Detection} und \textit{Face Recognition}, die den Prozess der 
Erkennung (Detektion) von Gesichtern in Bildern und die Identifikation (Wiedererkennung) desselben Gesichts unterscheiden, während im Deutschen beide Vorgänge wohl gleichbedeutend zu \textit{Gesichtserkennung} sind. Da hier keine neuen Begrifflichkeiten eingeführt werden
sollen, sei darauf hingewiesen, dass Gesichtserkennung kontextuell auch nur eine der beiden Bedeutungen haben kann. In wichtigen Fällen wird auf Eindeutigkeit im Ausdruck geachtet und z.B. einer der beiden englischen Begriffe genutzt.

Im deutschsprachigen Wissenschaftsbereich scheinen die Begriffe \textit{künstliche Intelligenz, Maschinenlernen} und 
\textit{künstliche neuronale Netze} mehr oder weniger eindeutig definiert zu sein:
Künstliche Intelligenz stellt oft einen Oberbegriff für \ac{KNN} und Maschinenlernalgorithmen dar, während letzteres alle Algorithmen umfasst, die ohne \acp{KNN} auskommen, aber trotzdem (untechnisch ausgedrückt) \textit{lernen} \cite{man1:russel_norvig_ki}.



