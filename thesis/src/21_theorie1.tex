\subsection{Wiedererkennung von Gesichtern}
\label{kap:gesichtserkennung}

Dass die Wiedererkennung von Gesichtern und deren Detektion im Bild gar nicht so grundverschiedene Aufgaben sind, wird schon daran deutlich, dass sich einige Algorithmen für beides (in jeweils angepasster Form) eignen, wie bereits im vorangegangenen Kapitel \ref{sec:t0_algorithmen} erwähnt. 

\paragraph{Warum ist das so?}\mbox{}

Diese behandelten Algorithmen arbeiten oft mit mathematisch darstellbaren - und damit für Computer verständlichen - Merkmalen (meist engl. als \textit{features} bezeichnet) des zu erkennenden Objekts. Das könnten zum Beispiel wiederkehrende geometrische Strukturen sein, die alle menschlichen Gesichter gemeinsam haben und sie von anderen Objekten und dem Hintergrund im Bild unterscheiden. Genau hier liegt der Unterschied zur Wiedererkennung: Es sind nun nicht ,,Strukturen'' gefragt, die alle Gesichter von allen anderen Objekten unterscheiden, sondern solche, die das Gesicht einer bestimmten Person zu denen anderer Personen unterscheiden. Vom Prinzip her handelt es sich wieder um eine Klassifikationsaufgabe, nur spezialisiert und potenziell schwieriger, denn Gesichter verschiedener Personen sind wohl nicht so einfach zu unterscheiden wie ein menschliches Gesicht und etwa eine Katze. Darum ist es eine Hauptaufgabe von Wiedererkennungsalgorithmen, Merkmale (z.B. als \textit{feature vector} oder \textit{feature matrix}) zu definieren, die bei Gesichtern derselben Person möglichst dicht beieinander liegen und bei Gesichtern unterschiedlicher Personen möglichst weit auseinander \autocite{buecher:2015_handbook_pattern_recognition_computer_vision, unsortiert:handbook_of_face_recogn}. 

Mathematisch wird dieser Sachverhalt in der Praxis meist mittels euklidischer Distanz (\ref{eq:dist_euklid}) oder Kosinusdistanz (\ref{eq:dist_kosinus}) (seltener Jaccard-Ähnlichkeit) gemessen. Zu beachten ist bei der praktischen Anwendung der unterschiedliche Werteraum: Die Kosinusdistanz kann (in der hier dargestellten Form) zwischen 0 (identisch) und 1 liegen (bei ausschließlich positiven Werten in $x$ und $y$), während die euklidische Distanz Werte von 0 bis (theoretisch) unendlich annehmen kann (praktisch ist aber der Wertebereich jedes Feature-Vektors bzw. jeder Feature-Matrix begrenzt, allein schon aus technischen Gründen,\footnote{Beispielsweise hat eine Fließkommazahl, \texttt{float}, auf einem x86\_64-System üblicherweise einen Maximalwert von $16.777.216$ (entspricht $2^{24}$, vgl. \url{https://stackoverflow.com/q/57003891}), Überlauf hier vernachlässigt.} sodass auch die euklidische Distanz einen Maximalwert besitzt) \autocite{buecher:2015_handbook_pattern_recognition_computer_vision}.

\begin{equation} \label{eq:dist_euklid}
dist_{Euklid}(x,y) = \sqrt{ \sum_{xi=1}^{m} (x_i - y_i)^2 }
\end{equation}

\begin{equation} \label{eq:dist_kosinus}
dist_{Kosinus}(x,y) = 1 - \frac{x \bullet y}{ \vert\vert x\vert\vert \cdot \vert\vert y \vert\vert }
\end{equation}
\\
Um diese Formeln anwenden zu können, müssen Feature-Vektoren / Feature-Matrizen her. Diese liefern die Gesichtserkennungsalgorithmen. Rokkones unterscheidet hier inhaltlich zwischen zwei grundlegenden Kategorien \cite{face_reco:2018_master_thesis_rokkones}:

\begin{itemize}
    \item Merkmalextraktoren (Feature Extraction): \ac{LBP}, \ac{LDP}, \ac{LDSP}, Haar-Wavelets und \ac{HOG}.
    \item Deep-Learning: \ac{CNN} und \ac{RNN}.
\end{itemize}

Sauber wäre diese Trennung nicht, da Methoden aus dem Deep-Learning, also \acp{KNN}, nicht selten selbst Merkmalextraktoren sind, wie bspw. VGG-Face. Schon 1989 hat Hornik et al. gezeigt, dass neuronale Netze auch als  Funktionsapproximatoren angesehen werden können, was als \textit{Universales Approximationstheorem} bekannt ist. Die Funktion Bilddaten \textrightarrow  Klassifikation bzw. Bilddaten \textrightarrow  Merkmalmatrix kann beliebig akkurat approximiert werden, vorausgesetzt es sind genügend Hidden-Layer verfügbar \cite{man1:hornik_nn}.\footnote{Streng genommen besagt das \textit{universale Approximationstheorem}, dass mehrschichtige, sigmoide Feedforward-Netze Borel-messbare Funktionen von einem endlich dimensionalem Raum in einen anderen endlich dimensionalen Raum approximieren können.} Bei der Anwendung solcher Netze wird über die Hidden-Layer einer auf \ac{KNN} basierenden Gesichtserkennung zwar in jedem Fall die Dimensionalität der Daten reduziert bis hin zu einer Klassifikationsschicht, die einen einzigen Wert zur Ausgabe hat, nämlich das Ergebnis der Klassifikation als Label, aber im Falle von VGG wird nicht direkt eine Klassifikation ausgegeben, sondern ein (1024 oder 4096-dimensionaler) Feature-Vektor, der an sich kein fertiges Klassifikationsergebnis darstellt \cite{man1:vgg_face}. Vielleicht könnte man auf Basis dieser Tatsache neuronale Netze zur Gesichtserkennung kategorisieren in solche, die dem Anwender direkt ein Label liefern und solche, wie VGG, die einen Merkmalsvektor ausgeben, der in einem weiteren Schritt, i.e. ein Ähnlichkeitsvergleich, z.B. mittels euklidischer oder Kosinusdistanz, noch mit den Lerndaten abgeglichen werden muss.

2001 dagegen wurden von Gross et al. noch folgende Methoden zur Merkmalextraktion aufgezählt \cite{face_reco:quo_vadis}:

\begin{itemize}
    \item \acl{SVM},
    \item \acl{LDA},
    \item \acl{PCA},
    \item Kernel-Methoden und 
    \item \acp{KNN}.
\end{itemize}

Hier wurden noch verschiedene herkömmliche, also nicht \ac{KNN}, Maschinenlernmethoden einzeln aufgezählt, während \acp{KNN} noch eine Sonderstellung einnahmen. Inzwischen hat sich das Blatt längst gewendet, denn genau wie bei der Detektion haben Nicht-Deep-Learning-Ansätze in der Regel Probleme mit Varianzen bei der Ausrichtung des Gesichts sowie mit variierenden Beleuchtungsverhältnissen \cite{face_reco:quo_vadis}.

Neuronale Netze dominieren das Spektrum der Gesichtserkennungsalgorithmen fast vollständig, daher werden eher diese kategorisiert, manchmal in \ac{CNN} und \ac{RNN} \cite{face_reco:2018_master_thesis_casimiro_real_time_face_reco}. 

Normalerweise käme man in einer Arbeit über die Thematik der Gesichtserkennung mittels \ac{KNN} jetzt zu dem Punkt, längere Zeit darüber zu philosophieren, welcher Netzaufbau, welche Aktivierungsfunktionen, welche Hyperparameter doch bloß die besten wären, welche in der Vergangenheit verwendet wurden, wie man diese modifizieren könnte. Damit soll in der vorliegenden Arbeit nicht angefangen werden - aus zweierlei Gründen: 

\begin{enumerate}
     \item Es soll kein neues Netz (oder Algorithmus) zur Gesichtserkennung kreiert werden, denn es existieren bereits hunderte. Konsequenterweise soll parallel dazu beispielsweise auch keine neue Datenschnittstelle zur Kommunikation zwischen einem mobilen Gerät und einem Server geschaffen werden, es existieren ebenfalls einige. Dazu mehr in späteren Kapiteln ($\rightarrow$ Kapitel \ref{kap:kommunikation} \nameref{kap:kommunikation}).
     \item Der Aufbau und auch die Systematik neuronaler Netze kann schnell sehr komplex werden. Neuronale Netze zur Gesichtserkennung besitzen sehr viele verschiedene Schichten (Hidden Layer), deren Aufbau für einen Anwender überhaupt keine Rolle spielt.
\end{enumerate}

Das ist natürlich so nicht ganz wahr. Ein Anwender, der einen Gesichtserkennungsalgorithmus in die Hand gedrückt bekommt, über dessen Innenleben er keine Kenntnis hat (Stichwort Blackbox), bemerkt in jedem Fall mindestens drei Eigenschaften allein durch die Anwendung: die Wiedererkennungsrate, die Rechenzeit und auch den Schwierigkeitsgrad der Anwendung; gemeint ist die Komplexität der Implementierung und Integration in ein Gesichtserkennungsprogramm, falls diese nicht bereits gegeben ist. Genau diese drei Faktoren sollen im Kontext dieser Arbeit folglich spielentscheidend für die Auswahl des zur Anwendung kommenden Gesichtserkennungsalgorithmus sein.

\subsubsection{Datensätze}

\begin{sidewaystable}[]
	\footnotesize
	\begin{tabular}{|l|l|l|l|l|l|l|}
		\hline
		\textbf{Datensatz} & \textbf{Größe} & \textbf{Anzahl Bilder} & \textbf{Individuen} & \textbf{Lizenz} & \textbf{Verfügbarkeit} & \textbf{Quelle} \\ \hline
		FERET & 8,5 GB & 14.126 & 1.199 & Release Agreement & auf Antrag & \cite{ds:feret} \\ \hline
		FaceScrub &  & 106.863 & 530 & CC 3.0 & auf Antrag & \cite{man1:scrubface} \\ \hline
		\begin{tabular}[c]{@{}l@{}}CASIA-Webface\end{tabular} & 4,1 GB & 494.414 & 10.575 &  & auf Antrag, offizielle Links tot & \cite{ds:casia} \\ \hline
		\begin{tabular}[c]{@{}l@{}}Labeled Faces \\ in the Wild \\ (LFW)\end{tabular} & 173 MB & 13.233 & 5.749 &  & frei herunterladbar & \cite{man1:lfw_updates} \\ \hline
		IMDB-WIKI & $\sim$275 GB & 523.051 & 82.612 & nur für Forschung & frei herunterladbar & \cite{ds:imdb} \\ \hline
		\begin{tabular}[c]{@{}l@{}}YouTube \\ Faces\end{tabular} & 12 GB &  & 1.595 &  & auf Antrag & \cite{ds:youtube} \\ \hline
		CelebFaces &  & 202.599 & 10.177 &  & frei herunterladbar, Links tot & \cite{ds:celebs} \\ \hline
		Facebook &  & 800.000 &  & nur für Forschung & Links tot & \cite{ds:facebook} \\ \hline
		PubFig &  & 58.797 & 200 & Bilder haben Copyright & nur Links zu Bildern & \cite{ds:pubfig} \\ \hline
		BioID & $\sim$100 MB & 1.521 & 23 &  & frei herunterladbar & \cite{ds:bioid} \\ \hline
		\begin{tabular}[c]{@{}l@{}}CMU \\ Face\\  Images\end{tabular} & 10 MB & 640 & 20 &  & frei herunterladbar & \cite{ds:cmu_face} \\ \hline
		\begin{tabular}[c]{@{}l@{}}CMU Pose, \\ Illumination, and \\ Expression (PIE)\end{tabular} & 305 GB & \textgreater 750.000 & 337 & \begin{tabular}[c]{@{}l@{}} Copyright; Kosten:\\ Forschung \$350, \\ Sonstige \$1000 \end{tabular} & auf Bestellung & \cite{ds:cmu_pose} \\ \hline
		MegaFace Challenge 2 &  & 4,7 Mio. & 672.057 &  & auf Antrag & \cite{ds:megaface} \\ \hline
	\end{tabular}
	\caption{Übersicht des Rechercheergebnis von Datensätzen zur Gesichtserkennung.}
	\label{tab:datensaetze}
\end{sidewaystable}


Da heutzutage Rechenkapazität oft nicht mehr als kritischer Faktor angesehen wird, liegt sowieso der Forschungsfokus darin, die Präzision in der Wiedererkennung von Gesichtern zu verbessern. Der am häufigsten genutzte Datensatz zur Qualitätsmessung (engl. Benchmark bzw. Benchmarking) des eigenen Algorithmus und gleichzeitig das umfassendste, frei verfügbare Benchmark (gemessen an der Zahl der verglichenen Gesichtserkennungen) ist \ac{LFW} der Universität Massachusetts. Weit über 100 Algorithmen finden sich dort aufgelistet. Ein Nachteil des Datensatzes \ac{LFW} ist, dass sich 5.749 verschiedene fotografierte Personen 13.233 Einzelbilder teilen, d.h. für jede Person stehen durchschnittlich nur etwa 2,3 Bilder zur Verfügung. Faktisch sind sogar nur 1.680 der 5.749 Identitäten mit zwei oder mehr  Bildern vertreten \cite{man1:lfw_updates}. \ac{LFW} ist nicht der einzige frei verfügbare Datensatz, der sich zur Gesichtserkennung eignet. \textit{Scrubface} beispielsweise, der mit 106.863 Bildern bei 530 Personen eine der höchsten 'Bilder-pro-Person-Ratios' mit etwa 201,63 Bildern pro Person anbietet, ist leider nur auf 'Antrag' verfügbar (in  der Praxis ein Web-basiertes Formular), und allein wegen dieser Hürde möglicherweise seltener in Veröffentlichungen von Gesichtserkennungsalgorithmen als Benchmark vertreten. Eine weitere Gefahr von Scrubface ist, dass dieser Datensatz die Bilder nicht selbst auf einem Server hostet, sondern im Prinzip nur aus einer Linksammlung inklusive Webcrawler besteht. Werden in der Sammlung eingebettete Links zu Bildern ungültig, verliert diese ihre Konsistenz. Inwiefern in einem solchen Fall Ergebnisse zwischen Algorithmen, die unterschiedlich vollständige Versionen von Scrubface verwenden, vergleichbar sind, ist fraglich. Die Autoren versuchen diese Problematik zu mildern, indem sie in den Linklisten den \ac{SHA}256-Hashwert mitliefern, sodass es jedem Nutzer möglich ist zu überprüfen, ob es sich bei dem heruntergeladenen Bild um das gleiche Bild handelt wie von den Autoren beabsichtigt \cite{man1:scrubface}. Ein dritter nennenswerter (so die Urheber von \ac{LFW})-Datensatz ist CASIA-Webface. Dieser enthält 494.414 Einzelbilder bei 10.575 Individuen, was durchschnittlich 46,75 Bilder pro Person bedeutet, welcher damit zwischen dem Wert von \ac{LFW} und Scrubface liegt. Im Gegensatz zu Scrubface sind hier die Bilder als Daten, nicht als Link enthalten, so kann durch tote Links keine Konsistenz verlorengehen. Der CASIA-Datensatz stammt von der Chinesischen Akademie der Wissenschaften, Institut für Automation (CASIA) der Fakultät Biometrik- und Sicherheitsforschung (CBSR). Ein möglicher Nachteil ist, dass wegen der Herkunft des Datensatzes viele seiner Ressourcen ausschließlich auf chinesischer Sprache verfügbar sind.\footnote{vgl. dazu die CASIA Webseite http://www.cbsr.ia.ac.cn/} Tabelle \ref{tab:datensaetze} zeigt eine Übersicht von Datensätzen zur Gesichtserkennung, die noch weitere außer den drei eben genannten enthält.\footnote{ Verfügbarkeit von Links wurde am 19. Juli 2019, 17:18 Uhr überprüft.}

\begin{figure}[h]
	\centering
	\hspace*{+1.25cm}
	\includegraphics[width=1.0\linewidth]{grafiken/plot_kumar/lfw_human.png}
	\caption{Ergebnis menschlicher Gesichtserkennung nach \cite{man1:human_scores}.}
	\label{fig:human_scores}
\end{figure}

Wichtig festzuhalten ist, dass für den \ac{LFW} Datensatz auch eine Studie an Menschen für die Gesichtserkennung durchgeführt worden ist. Die Ergebnisse liegen zwischen 0,9427 AUC und  0,992 AUC, je nach auf den Datensatz angewendeten Filter (vgl. Abbildung \ref{fig:human_scores}). So sollte dieser Ergebnisbereich als Messlatte maschineller Algorithmen dienen. \textit{Ausgerichtet} bedeutet, dass die räumliche Orientierung des Gesichts so korrigiert wurde, dass es völlig frontal erscheint; \textit{ausgeschnitten} bedeutet, dass der Hintergrund entfernt wurde, sodass das Gesicht das Bild füllt und \textit{inverse Maske} bedeutet, dass das Gesicht ausgeschnitten ist (durch ein schwarz gefülltes Rechteck ersetzt) und fast nur aus Hintergrund besteht (sowie Teile von Haar, Ohren, Hals usw.). Es wird von den Autoren angemerkt, dass die Studienbedingungen nicht dieselben qualitativen Anforderungen wie die der Maschinenlernalgorithmen erfüllen \cite{man1:human_scores,man1:lfw_updates}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\linewidth]{grafiken/plot_recos/lfw_algos.png}
	\caption{Ergebnis ausgewählter maschineller Algorithmen nach \cite{man1:lfw_updates}.}
	\label{fig:lfw_algo_scores}
\end{figure}

In Abbildung \ref{fig:lfw_algo_scores} werden Ergebnisse einiger bereits erwähnter Algorithmen (Fisher-Vector-Faces, (propabilistic) \ac{LDA}/\ac{LBP}, Eigenface) sowie einzelne, bekannte neuronale Netze (DLib, DeepFace, Face++, DeepID) verglichen. Mit sichtlichem Abstand das schlechteste Ergebnis erzielt der in die Jahre gekommene Eigenface-Algorithmus, der möglicherweise besser für die Detektion als für die Wiedererkennung  geeignet ist (vgl. Kapitel \ref{kap:gesichtdetektion} \nameref{kap:gesichtdetektion}). Die beiden weiteren Nicht-\ac{KNN}-Kandidaten, Fisher-Vector-Faces und propabilistic \ac{LDA}/\ac{LBP} liegen im Mittelfeld, während die Topleistungen ausschließlich neuronale Netze anführen. Ihr Großteil erzielt Leistungen von \ac{AUC}-Werten über 0.90, die besten schaffen über 0.95, einige auserwählte knacken selbst die 0.99er Marke (z.B. DLib) \autocite{man1:dlib09,man1:lfw_updates}. Manchmal inspirieren gute Leistungen eines Algorithmus Modifikationen durch Dritte, so zum Beispiel der auf DLib basierende FAREC, welcher bei einem (leider) nicht vergleichbaren Datensatz (Face  Recognition  Grand  challenge) eine Genauigkeit von $96\%$ angibt \cite{face_reco:dlib}.

Bei der Implementierung erwähnter neuronaler Netze in Software muss beachtet werden, dass einige Algorithmen proprietär sind und damit nicht der Öffentlichkeit frei zugänglich. Andere veröffentlichen zwar die Struktur ihrer Netze, teils auch inklusive genutzter Hyperparameter, das aber ohne Quelltext, was selbstverständlich a) Nachvollziehbarkeit und b) Reimplementierung erheblich erschwert. Auf der Webseite\footnote{Labeled Faces in the Wild, Webseite: \url{http://vis-www.cs.umass.edu/lfw/results.html}} von \ac{LFW} finden sich zu allen Algorithmen Verweise und Hinweise, ob diese proprietär sind.

Es soll nicht der Eindruck entstehen, dass bei der Recherche nach Algorithmen ausschließlich \ac{LFW} betrachtet wurde. Abgesehen von der in Tabelle \ref{tab:datensaetze} genannten Datensätze existieren noch eine Reihe weitere. Nur, weil diese hier keine explizite Nennung finden, soll ihr wissenschaftlicher Wert nicht als weniger relevant angesehen werden. Einige Veröffentlichungen nutzen auch in Eigenregie neu erstellte Datensätze anstelle eines bestehenden (z.B. \cite{face_reco:2017_iot_face_reco}). 

Eine \textit{honourable mention} für einen solchen soll noch der \ac{VGG}-Datensatz am Department of Engineering Science der Universität Oxford hier finden. Nennenswert ist dieses letzte Beispiel wegen seiner hochperformanten Implementierung eines \acp{CNN}, das außer auf dem eigenen Datensatz noch auf wenigstens zwei weiteren getestet wurde: \ac{LFW} \cite{man1:lfw_updates} (0,9913 \ac{AUC}) und YouTube Faces (0,9714 \ac{AUC}) (\cite{ds:youtube}). Das Netz ist eine Merkmalextraktion, i.e. keine direkte Klassifikation, mit einem 1.024- oder 4.096-dimensionalen Merkmalsvektor, wobei anzumerken ist, dass die 1.024-dimensionale Variante die genannten (besseren) Ergebnisse erzielt. Die Autoren geben den Datensatz und Algorithmus frei für nicht kommerzielle Forschung unter einer gebräuchlichen Lizenz\footnote{Creative Commons Attribution License 4.0} und bemühen sich um hohe Verfügbarkeit durch gleich drei Quelltextveröffentlichungen: in \textit{Torch}, \textit{Caffe} und \textit{MatLab} \cite{man1:vgg_face}. Dementsprechend schwirren in diversen Quelltext-Cloud-Diensten wie z.B. \textit{GitHub}\footnote{GitLab-Webseite: \url{https://github.com/}} sowohl diverse Kopien des Originals als auch abgewandelte Varianten.

\subsection{Verwandte Anwendungen aus IoT und Robotik}
\label{kap:verwandte_anwendungen}

Eine Gesichtserkennung kann inzwischen direkt auf einem Roboter bzw. einem \ac{IoT}-Gerät laufen \autocite{face_detect:2018_haar_features}, dazu reichen deren Leistungen aus, doch beim Anlernen eines \ac{KNN} kommen mobile Endgeräte momentan in der Regel noch an ihre Grenzen, da ihnen vor allem in den meisten Fällen dedizierte Grafikprozessoren fehlen, die sich mit Hilfe spezieller Frameworks auf diesen Anwendungsfall spezialisieren lassen.
Sollte sich auch in Zukunft die Leistungsfähigkeit von Einplatinenrechnern steigern, hat das Auslagern des Anlernens immer noch den Vorteil, dass nur eine einzelne zentralisierte Lerndatenbasis (Lernkorpus) angelegt / gepflegt werden muss. Im Falle der Gesichtserkennung mit verteilten \ac{IoT}-Geräten, die als Klienten mit einem Zentralrechner interagieren, kann folglich eines der Geräte von den Erfahrungen aller anderen profitieren oder technisch ausgedrückt: Es kann ein Gerät mit einem angelernten Netz arbeiten, dessen Lernkorpus nicht nur aus diesem Gerät, sondern aus allen vernetzten Klienten stammt. In diesem Kapitel sollen einige mit der Problemstellung dieser Arbeit verwandte Veröffentlichungen betrachtet werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\linewidth]{grafiken/plot_iot_trend/iot_prognose.png}
	\caption{Prognose der Weltbevölkerung vs. vernetzter \ac{IoT}-Geräte nach \cite{man1:iot_studie_cisco}.}
	\label{fig:iot_prognose}
\end{figure}

Als Erstes soll ein Begriff, der als Trend im \ac{IoT}-Sektor gilt, eingeführt werden, welcher auch im Zusammenhang der Gesichtserkennung jetzt oder möglicherweise in der Zukunft eine Rolle spielt: das \textit{Fog-Computing}. Der Begriff wurde  wohl so gewählt, um an Cloud-Computing zu erinnern,\footnote{Engl. \textit{fog = der Nebel}; engl. \textit{cloud = die Wolke}.} sich jedoch davon abzugrenzen. Im Falle von Gesichtserkennung könnte im Cloud-Computing diese bspw. als sogenannte \ac{SaaS} angeboten werden oder auf einer niedrigeren Abstraktionsebene als \ac{PaaS} bzw. \ac{IaaS}. Damit würden Algorithmen nicht mehr auf lokalen Systemen instanziiert werden, sondern eine in der Cloud betriebene Erkennung lokale Abnehmer (Endgeräte) mit Ergebnissen bedienen. \ac{IoT}-Geräte, die Teil eines Fog-Computing-Netzwerks sind, würden im Gegensatz dazu \ac{KI}-Algorithmen selbst betreiben und nur deren Ergebnisse weitergeben. Der Kerngedanke, der hinter Fog-Computing steckt, ist die Tatsache, dass für \ac{IoT}-Geräte (jetzt oder in Zukunft) Rechenkapazität eine abundantere Ressource als die Bandbreite der Kommunikationsschnittstelle ist bzw. sein wird \autocite{iot:2018_iot_emerging_technologies}. Die Problematik der Leistungsfähigkeit ist relevant, denn die Anzahl online verbundener \ac{IoT}-Geräte nimmt rapide zu (vgl. Abbildung \ref{fig:iot_prognose}), woraus man schließen kann, dass auch die Nachfrage und Entwicklungen in diesem Bereich ansteigen \cite{man1:iot_studie_cisco}. Der Faktor Sicherheit wird im \ac{IoT}-Umfeld immer wieder als kritisch angesehen und trotzdem häufig missachtet \autocite{iot:2018_iot_machine_learning_trends,iot:iot_security_evolution,iot:iot_security_trends,iot:2018_iot_security}.

\paragraph{Ähnliche Arbeiten}\mbox{}

Nachfolgende Arbeiten werden weder chronologisch noch nach Relevanz sortiert. Die Reihenfolge ist somit willkürlich. Eine Herabsetzung wissenschaftlicher Forschungsleistungen dadurch ist völlig unbeabsichtigt.

Ueberschaers Diplomarbeit \cite{robotik:diplom_robot_bluetooth} von 2009 implementiert Erkennung und Identifikation von Personen in der Programmiersprache C++ über eine Bluetooth-Kommunikationsschnittstelle für einen Roboter 'WelcomeBot'. Optische Gesichtserkennung wurde erwogen, ist letztendlich doch wegen Nachteilen wie unzureichender Rechenleistung verworfen worden. Eine beachtenswerte Leistung des Projekts ist die funktionierende Navigation des Roboters in Relation zu ihm zugewiesenen Personen mittels Sensorik. Einen Großteil der Arbeit nimmt die Implementierung der Kommunikationsschnittstelle ein. Es wird Quelltext in C++ auf Bit-Ebene implementiert via Zeigern, Ringpuffern und Hexadezimal-Formatierungen sowie eine Entfernungsabschätzung auf Basis der Signalstärke. Bei der Diplomarbeit ist zu beachten, dass bei Sicherheitseinschätzungen von Netzwerkschnittstellen (\ac{SSH}, Telnet und \ac{HTTP}) vermutlich eine Verwechslung auftrat: Telnet und \ac{HTTP} (beide enthalten überhaupt keine kryptographische Schicht, d.h. jede Nachricht wird als Klartext übertragen \cite{buecher:websecurityprivacycommerce2nd}) werden als sicher (,,Sicherheit +'') eingestuft, während die einzige einigermaßen sichere Schnittstelle (es bestehen mögliche Verwundbarkeiten), SSH, als unsicher (,,Sicherheit -'') bezeichnet wird.\footnote{Vgl. \cite{robotik:diplom_robot_bluetooth} S.76.}

Eine nicht ganz unähnliche Arbeit von 2018 stellt die Implementierung einer geführten Tour durch das Robotik-Labor der Universität Queensland (Australien) durch einen Pepper-Roboter. Augenmerk liegt bei dem Projekt ebenfalls auf der Navigation mittels Sensorik und festgelegter Route durch das Labor sowie der Präsentation von Informationen an bestimmten Schlüsselorten. Bei der Navigation wurde insbesondere auf das Umgehen von Hindernissen mit Hilfe von Laser-Abstandsmessern geachtet. Eine Erkennung von individuellen Personen fand nicht statt \cite{robotik:pepper_tours}.

Unter dem vielversprechendem Titel ,,Multi-User  Identification  and  Efficient  User  Approaching  by  Fusing Robot and  Ambient  Sensors'' (zu Dt. etwa ,,Identifikation mehrerer Nutzer und effiziente Nutzerannäherung durch die Fusion von Robotik- und Umgebungssensorik'') stellte 2014 ein Team der Universität Amsterdam einen modifizierten Roboter, genannt Care-O-Bot\textregistered, zur Umsorge pflegebedürftiger Personen vor. Die Neuheit liegt hier in der Erkennung und der automatischen Verfolgung zu betreuender Personen. Die Implementierung wird auf theoretischer und praktischer Ebene offengelegt \cite{robotik:multi_user_approach}.

Ein Jahr vorher, 2013, untersuchte das Team des Fraunhofer-Instituts für Produktionstechnik und Automatisierung (IPA) die Möglichkeit, Gesichtserkennung in Pflegerobotern, genannt wird derselbe Care-O-Bot, der im vorigen Paper behandelt wurde, zu implementieren. Diese Arbeit kann also als direkter Vorläufer dafür gesehen werden. Man kommt zu dem Schluss, dass a) die Roboter-Hardware für die Anwendung genügt und b) Algorithmen wie Eigenfaces und Fisherfaces, die mit der Programmbibliothek OpenCV mitgeliefert werden, sowohl robuste Ergebnisse liefern (Erkennungserfolge in $>90\%$ der Fälle) als auch Resistenz gegenüber unterschiedliche Gesichtsausdrücke als auch -ausrichtungen aufzeigen. Zur Verifikation sind Datensätze der Universität Yale und AT\&T verwendet worden. Noch ein erwähnenswerter Fakt ist die Erkenntnis, dass der Raspberry Pi seine volle Rechenleistung erst bei Netzteilen, die 2A liefern, ausschöpfen kann. Über diesen Sachverhalt kann man sicherlich leicht stolpern \cite{robotik:person_recognition}.

In einer Bachelorarbeit der Universitat Politècnica de Catalunya von 2018 mit Kurzfassung auf English, Spanisch und Katalanisch werden die Frameworks OpenCV, DLib, TensorFlow in Python auf dem Raspberry Pi 3B verbunden und zur Gesichtserkennung via \ac{KNN} implementiert. Eine Kostenanalyse für Material sowie Personal und eine Projektanalyse mit GANTT-Chart sticht als ungewohntes Bild hervor. Cloud-Techniken wie \textit{Google Colaboratory} werden in Betracht gezogen, jedoch wegen technischer Probleme verworfen und die Gesichtserkennung wird schließlich lokal implementiert (\textit{Inception V3} sowie \textit{MobileNet}). Ein Echtzeit-Streamen von Videos über WLAN wird umgesetzt. Die Gesichtserkennung auf dem Raspberry Pi direkt laufen zu lassen führt zu unzumutbaren Latenzen. Bilder von der Kamera des Raspberry Pi werden auf $\frac{1}{4}$ der Größe skaliert, um Rechen- und Netzwerkleistung einzusparen. Als Erkennungsrate wird im Ergebnis $93\%$ angegeben, leider ohne dabei weitere Details zu nennen. Die Arbeit ist als Vorarbeit gedacht für die Interaktion von einem Betreuungsroboter für Kinder mit autistischen Erkrankungen
\cite{robotik:2018_robo_social_vision_embedded_external}. 

Auch die \textit{soziale Interaktion mit Robotern} ist aktiver Forschungsbereich: Bei einem individuellem Roboterdesign ,,Mybot'' aus Korea von 2018 liegt der Fokus auf Simulation von Emotionen. Zur Gesichtsdetektion wird erfolgreich der in OpenCV integrierte Haar-Algorithmus angewandt \cite{robotik:2018_social_relationship_face_reco}.

Ein 2017 herausgebrachtes White Paper von Intel \ac{AI} zeigt, dass die Anwendung von \acp{KNN} (u.A. GoogLeNet, VGGFace, AlexNet) auf dem Raspberry Pi 3B nicht ohne weitere Hardware praktikabel ist. Die Netze werden außer auf dem Raspberry Pi auch auf Intels \ac{NCS} sowie dem Intel Joule implementiert. Der Joule zeigt vor dem \ac{NCS} und an letzter Stelle dem Raspberry Pi die schnellsten Verarbeitungszeiten. Caffe soll das energetisch effizienteste Framework sein. Mit Hilfe des \ac{NCS}, der Caffe und TensorFlow Modelle unterstützt, ist es möglich, Bilder mit 4 \ac{FPS} direkt zu verarbeiten. Die Aussage, \acp{FPGA} sollen weniger effizient als Intels eigener \ac{NCS} sein, ist auf Grund Intels Autorenschaft potenziell kritisch zu betrachten \cite{robotik:2017_cnn_benchmark_libraries}.

Als rechenschwächste aller hier genannter Plattformen stellt der Arduino Uno (Rev.3) den Autoren der 2014 erschienenen Veröffentlichung ,,OpenCV WebCam Applications in an Arduino-based Rover'' eine Herausforderung dar. Der fahrbare Roboter erhält einen zusätzlichen, ähnlich dem Raspberry Pi, auf \ac{ARM} basierenden Mini-PC mit Linux-Betriebssystem für Webcam-unterstützte Erkennungsaufgaben, auch Gesichtsdetektion (implementiert mit Hilfe von OpenCVs integriertem Haar-Detektor). Ein Fazit der Arbeit ist, dass viele Hürden einfach mit zusätzlicher Rechenkapazität genommen werden können, so wird auch für zukünftige Forschung vorgeschlagen, stärkere Algorithmen in Kombination mit rechenstärkerer Hardware einzusetzen. Die Ergebnisse der Gesichtsdetektion fallen knapp aus; Belichtungsverhältnisse zeigen sich als Problemfaktor, ebenso wie entschieden werden kann, welche Frames aus dem kontinuierlichen Bild-Stream zur Erkennung genutzt werden sollen \cite{robotik:2014_arduino_opencv_rover}.

Die Relevanz der soeben aufgelisteten Forschungsarbeiten zu dieser Thesis ist unterschiedlich groß. Die vielleicht themenverwandteste Arbeit ist die erwähnte katalanische Bachelorarbeit von 2018. Gemeinsamkeiten sind:
\begin{itemize}
	\item Die zu implementierende Gesichtsdetektion und -wiedererkennung,
	\item die Plattform Raspberry Pi 3 B+ (als Analogon zu einem Hardware-technisch verwandten Roboter) sowie
	\item die Auslagerung der \ac{KI}-Algorithmen auf eine externe Plattform.
\end{itemize}

\begin{table}[!ht]
	\begin{tabular}{|l|l|l|}
		\hline
		\textbf{Thematik} & \textbf{Kriz} & \textbf{Sevilimedu} \\ \hline
		\begin{tabular}[c]{@{}l@{}}Fokus der \\ Implementierung\end{tabular} & \begin{tabular}[c]{@{}l@{}}Algorithmen, \\ Sicherheit, Performanz\end{tabular} & kein Fokus \\ \hline
		\begin{tabular}[c]{@{}l@{}}Wirtschaftliche\\ Betrachungen\end{tabular} & kein Fokus & \begin{tabular}[c]{@{}l@{}}materielle und personelle\\ Aufwandsabschätzung\end{tabular} \\ \hline
		Projektplanung & keine & \begin{tabular}[c]{@{}l@{}}GANTT-Chart, Teilziele, \\ Zeit- und Ressourcenabschätzung\end{tabular} \\ \hline
		\begin{tabular}[c]{@{}l@{}}Sicherheit, \\ Datenschutz\end{tabular} & \begin{tabular}[c]{@{}l@{}}relevant für die \\ Durchführung der Studie\end{tabular} & kein Fokus \\ \hline
		\begin{tabular}[c]{@{}l@{}}Übertragbarkeit \\ auf Robotik\end{tabular} & Pepper; Bibliotheksroboter & \begin{tabular}[c]{@{}l@{}}CASPER-Projekt Roboter\\ für Kinder mit Autismus\end{tabular} \\ \hline
		Hardware & \begin{tabular}[c]{@{}l@{}}Raspberry Pi 3B+,\\ für KI-Anwendungen\\ optimierter Server\end{tabular} & \begin{tabular}[c]{@{}l@{}}Raspberrry Pi 3B, \\ Laptop ASUS N55SF\end{tabular} \\ \hline
	\end{tabular}
	\caption{Vergleich der Fokussierung dieser Thesis und der Arbeit von Sevilimedu \cite{robotik:2018_robo_social_vision_embedded_external}.}
	\label{tab:vergleich_bachelorarbeit}
\end{table}

Wo inhaltliche Differenzen auftreten, veranschaulicht Tabelle \ref{tab:vergleich_bachelorarbeit}. Vorgeschlagene Themen für zukünftige Forschung werden in dieser Thesis (je nach Auslegung) aufgegriffen: die Nutzung des stärkeren Raspberry Pi 3B+ und Optimierung des \ac{KI}-Modells \cite{robotik:2018_robo_social_vision_embedded_external}. Auch werden hier Themen erörtert, die nicht im Fokus anderer Arbeiten stehen (z.B. \ac{IT}-Sicherheit, Perspektive aus der Softwareentwicklung).
