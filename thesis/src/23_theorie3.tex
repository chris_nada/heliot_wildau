
\subsubsection{Kommunikation} 
\label{kap:kommunikation}

Da es sich bei dem in dieser Arbeit erörtertem System um eines mit Klient/Server-Architektur handelt, ist es essenziell, eine Schnittstelle zur Kommunikation zu erarbeiten. 

Dabei muss man sich nicht nur Gedanken über die technische Implementierung machen, sondern auch darüber, wie der Kommunikationsprozess ablaufen soll.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\linewidth]{grafiken/ma_kommunikationsprozess.png}
    \caption[Möglichkeiten zur Aufteilung der Algorithmen auf Klient und Server.]{Möglichkeiten zur Aufteilung der Algorithmen auf Klient und Server.\newline Abkürzungen: Detektion (Det.), Extraktion des Feature-Vektors (Ext.), Erkennung durch Kosinusabgleich mit den Lerndaten (Kos.).}
    \label{fig:kommunikationsprozess}
\end{figure}

Abbildung \ref{fig:kommunikationsprozess} stellt die möglichen Verteilungen der Teilalgorithmen einer Merkmal-basierten Gesichtserkennung auf Klient und Server dar:

\begin{itemize}
    \item In Fall A wird das von der Kamera des Klienten aufgenommene Bild unbearbeitet an den Server gesendet. Dieser führt alle zur Klassifikation nötigen Schritte aus:
    \begin{enumerate}
        \item Detektion des Gesichts, 
        \item Eingabe des ausgeschnittenen Gesichts in den Merkmalextraktor (\ac{KNN}) zur Erzeugung des Feature-Vektors und schließlich
        \item Klassifikation durch Abgleich der Kosinusdistanz zu den Lerndaten.
    \end{enumerate}
    \item In Fall B wird das Gesicht bereits vom Klienten aus dem Gesamtbild ausgeschnitten und an den Server übermittelt. Ein Gesichtdetektionsalgorithmus wird von Seiten des Klienten benötigt.
    \item In Fall C wird das vom Klienten ausgeschnittene Bild in denselben Gesichtserkennungsalgorithmus mit Merkmalextraktion gegeben, der sich auch auf dem Server befindet, um bereits einen Feature-Vektor zu erzeugen, der an den Server übermittelt wird zur Klassifikation.
\end{itemize}

Die Szenarien unterscheiden sich folglich darin, welcher Algorithmus auf welchem System ausgeführt wird und welche Daten übermittelt werden. Im Prinzip findet hier ein Tausch Rechenkapazität vs. Netzwerkkapazität statt. In Fall A findet der stärkste Netzwerkverkehr statt; in Fall C der schwächste; in Fall A wird die Rechenkapazität des Klienten am geringsten beansprucht und in Fall C am meisten. Fall B liegt bei beiden Variablen dazwischen.

\paragraph{Datengröße}\mbox{}

Ein unbearbeitetes Bild aus der Kamera des Raspberry Pi hat eine maximale Auflösung von 2592x1944 Pixel. Bei 8 Bit pro \ac{RGB}-Farbkanal ergibt sich daraus eine Dateigröße von über 14 MiB.\footnote{$2592px * 1944px * 8 Bit * 3 = 120.932.352 Bit.$} Ein Streamen in dieser Auflösung allein kann ein hohes Maß an Rechenkapazität beanspruchen, weshalb sich eine Reduzierung der Auflösung empfiehlt. Zusätzlich zur Aufnahme wird vom Raspberry Pi auch eine Bildkompression durchgeführt, gewöhnlicherweise in das \ac{JPEG}-Format \autocite{unsortiert:raspberry_pi_cookbook__software_hardware}.\footnote{JPG und JPEG werden synonym gebraucht.} Ein unkomprimiertes Bild von 1 MB Größe kann durch diese Kompression oft auf 50 KB gekürzt werden, womit \ac{JPEG} eine der stärksten, geläufigen, allerdings auch eine verlustbehaftete Bildkompression darstellt. Sie basiert auf der Huffman-Entropiekodierung von 1952, die insbesondere bei Bildern mit niedriger Shannon-Entropie hohe Leistungen erzielen kann, und ist 1994 als \ac{ISO} 10918-1 standardisiert worden. Wenn die verlustbehaftete Kompression (vgl. Abbildung \ref{fig:png_vs_jpeg}) durch \ac{JPEG} sich problematisch darstellt, kann das \ac{PNG}-Format eine valide Alternative sein, welches einen verlustfreien Kompressionsalgorithmus nutzt, der auf Reduzierung des Farbraums auf im Bild verwendete Farben sowie Huffman-Entropiekodierung basiert. Zusätzlich bietet das Format eine eingebaute Integritätsprüfung mittels \ac{CRC} sowie einen optionalen, vierten, transparenten Farbkanal (RGBA, \textit{A} steht dabei für \textit{Alpha}). Im Schnitt sind \acp{PNG} durch diese zusätzlichen Merkmale deutlich größer als \acp{JPEG} \autocite{buecher:1999_compressed_image_file_formats}. Beide Formate werden von OpenCV unterstützt \autocite{buecher:2017_learning_opencv_3}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\linewidth]{grafiken/Comparison_of_JPEG_and_PNG.png}
    \caption[Beispiel verlustbehafteter Kompression.]{Beispiel verlustbehafteter Kompression mit Bildartefakten (\ac{JPEG}) vs. verlustfreie Kompression (\ac{PNG}). (Bildquelle: \url{https://upload.wikimedia.org/wikipedia/commons/a/a4/Comparison_of_JPEG_and_PNG.png} Lizenz: GPLv2.)}
    \label{fig:png_vs_jpeg}
\end{figure}

Im Fall C, in dem kein eigentliches Bild übertragen wird, entspricht die Größe des Merkmalvektors beispielsweise beim VGG-Algorithmus mit 1024 Features der eines Arrays von 1024 Gleitkommazahlen \cite{man1:vgg_face}. Würden dafür der \ac{IEEE}-754-Double-Precision-Standard mit 64 Bit Genauigkeit genutzt werden, wäre der gesamte Array 8 KiB lang, was zum Vergleich einem quadratischen Bild von etwa 52x52 Pixel ohne Kompression bei 24 Bit \ac{RGB}-Farbraum entspräche.

\newpage
\paragraph{Aufteilung der Algorithmen}\mbox{}

Sowohl Detektion als auch Gesichtserkennung wurden vom Standpunkt der Performanz in den vorangegangenen Kapiteln (\ref{kap:performanz_theorie}, \ref{kap:frameworks}) beleuchtet. Je nach Algorithmus kann sich hier eine unterschiedliche Aufteilung ergeben. 

Genau diese Thematik, die Aufteilung der Algorithmen auf Server und Klienten, ist gleichzeitig der Kern des relativ neuartigen Konzepts des Fog-Computing (auch Edge-Computing), das ab etwa 2010 aufkam. Dieses ist aus der Problematik heraus entstanden, dass (insbesondere \ac{IoT}-) Geräte große Datenmengen an Server zur Auswertung schicken und diese Datenmengen auf Grund der stark anwachsenden Zahl an verbundenen Geräten (vgl. Abbildung \ref{fig:iot_prognose}) selbst mit aktueller Infrastruktur ihre Grenzen erreichen bzw. in absehbarer Zukunft erreichen werden, woraus sich die Motivation ergibt, durch die Verlagerung von Teilalgorithmen (zur Datenanalyse) die schnell steigende Rechenkapazität der Geräte auszunutzen, um langsam wachsende Netzwerkkapazitäten zu entlasten \autocite{man1:fog_computing}.

\paragraph{Hardware-Schnittstellen}\mbox{}

Der Raspberry Pi verfügt über drei Hardware-Schnittstellen, die zur Datenübertragung genutzt werden können:

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\linewidth]{grafiken/ma_raspi_ics.jpg}
    \caption[Raspberry Pi 1B+ mit Elektronik-Peripherie.]{Raspberry Pi 1B+ mit Elektronik-Peripherie. A = Raspberry Pi 1B+; B = SIM800L; C = 5V/3,3V Spannungswandler; D = Funkentstörkondensator; E = ESP8266; F = nRF24L01 mit Antenne.}
    \label{fig:raspi_ics}
\end{figure}

\begin{enumerate}
    \item Das \ac{GPIO} (Abb. \ref{fig:raspi_ics}), welches beim Raspberry Pi 1 über \ac{PWM} mit fester Frequenz von 19,2 MHz an Pin 12 (bzw. GPIO18) verfügt. Spätere Raspberry-Pi-Modelle (1A+ sowie 2 aufwärts) verfügen über ein zweites \ac{PWM}-fähiges \ac{GPIO} an Pin 33 (bzw. GPIO13) \autocite{unsortiert:exploring_raspberry_pi}. Das \ac{GPIO} kann für eine ganze Reihe von Kommunikationsschnittstellen zur Datenübertragung genutzt werden, viele davon sind nicht vom Typ \ac{LAN} / \ac{WLAN}. Das Modul SIM800L \cite{man1:SIM800L} z.B. ermöglicht Kommunikation über Mobilfunknetze (SMS, Daten, Anrufe) oder das Funkmodul nRF24L01 (Abb. \ref{fig:raspi_ics}) für Übertragung von Daten im Frequenzband 2,400 GHz bis 2,4835 GHz \cite{man1:nrf24}.
    
    \item Bluetooth und \ac{WLAN}-fähig sind die Modelle ab 3 über \acp{IC} von Broadcom. Raspberry Pis ohne eingebautes \ac{WLAN} können nachgerüstet werden. Das ist beispielsweise möglich über ein \ac{USB}-\ac{WLAN}-Dongle oder ein \ac{WLAN}-Modul, z.B. das verbreitete ESP8266 (Abbildung \ref{fig:raspi_ics}), welches sich über \ac{GPIO} mit dem System verbinden lässt \autocite{unsortiert:beginning_robotics_with_raspberry,man1:esp8266}. Zu beachten ist, dass einige solcher \ac{GPIO}-Module (wie auch das ESP8266) nicht mit der Raspberry-Pi-Ausgangsspannung von 5V betrieben werden dürfen, sondern mit 3,3V, weshalb sie einen logischen Spannungswandler benötigen, den es z.B. als integrierte Schaltung SN74LV1T34 \cite{man1:SN74LV1T34} gibt, falls das Raspberry Pi über keinen eigenen 3,3V Ausgang verfügt. Alternativ kann man natürlich auch eine eigene Spannungswandlerschaltung implementieren. Im Gegensatz zum eingebauten \ac{WLAN}-\ac{IC} von Broadcom lässt sich bei diesen Modulen die Reichweite z.B. über physische Schirmung, eine angepasste Antenne sowie eine Schaltung zur Spannungsglättung (z.B. mittels Funkentstörkondensator, Abbildung \ref{fig:raspi_ics}) vergrößern \cite{buecher:electronics_kamins}.
    
    \item Alle Raspberry Pis (außer die Zero- und A-Versionen) verfügen über eine kabelgebundene RJ45-Netzwerkschnittstelle \cite{unsortiert:raspberry_pi_cookbook__software_hardware}.
\end{enumerate}

\paragraph{Software-Schnittstellen}\mbox{}

Um mit einem Server im Netzwerk zu kommunizieren, existieren für die Schnittstellen \ac{WLAN} und RJ45 \ac{LAN} standardisierte Software-Implementierungen, wie z.B. \ac{TCP} \cite{man1:tcp} oder \ac{UDP} \cite{man1:udp}. Andere Schnittstellen wie Funk, \ac{GSM} oder Bluetooth bräuchten eine Art Vermittlerstation, die die Daten vom Raspberry Pi mit entsprechender Hardware-Schnittstelle in beide Richtungen empfangen, umwandeln und weiterleiten kann an einen im Netzwerk befindlichen Server. Sowohl \ac{TCP} als auch \ac{UDP} sind nach dem \ac{OSI}-Modell auf der Transportschicht (Schicht 4) angesiedelt. Über ihr abstrahiert liegen anwendungsorientierte Schichten, wie \ac{HTTP} bzw. \ac{HTTPS}, während darunter die Netzwerk- oder Paketschicht \ac{IP} auf Ebene 3 liegt, die \ac{WLAN}- und \ac{LAN}-Implementierungen auf dem Data-Link-Layer (2) mit der physikalischen Schicht (1) an unterster Stelle. \autocite{man1:osi,buecher:2003_unix_network_programming_sockets_api}. 
\\
\paragraph{Sicherheit}\mbox{}
\label{kap:sicherheit}

Würde man sich dazu entscheiden, eine Datenübertragung auf Socket-Ebene implementieren zu wollen, hätte man die Auswahl zwischen \ac{TCP} und \ac{UDP}, während man bei der Wahl von \ac{HTTP} bzw. \ac{HTTPS} (d.h. \ac{REST}) auf \ac{TCP} (mehr oder weniger) angewiesen ist. Eine \ac{REST}-Implementierung über \ac{UDP} ist zwar standardisiert worden als RFC 7252 \cite{man1:rest_udp}, wird aber wegen der Unzuverlässigkeit von \ac{UDP} kaum gebraucht. Es gibt hier auch weitere \ac{UDP}-bedingte Tücken wie die nicht garantierte Paketsendereihenfolge zu beachten, die für Programmierer unbequem sein könnten \cite{buecher:2002_http_the_definitive_guide}. Ein weiteres Problem bei Kommunikation auf Socket-Ebene ist, dass Sicherheit selbst implementiert werden müsste, während \ac{REST} über \ac{HTTPS}, solange es korrekt eingesetzt wird, eine Ende-zu-Ende-verschlüsselte Verbindung mittels \ac{SSL} bzw. dessen Nachfolger \ac{TLS} einschließt, die resistent gegenüber Man-in-the-Middle-Angriffen (im Deutschen auch Janusangriffe genannt) sind. Bei Kommunikation über Funkkanäle, zu der auch \ac{WLAN} zählt, sind solche Angriffe insbesondere problematisch, da Angreifer sich nur in Reichweite eines sendenden Geräts befinden müssen, um eine unverschlüsselte Übertragung in Klartext mitzuhören. Wichtig zu wissen ist auch, dass die Standards \ac{SSL} und \ac{TLS} nur vorschreiben, auf welche Art und Weise der Schlüsselaustausch sowie anschließend der Nachrichtenaustausch bei der symmetrischen Ende-zu-Ende-Verschlüsselung vonstattengehen, nicht aber, mit welchen genauen kryptographischen Verfahren zu verschlüsseln ist. Es sind hier sowohl Block- als auch Stromchiffren denkbar. Eine Quasi-Weiterentwicklung der symmetrischen Ende-zu-Ende-Verschlüsselung ist die \textit{authenticated Encryption}, bei der die Kommunikation nicht nur Ende-zu-Ende-verschlüsselt ist, sondern sich die beiden teilnehmenden Kommunikationspartner gegenseitig authentifizieren müssen, d.h. beweisen müssen, dass sie auch sind, wer sie vorgeben zu sein \cite{buecher:2015_bulletproof_ssl_and_tls}.

Ein bei ausreichender Schlüssellänge (> 128 Bit) immer noch als sicher geltender Klassiker der Blockverschlüsselung von 2001 ist der ursprünglich vom \ac{NIST} standardisierte \ac{AES}, auch bekannt als \textit{Rinjdael} \cite{buecher:websecurityprivacycommerce2nd,man1:aes}. Trotz seines Alters konnten noch keine praktisch anwendbaren Attacken realisiert werden. Sicherheitsexperten bezeichnen (vereinfacht gesagt) Angriffe auf \ac{AES} als \textit{,,eine der schwierigsten Herausforderungen in der Kryptoanalyse von Blockchiffren seit mehr als einem Jahrzehnt''}.\footnote{Originaltext: \textit{,,Since Rijndael was chosen as the Advanced Encryption Standard, improving upon 7-round attacks on the 128-bit key variant or upon 8-round attacks on the 192/256-bit key variants has been one of the most difficult challenges in the cryptanalysis of block ciphers for more than a decade.''} \cite{man1:aes_cryptanalysis}.} Beachtenswert dabei ist, dass nicht Angriffe etwa auf den 'vollständigen' \ac{AES}-Algorithmus gemeint sind, sondern nur die ersten 7 bzw. 8 Runden von insgesamt 10 (128 Bit), 12 (192 Bit) oder 14 (256 Bit) \cite{man1:aes_cryptanalysis}.

Eine wichtige, für Softwareentwickler essenziell zu beachtende Neuerung in der Kryptographie ist die Weiterentwicklung von Hash-Algorithmen zu spezialisierten Passwort-Hash-Algorithmen. Hashfunktionen sind Einwegfunktionen. Der ideale Hash-Algorithmus ist das sogenannte 'Zufallsorakel', welches eine beliebige Eingabe in eine einmalige Ausgabe umwandelt, wobei gleiche Eingaben immer zu derselben Ausgabe führen, unterschiedliche Eingaben jedoch niemals. Die Eigenschaft, dass gleiche Ausgaben praktisch nicht auftreten dürfen, heißt Kollisionsresistenz. Hash-Algorithmen werden für eine Vielzahl von Anwendungsfällen genutzt, wie z.B. Datenintegritätsprüfung, Prüfsummen, Cache-Verfahren, Hash-Tables / Hash-Trees, Passwortspeicherung und elektronische Zertifikate. Idealerweise lassen sich aus der Ausgabe einer Hashfunktion keinerlei Erkenntnisse über die Eingabe finden, d.h. die Ausgabe sollte pseudo-zufällig sein. Einige Hash-Algorithmen, wie \ac{SHA}-1 oder MD5 gelten als nicht mehr sicher, weshalb die Herausgeber von Webbrowsern diese schon seit einigen Jahren nicht mehr unterstützen. Warum sind 'geknackte' Hashfunktionen gefährlich? Würde bspw. ein Passwort in einer Datenbank mit einem unsicheren Hash-Algorithmus gespeichert werden und ein Angreifer gelangt an den in der Datenbank gespeicherten Ausgabewert der Hashfunktion, könnte er leicht eine Eingabe finden, die genau diesen Ausgabewert generiert und sich somit Zugang zum System verschaffen, ohne das eigentliche Passwort herausgefunden zu haben. Ein berühmtes Beispiel ist MD5, für den gleich mehrere verschiedene Angriffe existieren, mit derer Hilfe sich leicht Kollisionen finden lassen \autocite{man1:hashing,man1:md5attack}.

Trotz des Mitte der 2000er Jahre stark erschütterten Vertrauens in das \ac{NIST}, welches wiederholt mit der \ac{NSA} zusammenarbeitete und für sie Hintertüren z.B. in den auf elliptischen Kurven basierenden Zufallszahlengenerator {Dual\_EC\_DRBG}, standardisiert als {NIST SP 800-90A}, einbaute \cite{man1:nsa_backdoor}, sind die von der \ac{NIST} standardisierten \acp{SHA} die derzeit gebräuchlichsten und mit dem 2012 veröffentlichtem \ac{SHA}-3 \cite{man1:sha3} als sehr sicher geltend \cite{man1:sha3atk2019}.

In jüngster Zeit (Mitte der 2010er Jahre) hat es sich gezeigt, dass herkömmliche Hashfunktionen sowie die erste Generation der spezialisierten Passwort-Hashs anfällig gegenüber Angreifern sind. Der Hauptgrund dafür ist die große Menge ihnen zur Verfügung stehender und sich ständig steigernder Rechenleistung, die insbesondere von auf das Knacken von Passwort-Hashs zugeschnittenen \acp{GPU}, \acp{FPGA} und \acp{ASIC} stammt. Speziell die zur ersten Generation gehörende und weit verbreitete \ac{PBKDF2} gilt als anfällig. Die auf Basis dieser Tatsachen 2013 ins Leben gerufene \textit{Password Hashing Competition} endete 2015 mit der Verkündung des quelloffenen Argon2 als Gewinner \autocite{man1:phc_survey}. Im Gegensatz zu herkömmlichen Hashfunktionen, die oft auch auf eine schnelle Erzeugung der Ausgabe optimiert sind, ist bei Passwort-Hashfunktionen genau das Gegenteil der Fall. Diese Algorithmen sind absichtlich komplex in der Berechnung der Ausgabe, sodass es sogar mehrere Sekunden dauern kann, einen einzelnen Digest (Ausgabe) zu erzeugen. Der Argon2 orientiert sich in Sachen Anpassungsfähigkeit möglicherweise am \ac{SHA}-3: Beide verfügen über variable Ausgabenlänge, während beim Argon2 zusätzlich noch Dinge wie Grad der Parallelisierung oder gewünschter Speicherbedarf konfigurierbar sind \autocite{man1:argon2}. 

Vergleicht man die Veröffentlichungen zu der Ausschreibung des \ac{SHA}-3 und des Passwort-Hash-Wettbewerbs von 2013, könnte man an letzterem kritisieren, dass die teilnehmenden Algorithmen ein viel geringeres Maß an Kryptoanalyse erfahren haben als es bei den Kandidaten der \ac{NIST}-Ausschreibung der Fall war.

\newpage
\paragraph{REST}\mbox{}

Die bei der Kommunikation von Klient und Server zur Gesichtserkennung vielleicht wichtigsten Vorgänge sind

\begin{itemize}
    \item die Abfrage einer Klassifikation,
    \item das Registrieren neuer Teilnehmer sowie
    \item das Hinzufügen von Lerndaten.
\end{itemize}

Es sind weitere Abfragen denkbar, diese stellen aber eine geschlossene Grundstruktur dar, durch die das System vollständig nutzbar wäre.

Wie lassen sich diese Server-Anfragen als \ac{REST}-Anfragen formulieren?

Um diese Frage zu beantworten, müssen zuerst die grundlegenden \ac{REST}-Befehle geklärt werden:

\begin{itemize}
    \item GET fragt via \ac{URI} eine Ressource an, die an den Klienten übermittelt wird,
    \item PUT sendet eine Ressource vom Klienten an den Server,
    \item DELETE löscht eine Ressource auf dem Server,
    \item POST sendet Daten an den Server, um dort eine Ressource zu erzeugen \cite{buecher:2002_http_the_definitive_guide}.
\end{itemize}

Für die Implementierung einer \ac{REST}-Schnittstelle wird empfohlen, sich strengstens an den Standard zu halten, um beim Design einer \ac{REST}-\ac{API} unerwartetes Verhalten und damit Fehler bei einem auf das System zugreifenden Programm zu vermeiden \cite{buecher:2012_rest_api_design_rulebook}.

Normalerweise wäre die Abfrage einer Klassifikation eine GET-Anfrage. Diese könnte jedoch nur verwendet werden, wenn das zu klassifizierende Gesicht (Bilddaten) dem Server bekannt ist. Hierfür gibt es zwei denkbare Lösungen: Entweder wird das Bild in einer separaten Abfrage, z.B. PUT zuvor an den Server übermittelt oder die Daten werden in die GET-Anfrage eingebettet. Die letztere Lösung ist nicht zu empfehlen, da sie im Prinzip gegen den Standard verstößt, da GET-Anfragen keinen Request-Body enthalten. D.h. Daten müssten in die Request-\ac{URI} geschrieben werden, was sie Angreifern offenlegen würde (und außerdem ebenfalls gegen den Standard verstößt). Request-Addressen sind auch bei \ac{HTTPS} streng genommen nicht Ende-zu-Ende-verschlüsselt. Eine PUT-Anfrage mit Klassifikation in der Server-Response stellt möglicherweise einen Kompromiss aus Standardkonformität und Bedienbarkeit dar.

Das Registrieren neuer Nutzer sowie das Übermitteln von Lerndaten könnte mit hoher Standardkonformität als PUT-Request implementiert werden, da es im Prinzip einseitige Kommunikation von Klient zu Server darstellt, wobei sich eine einfache Antwort in Form eines 201 (,,in Ordnung, Ressource wurde erzeugt'') oder 409 (,,Konflikt'') anbietet \cite{buecher:2002_http_the_definitive_guide}. Streng nach Standard müssen an den Server via PUT gesendete Ressourcen an der von der \ac{URI} angegebenen Position verharren \cite{buecher:2010_restful_web_services_cookbook}. Da das Hauptklientel von \ac{REST}-Services aus Webbrowsern besteht, kann man sicherlich überlegen, inwiefern man den Standard für seine eigenen, abweichenden Zwecke anpasst.


%\subsection{Hardware}
%\label{kap:hardware}
%% Raspberry Pi als Plattform
%% S ( Theorie zu Robotik / Pepper Plattform)
%% S ( Theorie zu sozialer Interaktion)
%% Vergleich RasPi / Pepper
%% DSGVO
