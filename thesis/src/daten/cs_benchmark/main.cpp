#include "cs.hpp"
#include <iostream>
#include <array>
#include <random>
#include <chrono>

int main() { // int argc, char** argv

    // Initialisierung
    const size_t vektor_groesse  = 1024;
    const size_t anzahl_vektoren = 1000;
    std::array<std::vector<double>, anzahl_vektoren> vektoren;
    std::vector<double> ergebnisse;
    ergebnisse.reserve(anzahl_vektoren * anzahl_vektoren);

    // Zufallsgenerator
    std::uniform_real_distribution<double> urd(-1, 1);
    std::default_random_engine dre(time(nullptr));

    // Zufaellige Vektoren erzeugen
    for (size_t i = 0; i < anzahl_vektoren; ++i) {
        std::vector<double> v(vektor_groesse);
        for (size_t k = 0; k < v.size(); ++k) v[k] = urd(dre);
        vektoren[i] = v;
    }

    // Cosinus-Aehnlichkeiten berechnen + Zeit stoppen
    const auto timer_start = std::chrono::steady_clock::now();
    for (const auto& v1 : vektoren) {
        for (const auto& v2 : vektoren) {
            const double x = cs(v1, v2);
            ergebnisse.push_back(x);
        }
    }
    const auto timer_ende = std::chrono::steady_clock::now();
    const double dauer_ms_gesamt = std::chrono::duration_cast<std::chrono::milliseconds>
            (timer_ende - timer_start).count();

    // Benchmark ausgeben
    std::cout << "Aufrufe      = " << ergebnisse.size() << '\n';
    std::cout << "Dauer        = " << (dauer_ms_gesamt / 1000) << "s\n";
    std::cout << "Dauer / cs() = " << (dauer_ms_gesamt / ergebnisse.size()) << "ms\n";

    // So tun als ob Ergebnisse genutzt werden,
    // damit sie nicht wegoptimiert werden.
    for (const auto& e : ergebnisse) {
        if (e == 0) std::cout << std::flush; // quasi nichts tun
    }
}
