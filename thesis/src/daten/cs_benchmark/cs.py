import random
import time

import numpy

# Kosinusaehnlichkeit
def cs(v1, v2):
    x = numpy.dot(v1[0], v2[0]) / (numpy.linalg.norm(v1[0]) * numpy.linalg.norm(v2[0]))
    return x

def benchmark():

    # Initialisierung
    arrays = []
    vektor_groesse  = 1024
    anzahl_vektoren = 1000
    ergebnisse = []

    # Vektoren zufaellig erzeugen
    for i in range(anzahl_vektoren):
        A = numpy.zeros(vektor_groesse)
        for n in range(len(A)):
            A[n] = random.uniform(-1.0, 1.0)
        arrays.append(A)

    # Kosinusaehnlichkeiten + Timen
    timer_start = time.time()
    for v1 in arrays:
        for v2 in arrays:
            ergebnis = cs(v1, v2)
            ergebnisse.append(ergebnis)
    timer_ende = time.time()

    print("Aufrufe      =", len(ergebnisse))
    print("Dauer        =", (timer_ende-timer_start), "s")
    print("Dauer / cs() =", (1000 * (timer_ende-timer_start) / len(ergebnisse)), "ms")

benchmark()
