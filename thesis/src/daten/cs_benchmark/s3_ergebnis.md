## C++

### GCC 8.3
-O3 -s mtune=generic
```
Aufrufe      = 1000000
Dauer        = 0.856s
Dauer / cs() = 0.000856ms
```


## Python

```
Aufrufe      = 1000000
Dauer        = 8.263441801071167 s
Dauer / cs() = 0.008263441801071168 ms
```
