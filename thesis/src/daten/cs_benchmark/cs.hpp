#include <cstddef>
#include <cmath>
#include <vector>

double cs(const std::vector<double>& v1, const std::vector<double>& v2) {
    const size_t n = v1.size();
    double zaehler = 0.0; 
    double nenner1 = 0.0;
    double nenner2 = 0.0;

    // Schleife berechnet Zaehler und Nenner
    for (size_t i = 0; i < n; ++i) {
        zaehler += v1[i] * v2[i] ;
        nenner1 += v1[i] * v1[i] ;
        nenner2 += v2[i] * v2[i] ;
    }
    return zaehler / (sqrt(nenner1) * sqrt(nenner2)) ;
}
