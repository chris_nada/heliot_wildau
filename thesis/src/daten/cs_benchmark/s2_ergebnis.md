## C++

### GCC 8.3
-O3 -s mtune=generic
```
Aufrufe      = 1000000
Dauer        = 1.376s
Dauer / cs() = 0.001376ms
```
-O3 -s march=native
```
Aufrufe      = 1000000
Dauer        = 1.308s
Dauer / cs() = 0.001308ms
```

### GCC 7.4
-O3 -s mtune=generic
```
Aufrufe      = 1000000
Dauer        = 1.407s
Dauer / cs() = 0.001407ms
```
-O3 -s march=native
```
Aufrufe      = 1000000
Dauer        = 1.313s
Dauer / cs() = 0.001313ms
```

## Python

```
Aufrufe      = 1000000
Dauer        = 17.49910306930542 s
Dauer / cs() = 0.01749910306930542 ms
```
