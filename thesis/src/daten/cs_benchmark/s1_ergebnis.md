## C++

### GCC 7.4

```
Aufrufe      = 1000000
Dauer        = 3.654s
Dauer / cs() = 0.003654ms
```

## Python

```
Aufrufe      = 1000000
Dauer        = 46.70897912979126 s
Dauer / cs() = 0.04670897912979126 ms
```
