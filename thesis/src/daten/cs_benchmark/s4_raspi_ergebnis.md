## Verbrauch

+ ausgeschaltet 1 W
+ Leerlauf, keine Peripherie 3,4 W; 3,8 W
+ mit Display 7,1 W
+ mit Display, Normale Nutzung 8,4 W
+ mit Display, stressberry 8,1 W; 10,1 W
+ mit Display, Stresstest (stress-ng) 11,4 W
+ mit Display, Wifi 8,1 W bis 8,9 W 
+ cs 9,7 W

## C++

### GCC 6.3
Vektor-const-ref; -O3 -s
```
Aufrufe      = 1000000
Dauer        = 5.131s
Dauer / cs() = 0.005131ms
```

Vektorkopie; -O3 -s
```
Aufrufe      = 1000000
Dauer        = 13.717s
Dauer / cs() = 0.013717ms
```

## Python

```
Aufrufe      = 1000000
Dauer        = 202.19166898727417 s
Dauer / cs() = 0.20219166898727417 ms
```

