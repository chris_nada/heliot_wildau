#!/usr/bin/gnuplot

# I/O
set term png lw 2
set datafile separator ";"
set key autotitle columnhead

# Plot Konfiguration
set grid
set xlabel "Iteration"
#set key outside
unset key

# Plot Loss
set output "loss.png"
set ylabel "Loss"
plot "out_iter.csv" using 1:3 with line lw 0.5 lc rgb "#666666"

set output "accuracy.png"
set ylabel "Accuracy"
plot "out_test.csv" using 1:2 with line lw 1.0 lc rgb "#666666"
