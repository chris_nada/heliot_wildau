# Einfacher Skript, um Caffes Konsolenausgaben als CSV zusammenzufassen.

def parse(dateiname):
    f = open(dateiname, "r")
    out_iter = open("out_iter.csv", "w")
    out_iter.write("Iteration;I/s;Loss\n")
    out_test = open("out_test.csv", "w")
    out_iter.write("Iteration;Accuracy\n")

    iteration = 0
    iter_p_s  = ""
    loss      = ""
    for zeile in f:
        zeile = zeile.replace(" ", "")
        zeile = zeile.replace("\n", "")

        # Anzahl Iteration parsen
        if "solver.cpp" in zeile and "Iteration" in zeile and "sgd_" not in zeile:
            zeile = zeile[zeile.index("Iteration") + len("Iteration"):]

            # 'Test'-Zeile
            if ",Test" in zeile:
                iteration = int(zeile[0 : zeile.index(",")])
            # Zeile enthält Loss-Information
            elif ",loss=" in zeile:
                iteration = int(zeile[0 : zeile.index("(")])
                loss      = zeile[zeile.find(",loss=") + len(",loss=") : ]
                iter_p_s  = zeile[zeile.find("(") + 1 : zeile.find("iter/s")]
                x = str(iteration) + ";" + iter_p_s + ";" + loss
                out_iter.write(x + "\n")
                print ("Loss", x)

        # Zeile enthält Accuracy-Information
        if "accuracy" in zeile:
            acc = zeile[zeile.rfind("=") + 1 : ]
            x = str(iteration) + ";" + acc
            out_test.write(x + "\n")
            print ("Test", x)
    f.close()
    out_test.close()
    out_iter.close()

parse("log.txt")
