\newpage
\section{Ergebnisse}

\subsection{Benchmark Kosinusdistanz}

Die Berechnung der Kosinusdistanz kann zum Einstieg als einfaches, aber sicherlich nicht allgemeingültiges Benchmark dienen, um alle genutzten Rechnersysteme zu vergleichen. Allgemeingültig ist es deshalb nicht, weil es auf Instruktionsebene sehr monoton beschaffen ist. Das zeigt sich deutlich, sobald man den von \ac{GCC} für x86\_64 kompilierten C++-Quelltext betrachtet. Der Algorithmus wird hauptsächlich durch den Body der for-Schleife dominiert, welcher \texttt{addsd}\footnote{Skalare Addition von zwei Double-Precision Gleitkommazahlen.} und \texttt{mulsd}\footnote{Skalare Multiplikation von zwei Double-Precision Gleitkommazahlen}, beides \ac{SIMD}-Instruktionen, in den XMM-Registern (\ac{SSE}) ausführt (Label .L3). Das \texttt{addq} gleich zu Anfang, welches 1 zum Register \texttt{rax} addiert, ist der Schleifeniterator.

{
	\scriptsize\linespread{1.0}\selectfont
	\lstinputlisting[frame=L]{daten/cs_benchmark/compiler_output_x86.txt}
}

Der gesamte Assemblerkode des Benchmarks steht Interessenten online zur Verfügung.\footnote{\url{https://gitlab.com/chris_nada/heliot_wildau/raw/master/thesis/src/daten/cs_benchmark/compiler_output.txt}}

Die Python-Version dieses Tests nutzt zur Berechnung ausschließlich NumPy-Aufrufe. Leider lässt sich hier nicht nachvollziehen, wie diese auf Instruktionsebene aussehen.

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.0\linewidth]{daten/cs_benchmark/b_dauer}
	\caption{Laufzeit des Kosinusdistanz-Benchmarks bei 1.000.000 Aufrufen und 1.024-dimensionalen Vektoren.}
	\label{fig:benchmark_kosinus_dauer}
\end{figure}

Für jeden einzelnen Test wurde 1.000.000 mal die Kosinusdistanz berechnet. Die Laufzeit des Benchmarks in C++ und Python für alle Systeme zeigt Abbildung \ref{fig:benchmark_kosinus_dauer}. Hier lässt sich klar der Leistungsunterschied der Systeme erkennen, der für beide Versionen des Benchmarks sich stark ähnelt. Für die Systeme gilt diesem Test nach die Leistungsabstufung S3 > S2 > S1 > S4. Bemerkenswert ist, dass das Benchmark keine Parallelisierung nutzt, nicht für die Architektur des modernen \acp{CPU} von S3 optimiert wurde und trotzdem dieses stark auf Parallelisierung spezialisierte System mit erkennbarem Abstand als Sieger hervorgeht. Unerwarteterweise ist die Python bzw. NumPy-Version des Benchmarks sogar auf dem schnellsten System (S3) langsamer als die in C++ kompilierte Version auf dem langsamsten System (Raspberry Pi alias S4). Der Schleifenkörper besteht im Prinzip nur aus einer einzigen Quelltextzeile NumPy:

\begin{quote}
	\scriptsize\linespread{1.0}\selectfont
	\begin{verbatim}
		x = numpy.dot(v1[0], v2[0]) / (numpy.linalg.norm(v1[0]) * numpy.linalg.norm(v2[0]))
	\end{verbatim}
\end{quote}

Warum trotz hocheffizientem \cite{man1:numpy} Numpy-Aufruf solch ein Unterschied besteht, kann ohne Analyse, wie der Kode vom Interpreter ausgeführt wird, nicht erklärt werden.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\linewidth]{daten/cs_benchmark/b_energieaufwand}
	\caption{Energieaufwand für die 1.000.000-fache Berechnung der Kosinusdistanz (Version: C++).}
	\label{fig:benchmark_kosinus_energie}
\end{figure}

Der schnellste Rechner ist nicht immer der energieeffizienteste Rechner. Das zeigt das Ergebnis der Stromverbrauchsmessung (veranschaulicht in Abbildung \ref{fig:benchmark_kosinus_energie}).\footnote{1 Ws = 1 J} Der Raspberry Pi (S4) schneidet hier am besten ab, gefolgt vom Laptop (S1). Verwunderlich ist dieses Ergebnis nicht, wenn man bedenkt, dass Raspberry Pis eine Zeit lang dank ihrer Energieeffizienz als Bitcoin-Miner lukrativ waren \cite{man1:raspi_webserver_bitcoin}. Trotz der von den verglichenen Systemen schnellsten Ausführung benötigt PC S2 einen mehr als doppelt so hohen Energieeinsatz wie Laptop S1 und einen über drei Mal so hohen wie der Raspberry Pi S4. Verglichen mit der C++-Version braucht die Python-Version auf den drei gemessenen Systemen etwa zwölffach soviel Energie. 

Für System 3 liegen keine energetischen Messungen vor.

\subsection{Gesichtsdetektoren}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.75\linewidth]{daten/plot_detektion_quali}
	\caption{Relative Anzahl erkannter Gesichter des CASIA-Webface-Datensatzes nach Detektortyp.}
	\label{fig:detektoren_qualitest}
\end{figure}

In Kapitel \ref{kap:gesichtdetektion} deutete sich bereits aus den Ergebnissen in der Literatur an, dass komplexere Algorithmen (\acp{KNN}) toleranter gegenüber Störeinflüssen bei der Gesichtsdetektion sind. Die mit den verwendeten Algorithmen durchgeführten Tests decken diesen Fakt, was Abbildung \ref{fig:detektoren_qualitest} veranschaulicht. Das neuronale Netz aus DLib schnitt mit einer Erkennungsrate von $99,86\%$ am besten ab. Von den nicht auf \ac{KNN} basierenden Algorithmen ist der \ac{HOG}-Detektor (ebenfalls aus DLib) mit $88,42\%$ der stärkste. Die drei zur frontalen Gesichtsdetektion vortrainierten Haar-Kaskaden aus OpenCV schnitten bei der Erkennnungsqualität am schlechtesten ab. Der beste dieser drei, im Quelltext als 'default' bezeichnet, erkennt knapp $\dfrac{3}{4}$ aller Gesichter; die beiden alternativen Versionen, 'alt' sowie 'alt2', nur etwa $\dfrac{2}{3}$. Wie sich gleich zeigen wird, haben diese qualitativ schwächeren Algorithmen trotzdem ihre Daseinsberechtigung. Vergleicht man nämlich die Laufzeitperformanz dieser Detektoren zeigt sich, dass die hohen Erkennungsraten sozusagen durch zusätzliche Rechenleistung erkauft werden.

\begin{figure}[!h]
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/plot_sbild_haar}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/plot_sbild_hog}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/plot_sbild_knn}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/plot_sbild_haar_typen}
	\end{subfigure}
	\caption{Laufzeitvergleich verschiedener Gesichtsdetektoren.}
	\label{fig:detektor_laufzeit}
\end{figure}

Veranschaulicht werden die Messungen in Abbildung \ref{fig:detektor_laufzeit}, wobei zu beachten ist, dass im Falle des \acp{KNN} S3 vermutlich nur schlechter als S2 abgeschnitten hat, da dieser Detektor umfangreich Parallelisierung durch \ac{BLAS} nutzt bzw. nutzen kann, dieses auf S3 zum Zeitpunkt der Messung aber nicht installiert war.

Die schnellsten Detektoren sind klar die Haar-Kaskaden, die ein Gesicht regelmäßig in weniger als einer hundertstel Sekunde in einem Bild erkennen können. Auffällig war bei der Untersuchung, dass selbst hier unterschiedlich trainierte Modelle (default, alt, alt2) vom selben Algorithmus unterschiedliche Rechenzeit aufweisen (Abbildung \ref{fig:detektor_laufzeit}, Haar-Modelle). Das langsamste Modell weist dabei die besten Detektionsraten auf (vgl. Abbildung \ref{fig:detektoren_qualitest}).

Der \ac{HOG}-Detektor kann mit Erkennungsraten von fast $90\%$ bei einer nicht um Größenordnungen langsameren Laufzeit (zirka 0,01 s bis 0,03 s) als die Haar-Kaskaden ein Kompromiss für schwächere Systeme sein.

Für das \ac{KNN} stand, wie bereits erwähnt, auf S3 leider kein \ac{BLAS} zur Verfügung. Somit können pro Sekunde auf den getesteten Systemen im besten Fall (S2) nur 4 bis 5 Bilder, im schlechtesten Fall (S1) etwas mehr als 2 Bilder erkannt werden.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.75\linewidth]{daten/plot_verbrauch_pro_bild}
	\caption{Durchschnittlicher Stromverbrauch pro Detektionsversuch bei Testdurchführung auf dem CASIA-Webface-Datensatz.}
	\label{fig:detektoren_stromverbrauch}
\end{figure}

Rechnet man den durchschnittlichen Stromverbrauch während des Detektionstests auf Wattsekunden pro Bild um, erhält man ein ähnliches Bild wie beim Benchmark der Kosinusdistanz: Auch hier arbeitet das mobile System (Laptop S1) zwar langsamer, dafür aber energieeffizienter als PC S2 (vgl. Abbildung \ref{fig:detektoren_stromverbrauch}).

\subsection{Gesichtserkennung}

\begin{figure}[!h]
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/reco/loss}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=1.0\linewidth]{daten/reco/accuracy}
	\end{subfigure}
	\caption{Entwicklung der Kostenfunktion (Loss) und der Wiedererkennungsrate (Accuracy) beim Training des DeepID-Netzes.}
	\label{fig:deepid_loss}
\end{figure}

Abbildung \ref{fig:deepid_loss} zeigt den Lernfortschritt während des Trainingsprozesses vom DeepID-Netz im Caffe-Framework auf den CASIA-Webface-Datensatz. Ab etwa 80.000 Iterationen wird die $90\%$-Genauigkeit-Marke überschritten und der Wert nähert sich dem für den \ac{LFW}-Datensatz in der Erstveröffentlichung des Netzes \cite{stollhoff:deepid} angegebenen Werts von $97,45\%$ an und betrug höchstens $96,71\%$ bei 250.000 Iterationen. Der Datensatz war dabei aufgeteilt in $90\%$ Trainingsdaten und $10\%$ Testdaten mittels Modulo 10, d.h. jede 10. Bilddatei wurde den Testdaten zugeordnet.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.75\linewidth]{grafiken/ma_vergleich_merkmale}
    \caption{Beispielbilder aus dem CASIA-Webface-Datensatz mit den dazugehörigen (graphisch veranschaulichten) Merkmalvektoren.}
    \label{fig:features_vergleich}
\end{figure}

Abbildung \ref{fig:features_vergleich} zeigt drei aus beispielhaft ausgewählten Bildern des CASIA-Webface-Datensatzes extrahierte Gesichter. Zwei der drei Gesichter (Label A) gehören derselben Person an. Daneben befinden sich die 160-dimensionalen Merkmalvektoren, erzeugt vom trainierten DeepID-Netz unter Zuhilfenahme folgenden Python-Skripts:\footnote{Teil des öffentlich gemachten Quelltextes; gezeigte Funktion ist Teil der Datei \url{https://gitlab.com/chris_nada/heliot_wildau/blob/master/server/build/trainer.py}}

{
    \scriptsize\linespread{1.0}\selectfont
    \lstinputlisting[frame=L,firstline=175,lastline=196]{../../server/build/trainer.py}
}

Trainiert wurde das Netz mit denselben Hyperparametern wie in der Quelltextveröffentlichung:\footnote{Quelltext (Dokumentation auf Chinesisch) öffentlich zugänglich: \url{https://github.com/hqli/face_recognition}}

\begin{quote}
	\small
\begin{verbatim}
	Learning Rate 	= 0.001
	Solver Momentum = 0.9
	Weight Decay	= 0.0005
	Solver Type		= SGD
\end{verbatim}
\end{quote}

S3 benötigte für das Training von 250.000 Iterationen ohne \ac{GPU}-Unterstützung und ohne Nutzung von \ac{BLAS} (d.h. das Training lief auf einem einzelnen Kern) 2 Tage 22 Stunden 15 Minuten und 22 Sekunden bei durchschnittlich fast 1 Iteration pro Sekunde. Auf S2 lief das Training trotz \ac{BLAS}-Unterstützung (und damit Parallelisierung auf 6 Kernen) sogar $32\%$ langsamer mit einer Geschwindigkeit von 0,68 Iterationen pro Sekunde. Bottleneck könnten hier auch die 8 GB Arbeitsspeicher gewesen sein.

Das komplexere \ac{VGG}-Face-Descriptor-Netz konnte nicht angelernt werden, da es nach Ablauf von 24 Stunden weniger als 1.000 Iterationen durchlief\footnote{Die genaue Zahl ist nicht bekannt, da alle 1.000 Iterationen ein Zwischenstand (Snapshot) geschrieben wird.} und daher als zu langsam (unter den gegebenen technischen Bedingungen) verworfen wurde. Die Größe eines Snapshots beim \ac{VGG}-Face-Descriptor beträgt zirka 0,5 GB; bei DeepID sind es etwa 9 MB. Ein einzelner Snapshot reicht Caffe aus, um das Modell daraus zu rekonstruieren (und ggf. anzuwenden).

\subsection{Raspberry Pi}

In seiner vorletzten Version\footnote{Im Juni 2019 erschien der Raspberry Pi 4.}, Raspberry Pi 3+, zeigt sich der Einplatinenrechner nicht wesentlich schlechtere Rechenleistung als ein gewöhnlicher Laptop (S1). Die derzeit neueste Version (Raspberry Pi 4) mit dem ARM Cortex-A72 als \ac{CPU} verspricht sogar noch bessere Performanz im Vergleich zum Modell 3+ mit ARM Cortex-A53 bei geringerer Leistungsaufnahme \cite{man1:arm_cortex_benchmarks}. Hier ist folglich Potenzial für Fog- oder Edge-Computing vorhanden, wie es in Kapitel \ref{kap:kommunikation} erörtert wurde.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.75\linewidth]{grafiken/raspi_stressberry}
	\caption{Raspberry Pi 3+ Hitzeentwicklung unter Last (Stressberry-Benchmark; Beginn 150s, Ende 450s).}
	\label{fig:raspi_stress}
\end{figure}

Nicht außer Acht lassen darf man, dass der Raspberry Pi im Auslieferungszustand nicht auf rechenintensive Aufgaben ausgelegt ist. So fehlt beispielsweise ein Kühlkörper, aktiv oder passiv, gänzlich. Verheerend zeigt sich das bei der Ausführung von Benchmarks (vgl. Abbildung \ref{fig:raspi_stress}), denn für den Raspberry Pi wird eine maximale Betriebstemperatur von 60°C angegeben \cite{man1:raspi2_max_temp}. 
\\\\
\paragraph{Leistungsaufnahme}\mbox{}

Leider hat der Raspberry Pi 3+ selbst im ausgeschalteten Zustand eine Leistungsaufnahme von 1 W. Wird der Einplatinenrechner über einen Akku betrieben, sollte deshalb durch eine weitere elektronische Maßnahme die Stromzufuhr vollständig überbrückt werden. Im Leerlauf, ohne jegliche Peripherie wurden 3,4 W bis 3,8 W gemessen. Mit einem 7,2 Zoll großem, angeschlossenem und eingeschaltetem Touchscreen\footnote{Modell Joy-it RB-LCD7.2} steigt die Leistung auf 7,1 W im Ruhezustand und auf etwa 8,4 W bei normaler Nutzung mit eingeschaltetem \ac{WLAN} (Browser, Texteditor usw.). Beim Ausführen des in Abbildung \ref{fig:raspi_stress} dargestellten Stresstests lag die  Leistung bei 10,1 W. Ein anderer, in C geschriebener Stresstest, \textit{stress-ng}, brachte die Leistungsaufnahme auf 11,4 W, was bei 5 V Spannung einen Strom von 2,28 A bedeutet. Witzig ist, dass der \ac{USB}-Standard selbst im elektrisch großzügigem Battery-Charging-Modus eigentlich maximal nur 1,5 A (bzw. 7,5 W) erlaubt \cite{man1:usb_standard}. Das auf den Raspberry Pi angeblich abgestimmte Netzteil\footnote{Modell Goobay 71889, Händlerangabe} liefert nach Etikettierung sogar bis zu 2,5 A, das hieße 12,5 W. Streng genommen, dürften sich solche Anschlüsse nicht mehr \ac{USB} nennen. Vermutlich ist aber die nicht standardkonforme Leistungsaufnahme des Raspberry Pis bekannt, weshalb Netzteilhersteller mit großzügiger Toleranz bei der Stromstärke produzieren. Dass der dauerhafte Betrieb mit den angegebenen Leistungen dem Gerät schadet, konnte nicht nachgewiesen werden. Auch im einfachen Kosinusdistanz-Benchmark aus dem vorangegangenen Kapitel steigt bei Ausnutzung von nur einem der vier Rechnerkerne die Leistungsaufnahme schon auf 9,7 W.

Bei regulärer Nutzung mit 8,4 W Leistungsaufnahme würde die Ladung im Akkubetrieb für etwa 36 Minuten je 1000 mAh Kapazität reichen. Beim Akkukauf für den Raspberry Pi könnte man die Rechnung mit 2000 mAh pro 1 h Betrieb überschlagen.
