\newpage
\section{Methoden}

Die Implementierung der Klient-Server-Architektur erfolgte quelloffen und nachverfolgbar.\footnote{GitLab Repository des Projekts: \url{https://gitlab.com/chris_nada/heliot_wildau}.}

Als Programmiersprachen wurden C++ sowie Python gewählt, da diese wie sich zeigte (vgl. Kapitel \ref{kap:programmiersprachen}) im Maschinenlernbereich am stärksten vertreten sind.

Bei der Auswahl von Programmbibliotheken, die nicht für den \ac{KI}-Teil der Arbeit relevant sind, wurde auf Minimalität geachtet. Es sollten keine Bibliotheken verwendet werden, die viel ungebrauchte Funktionalität beinhaltet und möglicherweise noch Abhängigkeiten zu weiteren Bibliotheken mit sich ziehen.

Für \textbf{Nutzeroberflächen} stehen in C++ unter anderem Qt, GTK+, wxWidgets und FLTK zur Verfügung. Die Nutzeroberfläche des Raspberry Pis als Klienten wurde daher in FLTK (Fast Light Toolkit) als kleinste, plattformunabhängige Bibliothek geschrieben \cite{crossplatform_development}.

Bei der \textbf{Sicherheit} zum Schutz von Passwörtern wird auf die in C verfasste Originalimplementierung der Hashfunktion Argon2 \cite{man1:argon2} zurückgegriffen, da dieser relativ neue Algorithmus noch in keiner allgemeinen Sicherheitsbibliothek zu finden war. Für geläufige sicherheitsrelevante Algorithmen wie z.B. \ac{AES} gibt es die sehr performanten Bibliotheken Botan oder Crypto++ \cite{man1:cpp_crypto_libs}. Willkürlich wurde hier Crypto++ gewählt, da sich die beiden Kandidaten stark ähneln. Crypto++ wurde in der Vergangenheit vom US-amerikanischen \ac{FIPS} zertifiziert \cite{man1:cryptopp_cmvp}, während Botan parallel vom deutschen \ac{BSI} eingesetzt wurde \cite{man1:botan_bsi}. Welchem man höhere Anerkennung zurechnet ist wohl Geschmackssache.

\subsection{Implementierung Server}

Der Quelltext wurde im C++17-Standard verfasst und ist plattformunabhängig, soweit wie es die verwendeten Bibliotheken erlauben.

Die Ausführung des Servers aus der Konsole ist durch ein Master-Passwort geschützt, welches mit einem gespeicherten Hash aus der Argon2-Hashfunktion aus einem lokalen Cache abgeglichen wird. Für weniger sensible Hashes wird \ac{SHA}-3 mit 512 Bit Digest-Länge verwendet.

Um kein komplettes Server-Framework vom Grundstein an neu zu implementieren, wurde ein einfaches, bestehendes Framework genutzt und darauf aufgebaut: \textit{cpp-httplib} von Yuji Hirose,\footnote{Quelltext online verfügbar unter \url{https://github.com/yhirose/cpp-httplib}.} welches aus einem einzigen C++-Header (httplib.h) besteht und damit sehr minimalistisch ist. Trotzdem wird \ac{HTTP} 1.1 standardkonform implementiert sowie \ac{HTTPS} via \ac{SSL} unterstützt.

Der Server bietet folgende \ac{REST}-Abfragen an:

\begin{itemize}
	\item \texttt{GET /info} gibt (auch im Browser) Informationen als Text darüber aus, wie die \ac{REST}-Schnittstelle des Servers abzurufen ist.
	\item \texttt{GET /existiert} gibt 200 als Antwort, wenn der als Parameter \texttt{login} überreichte Nutzername in den Lerndaten bereits existiert, ansonsten wird 404 zurückgemeldet.
	\item Mit \texttt{PUT /neues\_bild} kann eine einzelne neue Lerndatei an den Server übermittelt werden. Der Body muss dabei durch Semikolon abgetrennt den Pseudonym und danach die Bilddaten (als Array von \texttt{unsigned char} aus OpenCV) enthalten. Als Antwort bekommt man vom Server die Koordinaten (x, y, Breite, Höhe) des Gesichts, falls dieses erkannt wurde.
	\item \texttt{PUT /registrieren} legt einen neuen Nutzer an. Folgende Daten müssen durch Semikolon abgetrennt im Body der Anfrage enthalten sein: Nutzername, Pseudonym, Bilddaten.
	\item Über die Anfrage \texttt{PUT /recognize} lässt sich ein im Body enthaltenes Bild klassifizieren. Als Antwort bekommt man vom Server die Koordinaten (x, y, Breite, Höhe) des Gesichts, falls dieses erkannt wurde sowie den Nutzernamen.
\end{itemize}

Alle Nutzerdaten werden mittels Blockverschlüsselung \ac{AES} in 256 Bit im \ac{CFB}-Modus, welcher für diesen Anwendungsfall empfohlen wird \cite{man1:cipher_recommandations}, dateiweise chiffriert.

Der Server nutzt für die Gesichtsdetektion Quelltext in C++, für die Gesichtserkennung wird ein externes Python-Skript (\texttt{trainer.py}) genutzt,\footnote{Aufruf: \texttt{python3 trainer.py --predict}} welches das Klassifikationsergebnis in einer temporären Datei zwischenspeichert, die von C++ zurück ausgelesen wird. Welcher Algorithmus zur Gesichtsdetektion verwendet werden soll, lässt sich in einer Konfigurationsdatei (\texttt{config.ini}) festlegen. Neben dem Algorithmus lassen sich hier noch weitere Dinge konfigurieren, wie beispielsweise die zu nutzende Port-Nummer.

Sollte der bestehende Server mit einer anderen Gesichtserkennung genutzt werden, kann das Python-Skript angepasst oder dieses durch ein anderes Skript ersetzt werden, das dasselbe Input und Output (temporäre Dateien) besitzt.

\subsection{Implementierung Klient}

Der Klient wurde vollständig in C++ implementiert. Das Raspberry-Pi-Standardbetriebssystem Raspbian stellt sowohl Kompiler (\ac{GCC}) als auch Build-Tools wie \textit{Make} oder \textit{CMake} via \textit{apt}-Paketverwaltung zur Verfügung.

Folgende Funktionen galt es als Nutzeroberfläche zu realisieren:

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\linewidth]{grafiken/klient_registrierung}
	\caption{Klient: Fenster zur Registrierung neuer Teilnehmer.}
	\label{fig:klient_registrierung}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\linewidth]{grafiken/klient_erkennung}
	\caption{Klient: Fenster zur Visualisierung der Erkennung.}
	\label{fig:klient_detektion}
\end{figure}

\begin{itemize}
	\item Ein Fenster, in dem sich ein neuer Teilnehmer eines Probelaufs (für die Speicherung seiner Lerndaten, d.h. Bildaufnahmen) unter einem Pseudonym registrieren kann. Dazu gehört die Eingabe seiner Unterschrift zur Datennutzungseinwilligung (Abbildung \ref{fig:klient_registrierung}).
	\item Ein Fenster, in dem der laufende Kamera-Stream angezeigt wird und Nutzerfotos aufgezeichnet (und an den Server gesendet) werden (Abbildung \ref{fig:klient_detektion}).
	\item Ein Fenster, in dem der laufende Kamera-Stream angezeigt wird und dieser zur Klassifikation (im Hintergrund) an den Server gesendet wird, wonach das Ergebnis der Klassifikation dem Nutzer visualisiert wird.
\end{itemize}

Wie sich schnell herausstellte, stand in dem genutzten Nutzeroberflächen-Framework FLTK weder ein Widget\footnote{Widget = Kleinstes funktionales Element einer Nutzeroberfläche.} für den Kamera-Live-Stream noch ein Canvas-Widget (wie in einer Zeichen-Software) zur Eingabe einer Unterschrift zur Verfügung. Diese mussten (um einem Wechsel auf ein anderes Framework vorzubeugen) zusätzlich programmiert werden.

Im Backend war die komplexeste Aufgabe neben dem Konvertieren der Rohbilddaten, die von der Kamera stammen, die Implementierung der \ac{REST}-Schnittstelle zum Server. Im Zusammenhang musste hier darauf geachtet werden, dass die Bilddaten des Klienten auf dieselbe Art in Bytes (zur Übertragung) kodiert werden, wie sie der Server dekodiert. Es wurde dabei sowohl mit \ac{PNG}- als  auch \ac{JPEG}-Kompression experimentiert. Die zu übertragenden Daten müssen bei der Kommunikation mit dem Server selbst nicht zusätzlich verschlüsselt werden, da diese bei der Verwendung von \ac{HTTPS} bereits \ac{SSL}-verschlüsselt werden, solange sie sich im Body und nicht in der Adresszeile der Anfrage befinden.

\subsection{Rechnersysteme}

\begin{sidewaystable}[]
	\centering
	\footnotesize
	\begin{tabular}{|l|l|l|l|l|}
		\hline
		\textbf{Name} & System 1 (S1) & System 2 (S2) & System 3 (S3) & System 4 (S4) \\ \hline
		\textbf{Typ} & Laptop & PC & PC (Server) & Raspberry Pi 3 B+ \\ \hline
		\textbf{CPU - Name} & AMD A8-6410 & AMD FX-6300 & AMD Ryzen 2990WX & ARM Cortex-A53\footnote{Implementiert als Broadcom BCM2837B0.} \\ \hline
		\textbf{Architektur} & x86\_64 & x86\_64 & x86\_64 & ARMv8-A \\ \hline
		\textbf{CPU - Datengröße in Bit} & 64 & 64 & 64 & 64 \\ \hline
		\textbf{CPU - Jahr} & 2014 & 2012 & 2018 & 2012 \\ \hline
		\textbf{CPU - Kerne} & 4 & 6 & 32 & 4 \\ \hline
		\textbf{CPU - Threads} & 4 & 6 & 64 & 4 \\ \hline
		\textbf{CPU - Cache  L1 in KB} & 256 & 288 & 3000 & 32 \\ \hline
		\textbf{CPU - Cache  L2 in KB} & 2000 & 6000 & 16000 & 512 \\ \hline
		\textbf{CPU - Cache  L3 in KB} & 0 & 8000 & 64000 & 0 \\ \hline
		\textbf{CPU - Taktung in MHz} & 2000 & 3500 & 3000 & 1400 \\ \hline
		\textbf{CPU - Taktung (Turbo) in MHz} & 2400 & 4100 & 4200 & 1400 \\ \hline
		\textbf{CPU - TDP in W} & 15 & 95 & 250 & unbekannt \\ \hline
		\textbf{RAM - Speicher in MiB} & 6935 & 7926 & 63488 & 875 \\ \hline
		\textbf{RAM - Taktung in MHz} & 1333 & 1600 & 2933 & 900 \\ \hline
		\textbf{GPU - Name} & (Integriert) & NVidia GeForce GT 730 & 2 * Nvidia GeForce RTX 2080 & (Integriert) \\ \hline
	\end{tabular}
	\caption{Leistungsübersicht verwendeter Systeme.}
	\label{tab:systeme}
\end{sidewaystable}

Zur Arbeit sowie für Benchmarks und Tests standen die in Tabelle \ref{tab:systeme} aufgelisteten Systeme zur Verfügung. Im weiteren Verlauft werden sie mit ihren Alias (S1, S2, S3, S4) referenziert.

\subsection{Gesichtsdetektion}

Um die in der Literatur zur Verfügung stehenden Ergebnisse insbesondere auch auf das Merkmal Performanz hin zu überprüfen, wurden drei Algorithmen implementiert:

\begin{itemize}
	\item eine vortrainierte Haar-Kaskade aus der Implementierung von OpenCV \cite{man1:viola2001rapid},
	\item ein vortrainiertes \ac{HOG} aus der Implementierung von DLib \cite{sonstiges:2015_dlib_king_mmobject_detection} sowie
	\item ein zur Gesichtsdetektion vortrainiertes \ac{CNN} aus der Implementierung von DLib \cite{face_reco:dlib}.
\end{itemize}

Zur Messung der Energieeffizienz diente das zwischen Netzteil des jeweiligen Systems und Steckdose geschaltete Stromverbrauchsmessgerät Arendo Model 300792.

\subsection{Gesichtserkennung}
\label{kap:methodik_gesichtserkennung_kosinus}

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.0\linewidth]{grafiken/vgg1024_edit}
	\caption[Schematischer Aufbau des VGG Face Descriptors.]{Schematischer Aufbau des VGG Face Descriptors. Rot: Convolutional Layer; Orange: Max-Pooling Layer; Lila: Fully Connected Layer; Grün: Aktivierungsfunktionen; Blau und Weiß: Input/Output.}
	\label{fig:vgg_schema}
\end{figure}

Als besonders interessante Kandidaten stachen bei der Literaturarbeit die neuronalen Netze mit Ausgabe von Merkmalvektoren hervor mit deren Referenzimplementierung, dem \ac{VGG} Face Descriptor von der University of Oxford \cite{man1:vgg_face}. Interessant sind diese deswegen, da sie eine weitere Ebene der Aufteilung von Algorithmus auf Klient und Server ermöglichen (vgl. Kapitel \ref{kap:programmiersprachen}). Da solche Algorithmen die Klassifikation selbst nicht integriert haben, wird diese über einen weiteren Schritt mittels der Kosinusdistanz mit allen Merkmalvektoren der Lerndaten berechnet (siehe Formel \ref{eq:dist_kosinus} sowie Kapitel \ref{kap:gesichtserkennung}). Ein zu klassifizierender Merkmalvektor erhält dann das Label, zu dessen Gegenüber die geringste Distanz aus den Lerndaten besteht. Enthalten die Lerndaten n Bilder, muss n-mal die Kosinusdistanz von jeweils zwei 1024-dimensionalen (so die Größe des Merkmalvektors vom \ac{VGG} Face Desciptors) Vektoren maschinell berechnet werden. Um über diese Problematik Aussagen treffen zu können, wurde ein Benchmark sowohl in C++ als auch in Python (NumPy), den beiden relevantesten Programmiersprachen im Maschinenlernbereich wie Kapitel \ref{kap:frameworks} zeigt, verfasst:

\paragraph{Kosinusdistanz in C++}\mbox{}\\

{
    \textbf{cs.hpp}
	\scriptsize\linespread{1.0}\selectfont
	\lstinputlisting[language=C++,frame=L]{daten/cs_benchmark/cs.hpp}
}

{
    \textbf{main.cpp}
	\scriptsize\linespread{1.0}\selectfont
	\lstinputlisting[language=C++,frame=L]{daten/cs_benchmark/main.cpp}
}

\paragraph{Kosinusdistanz in Python}\mbox{}\\

{
    \textbf{cs.py}
	\scriptsize\linespread{1.0}\selectfont
	\lstinputlisting[language=Python,frame=L,lastline=36]{daten/cs_benchmark/cs.py}
}

Die Implementierung des \ac{VGG} Face Descriptors entstammt der Originalimplementierung, verfasst als Modell im Framework Caffe (vgl. Kapitel \ref{kap:frameworks}), welches vom Berkeley Artificial Intelligence Research (BAIR) der University of California, Berkeley, erstellt wurde.\footnote{Zugänglich ist die Originalimplementierung online: \url{http://www.robots.ox.ac.uk/~vgg/software/vgg_face}.}

Der \ac{VGG} Face Descriptor (Abbildung \ref{fig:vgg_schema}) besteht aus insgesamt 13 Convolutional Layern, die in zwei Zweiergruppen und danach drei Dreiergruppen mit jeweils abschließendem Max-Pooling-Layern arrangiert sind. Als deren Aktivierungsfunktionen werden die im Deep-Learning-Bereich sehr geläufigen \acp{ReLU} ($f(x) = \text{max}(0,x)$) verwendet. Hinter den tiefen Convolutional Schichten befinden sich drei Fully Connected Layer mit sowohl \acp{ReLU} als auch Dropout. Abgeschlossen wird das Netz dahinter von einer Softmax\footnote{Normalisierte Exponentialfunktion}-Schicht \autocite{man1:vgg_face,man1:bishop_christopher2006}.

\paragraph{Absicherung}\mbox{}

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.0\linewidth]{grafiken/deepid}
	\caption{Schematischer Aufbau des DeepID-Netzes.}
	\label{fig:deepid_schema}
\end{figure}

Die Komplexität und der Rechenaufwand der Gesichtserkennung erlaubt es leider - im Gegensatz zur Gesichtsdetektion - nur die Untersuchung eines Algorithmus auf einem System. Insbesondere der \ac{VGG} Face Descriptor gilt als verhältnismäßig tiefes Netz, das daher bei der Ausführung schwerfällig ist, was vermutlich nur System 3 unter Zuhilfenahme von \ac{GPU}-Parallelisierung (mittels \ac{CUDA}) zumutbar ist.

Sollte selbst System 3 Schwierigkeiten haben \ac{VGG} auszuführen, aus welchen Gründen auch immer, ist ein leichtgewichtiges Ersatznetz mit vergleichbarer Ergebnisqualität ausgewählt worden: DeepID (vgl. Abbildung \ref{fig:lfw_algo_scores}). Dieses Netz wurde von seinen Autoren seit seiner Erstveröffentlichung 2014 \cite{stollhoff:deepid} über mehrere Jahre hinweg weiterentwickelt. So entstanden DeepID2 \cite{man1:deepid2}, DeepID2+ \cite{man1:deepid2p} und schließlich DeepID3 \cite{man1:deepid3}. Die Netze unterscheiden sich in ihrer Ergebnisqualität untereinander nur leicht (weniger als 1 Prozentpunkt), jedoch nimmt die Tiefe der Schichten von Version zu Version (besonders stark im Fall von DeepID3) zu. Die Originalversion ist dargestellt in Abbildung \ref{fig:deepid_schema}. Vergleicht man das Netz mit dem \ac{VGG} Face Descriptor (Abb. \ref{fig:vgg_schema}), wird die geringere Anzahl Schichten auf dem ersten Blick deutlich (13 Convolutional Layer versus vier).

\subsection{Datensatz}

Sowohl für Gesichtsdetektion als auch zur Gesichtserkennung wurde CASIA-Webface als Datensatz genutzt. Dieser ist mit über 4 GB Größe umfangreich, aber nicht zu umfangreich um ihn auch auf schwächeren Systemen (Laptop S1) zu bewältigen. Er enthält 10.575 Individuen bei 494.414 Bildern. Mit durchschnittlich fast 47 Bildern pro Person spielt er vermutlich in derselben Liga wie ein Bibliotheksroboter, der von seinen Nutzern einige Bilder zur Erkennung aufnimmt. Jedoch strukturell ähneln sich die Fotos des CASIA-Webface-Datensatzes wohl untereinander viel weniger als solche, die immer in derselben Umgebung (womöglich noch am selben Tag) aufgenommen würden.

%% Implementierung der Gesichtserkennung 
%%% Merkmalsextraktion 
%%% Netzaufbau 
%%% Netzstruktur und Optimierung 
%%% Modifikationen State-of-the-Art Algorithmen 
%% IT-Architektur der Implementierung 
%%% Entwurf und Aufbau einer zentralisierten Architektur 
%%% Zentralstelle (Server): Framework, Programmiersprache, technische Spezifikation 
%%% Pepper
%%% Webcam 
%% Datensätze 
%%% Datenquellen 
%%% Datenschutz
