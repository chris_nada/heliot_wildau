\section{Theorie}

\subsection{Detektion des Gesichts}
\label{kap:gesichtdetektion}

Zu diesem Kapitel, das die Thematik der Detektion von Gesichtern behandelt, muss angemerkt werden, dass auch andere Ansätze existieren, den Gesichtsbereich zu identifizieren, wie beispielsweise die Detektion eines oder beider Augen \cite{face_reco:faik_phd}. Da der Fokus dieser Arbeit auf der Implementierung gestandener Systeme liegt, werden alternative Ansätze nicht behandelt, stattdessen traditionelle Systeme betrachtet.

\subsubsection{Prozessablauf}

Bevor auf ein Bild, das ein Gesicht enthält, ein Identifikationsalgorithmus angewandt werden kann, muss das Gesicht vom Hintergrund getrennt sein. Für diesen Arbeitsschritt wird eine Gesichtsdetektion angewandt, die auf ganz verschiedene Art und Weise den Bildbereich definieren kann, in dem sich ein Gesicht befindet. Schematisch dargestellt ist dieser Ablauf in Abbildung \ref{fig:maprozessablauf} \autocite{face_reco:dlib}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\linewidth]{grafiken/ma_prozessablauf}
	\caption{Stark abstrahierte Sichtweise auf den Prozessablauf der Gesichtsdetektion.}
	\label{fig:maprozessablauf}
\end{figure}

Diese Gesichtdetektionsalgorithmen stimmen von der Arbeitsweise häufig mit Objektdetektoren, die beispielsweise Autos, Straßenschilder oder Ähnliches erkennen können, überein. Das heißt, derselbe Algorithmus lässt sich sowohl für Gesichtsdetektion als auch Detektion von Straßenschildern nutzen, vorausgesetzt er wurde dafür angelernt. Beispiele für solch 'flexible' Algorithmen sind z.B. \ac{HOG} oder die nach dem ungarischen Mathematiker Alfréd Haar\footnote{Alfréd Haar (1885 - 1933)} benannte Haar-Kaskade (auch: Haar-Wavelet), es existieren noch viele andere. Beide genannten und viele weitere Algorithmen basieren auf der Extraktion und Klassifikation von Bildmerkmalen (engl. Features), nicht auf \ac{KNN}. Prinzipiell arbeiten derartige Objektdetektoren im Backend so, dass das Gesamtbild in Teile zerlegt wird, wobei entweder direkt für jedes einzelne Teil eine Wahrscheinlichkeit errechnet wird (so meist bei Anwendung neuronaler Netze), mit der es sich um das zu erkennende Objekt handelt, oder es werden Merkmale (bspw. in Form einer Matrix) extrahiert, die über einen weiteren Prozessschritt und einer von der Merkmalextraktion losgelösten Klassifikation (z.B. eine \ac{SVM}) einem Label zugeordnet werden (Straßenschild, Gesicht, Auto, ...). Abbildung \ref{fig:mamerkmalextraktion} soll diese beiden Arbeitsweisen veranschaulichen \autocite{unsortiert:handbook_of_face_recogn,face_reco:2018_master_thesis_rokkones}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{grafiken/ma_merkmalextraktion}
	\caption[Verschiedene Prozessabläufe bei der Gesichtsdetektion]{Arbeitsablauf für die Detektion (von Gesichtern) mit Merkmalextraktion vs. ohne. Algorithmus 'A', 'B' und 'C' können von völlig unterschiedlichem Typ sein.}
	\label{fig:mamerkmalextraktion}
\end{figure}



\subsubsection{Algorithmen}\label{sec:t0_algorithmen}

Wie schon angedeutet, gibt es sowohl für die Erkennung als auch die Identifikation von Gesichtern nicht nur eine vorgeschriebene Methode. Vielmehr ist es nicht leicht, sich über die Vielzahl von Algorithmen einen Überblick zu verschaffen, noch schwieriger ist die redaktionelle Aufgabe, Lesern objektiv die maßgebendsten Algorithmen vorzustellen.

Sowohl für die Erkennung als auch Wiedererkennung von Gesichtern lassen sich die Algorithmen grob in zwei Oberkategorien einteilen, unter der Bedingung, dass ein paar exotische Ausreißer, die in keine der beiden Schubladen passen, eine gesonderte Kategorie, \textit{Sonstige}, für sich erhalten. Eine Klasse bilden die Algorithmen, die ohne \ac{KNN} im Hintergrund laufen und Bildmuster auf völlig unterschiedliche Art und Weise mathematisch modellieren: Als Beispiel ist \ac{HOG} im Prinzip eine Kantenerkennung, die Ausrichtung und Intensität geometrischer Strukturen im Bild auf ein grobes Raster projiziert. Die Anwendung von \ac{HOG} zur Objekterkennung ist mit der Absicht verbunden, Haar-Wavelets zu ersetzen, die bis dato häufig diese Aufgabe übernahmen und bis heute oft noch dazu verwendet werden \autocite{man1:hog_dalal}. Merkmalextraktion ohne \ac{KNN} leidet immer an der Schwäche, dass die Detektion nur ohne Neigung des Objekts im Bild gut funktioniert. Für die Detektion von Gesichtern in zweidimensionalen Bildern wird eine Neigungstoleranz von $20\degree$ solcher und ähnlicher Algorithmen genannt; Haar-Wavelets sollen noch empfindlicher sein, weshalb man aktuell auch Ansätzen nachgeht, aus zweidimensionalen Bildern das dreidimensionale Gesicht zur Anwendung neuer 3D-Erkennungsalgorithmen zu rekonstruieren, was jedoch eine ganz eigene Problematik für sich ist und hier nur nebenbei genannt sein soll \autocite{face_reco:identifikazia}.

Wie zu Beginn des Kapitels erwähnt, gibt es nicht einen \textit{ultimativen} Algorithmus für die Detektion von Gesichtern, der auf jedem Anwendungsfall perfekt zugeschnitten ist. Algorithmen versuchen nur eine Tatsache mathematisch so zu modellieren, dass sich dieses Modell verwenden lässt. Dass \textit{,,alle Modelle falsch sind, manche jedoch nützlich''},\footnote{Englisches Original: ,,All models are wrong, but some are useful.'' - George Box. Britischer Statistiker (1919 – 2013).} stellte bereits George Box fest \cite{unsortiert:2019_100p_machine_learning}. Daraus lässt sich eine wichtige Folgerung für den Umgang mit Modellen ziehen: Nicht das Modell ist \textit{gut}, das möglichst perfekt die Realität widerspiegelt, sondern solches, welches maximalen praktischen Nutzen hat.

\begin{table}[h]
	\centering
\begin{tabular}{|r|c|c|}
	\hline 
	Eigenschaft & \ac{KNN} & Merkmalextraktion, \\
	            &          & kein \ac{KNN} \\ 
	\hline 
	Resistenz gegen & + & - \\ 
	Störfaktoren   &   &   \\
	\hline 
	Verfügbarkeit & - & + \\ 
	\hline 
	Rechenzeit & - & + \\ 
	\hline 
	Erkennungsrate & + & - \\ 
	\hline
\end{tabular}
	\caption[Vergleich KNN - Merkmalextraktion.]{Vergleich KNN - Merkmalextraktion; $+$ zeigen Stärken, $-$ zeigen Schwächen an.}
    \label{tab:vergleich_knn_merkmalextraktion}
\end{table}

Vergleicht man die Modelle, welche auf Merkmalextraktionsalgorithmen basieren mit anschließender Klassifikation und solche, die ein \ac{KNN} anwenden, lassen sich trotzdem einige allgemeine Beobachtungen machen (Tabelle \ref{tab:vergleich_knn_merkmalextraktion}, Kriterien wurden nach Vorhandensein von Information in der Literatur ausgewählt).

Wenn bei der praktischen Anwendung also Treffsicherheit (Erkennungsrate) und hohe Resistenz gegen Störfaktoren (Neigungswinkel des Gesichts, Varianz in der Beleuchtung oder Belichtung) priorisiert werden, empfiehlt sich die Anwendung der Gesichtsdetektion durch ein künstliches neuronales Netz; liegt der Fokus eher auf einer schnellen Ausführung (Rechenzeit, bspw. auf mobilen Endgeräten wichtig wegen knapper Ressourcen) und hoher Verfügbarkeit, so eignen sich die auf Merkmalextraktion basierenden Klassifikationen besser. Verfügbarkeit bedeutet hier, dass der Algorithmus auf möglichst vielen verschiedenen Geräten lauffähig ist, so kann z.B. ein fertig angelerntes neuronales Netz zur Gesichtsdetektion die Speicherkapazität solcher Geräte überschreiten, \autocite{man1:dlib09,face_reco:identifikazia,face_detect:2018_rus_closed_eyes_weg} während beispielsweise Haar-Wavelets bereits auf kleinen \ac{SOC}-Systemen lauffähig sind \autocite{face_detect:2017_facedetect_fpga,face_reco:conferenceceit13}.

\newpage

Zu den in der Literatur häufig eingesetzten Vertretern der Gesichtsdetektion gehören:

\begin{itemize}
	\item Haar-Wavelets (manchmal Haar-Cascade oder nach ihren Autoren auch als \textit{Viola-Jones-Methode} bekannt) \cite{man1:viola_jones_2001},
	\item \ac{HOG} \cite{man1:hog_dalal} (mit \ac{SVM}) \cite{face_reco:2018_master_thesis_casimiro_real_time_face_reco},
	\item \ac{LBP} \cite{face_reco:local_binary},
	\item \ac{PCA}, ,,Eigenface'' \cite{man1:eigenface_turk_pentland} und
	\item verschiedene Variationen von \acp{KNN}, unter anderem \acp{CNN} \cite{face_reco:dlib}.
\end{itemize}

Bei der Hauptkomponentenanalyse (\ac{PCA}), die auch beim Eigenface-Verfahren, dessen Name sich wohl an \textit{Eigenvektor bzw. Eigenwert} anlehnt, angewandt wird, werden, vereinfacht gesagt, die relevantesten Pixeldaten erfasst, durch die sich Objekte (z.B. Gesichter) unterscheiden. Aus diesem Schritt entsteht eine Kovarianzmatrix, über die anschließend via Linearkombination derer Elemente ein zu analysierendes Objekt so annähernd rekonstruiert werden kann. Bei der Gesichtsdetektion wird dabei das 'Durchschnittsgesicht' errechnet und die euklidische Distanz eines zu bestimmenden Objekts zu diesem bestimmt. Unterschreitet diese (von den Autoren als \textit{faceness} bezeichnete) Distanz einen durch empirische Versuche festgelegten Grenzwert, wird das Objekt als Gesicht eingestuft  \cite{man1:eigenface_turk_pentland}.

Der bisher noch nicht genannte Vertreter \ac{LBP} versucht, ähnlich wie \ac{HOG}, das in den Algorithmus gegebene Gesamtbild zu rastern und in jedem Feld (in ursprünglicher Form 3x3 Pixel, später größer und variabel) nicht Kanten zu erkennen, sondern über die Nachbarschaftsverhältnisse (Grauabstufungen) der Pixel das Feld zu klassifizieren. Das Verfahren ist daher farbinvariant. Auch die Viola-Jones-Methode und \ac{HOG} verlieren bei der Merkmalextraktion die Farbinformationen des Bildes \cite{face_reco:local_binary}.

Um Verwirrung zu vermeiden, sei noch festzuhalten, dass sowohl \acp{HOG} als auch Eigenface sowie neuronale Netze nicht nur zur Gesichtsdetektion, sondern auch (in dafür angepasster Form) zur Wiedererkennung eingesetzt werden können.

\subsubsection{Performanz}
\label{kap:performanz_theorie}

In der Originalbeschreibung des Verfahrens wird \ac{LBP} als zwar rechenzeitaufwändig eingeschätzt, aber weniger als alle konkurrierenden Algorithmen und man bezeichnet ihn als einzigen praktikablen Kandidaten für mobile Anwendungen \cite{face_reco:local_binary}. Die Problematik der Themen Rechenzeit, Performanz und Genauigkeit der Algorithmen fällt hier zum ersten Mal auf, denn die Aussage (von 2014), dass \ac{LBP} anderen Verfahren in Sachen Rechenzeit überlegen sei, ist unhaltbar, da diverse andere Algorithmen auf mobilen Plattformen zur Anwendung gekommen sind, als Beispiele seien genannt:

\begin{itemize}
\item Haar-Cascade auf \ac{ARM} \cite{face_detect:2018_haar_features}, 
\item Haar-Cascade auf einem \ac{SOC} \cite{face_detect:2017_facedetect_fpga},
\item ein auf OpenCV basierendes System auf einem Zync 7000 \ac{FPGA} \cite{face_reco:hardware_software_embedded} und
\item ein auf dem Roboter \textit{,,NAO''} laufender \ac{PCA} Algorithmus  \cite{face_detect:nao_face}.
\end{itemize}

Es ist hier nicht die Absicht, \ac{LBP} oder andere Algorithmen zu diskreditieren, im Gegenteil, \ac{LBP} gilt als solide Lösung zur Gesichtsdetektion mit hoher Performanz \cite{robotik:person_recognition}.
Es soll lediglich darauf aufmerksam gemacht werden, bei Aussagen über die Qualität von Algorithmen in Veröffentlichungen genauer hinzuschauen, insbesondere wenn Autor des Algorithmus und Autor der Veröffentlichung eine Personalunion innehalten.

Die um die Jahrtausendwende verbreiteten Detektoren (z.B. Haar) hatten meist ihre Stärke in der Rechenzeit; um jedoch den Anforderungen genügende Detektionsraten zu erzielen, war zu dieser Zeit oft zusätzliche, manuelle Feinjustierung notwendig. Höhere Rechenkapazitäten der folgenden Jahre ermöglichten schließlich Optimierungsverfahren wie Boosting, insbesondere AdaBoost (Adaptive Boosting), welches in der Lage ist, aus vielen \textit{schwachen} Klassifikationskomponenten ein hochakkurates Modell zu konstruieren. So vereint die moderne Version des Viola-Jones-Detektors beides: hohe Performanz sowohl in Ausführung als auch Detektionsrate \cite{man1:neue_facedetectsurvey}. 

Trotzdem unterliegen diese modernen Detektoren immer noch alten Problemen: Sie sind nach wie vor besonders anfällig gegenüber Änderungen in der räumlichen Ausrichtung des Gesichts, auch wechselnde Beleuchtungsverhältnisse sind problematisch \cite{face_reco:identifikazia}. 

Die durch moderne Rechenkapazitäten ermöglichte Antwort auf die Schwächen 'herkömmlicher' Algorithmen lautet \ac{KNN}. Vorteile in der Anwendung von \acp{KNN} sind hier die gleichwertige bis höhere Detektionsrate bei weniger Fehleranfälligkeit durch unterschiedliche Gesichtsausrichtungen. Auch erfordern neuronale Netze geringeren manuellen Aufwand für bspw. Markierung von Orientierungspunkten in Gesichtern (facial landmarks). Der Anlernprozess des Netzes umfasst damit weniger Handarbeit, dafür mehr (automatisierte) Rechenarbeit, die je nach Hardware auch mehrere Tage dauern kann \autocite{man1:neue_face_detect_nn}. 

Für die auf \ac{KNN}-basierte Detektion von Gesichtern gibt es nicht nur einen Algorithmus, i.e. einen festgelegten Netzaufbau. Vielmehr existieren unzählige Netzstrukturen, die aus z.T. ganz unterschiedlichen Ansätzen resultierten. Ein recht früher (1996) Sonderfall von Kanade ist das Kombinieren gleich mehrerer \acp{KNN}, vor allem um Detektionsraten zu steigern \autocite{man1:neue_face_detect_nn, man1:1996_kombi_nn}.

\begin{figure}[h]
\centering
	\begin{tikzpicture}[scale=1.25]
	\begin{axis}[
 		every node near coord/.append style={
			/pgf/number format/fixed zerofill,
			/pgf/number format/precision=3
		},
		xbar,
		xmax=1.0,
		y=-0.5cm,
		bar width=0.3cm,
		enlarge y limits={abs=0.45cm},
		%enlarge x limits=auto, 
		xlabel={Average Precision (AP)},
		symbolic y coords={
			HeadHunter,
			DPM (HeadHunter),
			Structured Models,
			Olaworks,
			DDFD NMS-avg.,
			Face++,
			SURF Cas. multiview,
			PEP-Adapt,
			XZJY,
			Shen et al.,
			TSM,
			Segul et al.,
			Li et al.,
			Illuxtech,
			Jain et al.,
			Viola Jones
		},
		ytick=data,
		nodes near coords, nodes near coords 
		]
	\addplot[black,fill=black!10!white] table[col sep=comma,header=false] {
		0.871,HeadHunter
		0.864,DPM (HeadHunter)
		0.846,Structured Models
		0.843,Olaworks, 
		0.840,DDFD NMS-avg.
		0.839,Face++
		0.837,SURF Cas. multiview
		0.809,PEP-Adapt
		0.786,XZJY
		0.777,Shen et al.
		0.766,TSM
		0.761,Segul et al.
		0.760,Li et al.
		0.718,Illuxtech
		0.677,Jain et al.
		0.597,Viola Jones
	};
	\end{axis}
	\end{tikzpicture}
\caption{Vergleich verschiedener Gesichtsdetektoren am FDDB-Datensatz nach \cite{man1:neue_face_detect_nn}.}
\label{fig:detektorenvergleich}
\end{figure}

Eine Übersicht über die Wiedererkennungsraten einiger Detektoren zeigt Abbildung \ref{fig:detektorenvergleich}. Die verwendete Einheit zum Vergleichen ist die \ac{AP}, was grob mit der \ac{AUC} übereinstimmt und von der Quelle so ausgewählt wurde. Der Vergleich wird als Balkendiagramm dargestellt anstelle vieler Recall-Precision-Kurven, um zahlreiche Algorithmen mit einer einzelnen Kennzahl vergleichbar zu machen, da genauere Details an dieser Stelle kaum Mehrwert bieten. Interessant ist noch die Nennung von OpenCV zur Gesichtsdetektion mit $AP = 68.57$ für den PASCAL-Datensatz \autocite{man1:neue_face_detect_nn, man1:PASCAL_challenge}. Es finden sich bedauerlicherweise keine Informationen über die Methodik, mit der OpenCV hier analysiert wurde. Die in C und C++ verfasste Programmbibliothek OpenCV verfügt über eine Reihe integrierter und gebrauchsfertiger Gesichtsdetektoren wie Eigenface, Fisher, Haar-Wavelets sowie die LBP-Histogramm-Methode. Welche nun in diesem Ergebnis repräsentiert ist, bleibt offen \cite{buecher:2017_learning_opencv_3}.

Der Gewinner des in Abbildung \ref{fig:detektorenvergleich} gezeigten Benchmarks, ,,HeadHunter'', ist ein komplexer Aufbau, aus 22 Teilmodellen bestehend, vor allem um verschiedene Variationen in der Kopfpose berücksichtigen zu können. Unter den restlichen Top-Kandidaten dominieren Abkömmlinge sowohl des Viola-Jones-Algorithmus als auch \ac{HOG}-basierte Detektoren in Kombination mit \ac{SVM} \cite{man1:headhunter}.


