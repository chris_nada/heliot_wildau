#include <iostream>
#include "einstellungen.h"

std::string Einstellungen::get_ip(Einstellungen::ADDRESSTYP addresstyp, const std::string& ini_key) {
    // Auslesen
    const std::string IP_FULL = werkzeuge::get_aus_ini(CONFIG_INI, ini_key);
    if (std::count(IP_FULL.begin(), IP_FULL.end(), ('.')) == 3 &&
        std::count(IP_FULL.begin(), IP_FULL.end(), (':')) == 1) {
        werkzeuge::log("\tIPv4 gueltig.");
        const std::string IP   = werkzeuge::tokenize(IP_FULL, ':')[0];
        const std::string PORT = werkzeuge::tokenize(IP_FULL, ':')[1];
        switch (addresstyp) {
            case ADDRESSTYP::IPv4:
                if (werkzeuge::is_valid_ipv4(IP)) return IP;
                else break;
            case ADDRESSTYP::PORT: return PORT;
        }
    }
    // Defaults
    werkzeuge::log("\tUngueltige IPv4-Addresse: " + IP_FULL);
    switch (addresstyp) {
        case ADDRESSTYP::IPv4: return "127.0.0.1";
        case ADDRESSTYP::PORT: return "8787";
    }
    werkzeuge::log("\tUngueltiger Addresstyp");
    return "";
}

const std::pair<std::string, uint16_t>& Einstellungen::server_ip() {
    static const auto IP   = get_ip(ADDRESSTYP::IPv4, "SERVER");
    static const auto PORT = get_ip(ADDRESSTYP::PORT, "SERVER");
    static const auto PORT_INT = werkzeuge::parse<uint16_t>(PORT, 8787);
    static const auto ds_server = std::make_pair(IP, PORT_INT);
    return ds_server;
}
