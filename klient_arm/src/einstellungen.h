#pragma once

#include <string>
#include <werkzeuge.hpp>
#include <algorithm>

namespace Einstellungen {

    /// Teile, aus denen sich Netzwerkaddressen zusammensetzen.
    enum ADDRESSTYP { IPv4, PORT };

    /// Pfad zur Konfigurationsdatei (mit Dateiendung).
    constexpr const char* CONFIG_INI = "config.ini";

    /// Maximale Länge eines Nutzer-Logins in Anzahl Zeichen.
    constexpr unsigned LOGIN_LAENGE_MAX = 20;

    /// Liefert die Netzwerkaddresse des Datenschutzservers inkl. Port als `second`.
    const std::pair<std::string, uint16_t>& server_ip();

    /**
     * Liefert Netzwerkaddressen (IPv4) gegebenen Keys aus der `CONFIG_INI` im Format <XXX.XXX.XXX.XXX:XXXX>.
     * @param addresstyp Bestimmt, ob nur IP-Addresse oder nur Port ausgelesen werden soll; durch `:` getrennt.
     * @param ini_key Schlüssel in der ini-Datei.
     */
    std::string get_ip(ADDRESSTYP addresstyp, const std::string& ini_key);

};
