#pragma once

#include <FL/Fl_Window.H>
#include <werkzeuge.hpp>

/**
 * Elternklasse für alle verwendeten Fenster.
 * Ausnahme: Dateiwahldialoge und Pop-Up-Dialoge.
 */
class Fenster {

public:

    /**
     * Konstruktor. Initialisiert das Fenster mit seinen Kindelementen.
     * @param titel Anzuzeigender Fenstertitel.
     */
    explicit Fenster(const char* titel);

    /// Öffnet das Fenster und startet dessen Programmlogik.
    virtual void start();

    /// Virtueller Destruktor.
    virtual ~Fenster();

    /// Fensterauflösung X.
    static constexpr int breite = 800;

    /// Fensterauflösung Y.
    static constexpr int hoehe = 600;

    /// Schriftgröße auf Knöpfen.
    static constexpr Fl_Fontsize btn_fontsize = 32;

    /// Schrifttyp auf Knöpfen.
    static constexpr Fl_Font btn_font = FL_HELVETICA;

    /// Padding Typ A (Höhe).
    static constexpr int padding_a = hoehe / 16;

    /// Padding Typ B (Breite).
    static constexpr int padding_b = breite / 4;

    /// Dynamische Knopfgröße X (groß).
    static constexpr int btn_size_x = breite / 2;

    /// Dynamische Knopfgröße Y (groß).
    static constexpr int btn_size_y = hoehe / 8;

protected:

    /**
     * Vererbter Konstruktor.
     * @param fenster
     */
    explicit Fenster(Fl_Window* fenster);

    /// Gibt Y Koordinate für das n-te Bildelement bei i Bildelementen insgesamt.
    static int get_y(int n, int i = 3) {
        return (i + n) * padding_a + n * padding_a + n * btn_size_y;
    };

    /// Setzt einen gleichartigen Stil für alle Fenster.
    static void init_stil(Fl_Window* fenster);

    /// FLTK Fenster.
    Fl_Window* fenster;

};
