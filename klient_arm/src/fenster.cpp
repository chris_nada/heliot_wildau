#include "fenster.h"
#include <FL/Fl.H>

Fenster::Fenster(Fl_Window* fenster) : fenster(fenster) {
    //
}

Fenster::Fenster(const char* titel) : Fenster(new Fl_Window(0, 0, breite, hoehe, titel)) {
    //
}

void Fenster::start() {
    fenster->show();
}

Fenster::~Fenster() {
    werkzeuge::log("~Fenster()");
    delete fenster;
}

void Fenster::init_stil(Fl_Window* fenster) {
    //static const auto farbe = fl_rgb_color(0xE0, 0xE0, 0xE0);
    //fenster->color(farbe);
    for (int c = 0; c < fenster->children(); ++c) {
        Fl_Widget* child = fenster->child(c);
        child->box(Fl_Boxtype::FL_THIN_UP_BOX);
        child->labelfont(FL_HELVETICA);
        //child->color(farbe);
    }
}
