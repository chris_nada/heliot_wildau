#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/fl_ask.H>

#include "fenster_kamera.h"
#include "verbindung.hpp"
#include "einstellungen.h"

Fenster_Kamera::Fenster_Kamera() : Fenster("Kamera aktiv"), valide_lerndaten_gesammelt(0) {

    // Kameraansichtsfenster
    fl_kamerastream = new Fl_Kamerastream(padding_a, padding_a);
    try { fps = std::stof(werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "FPS")); }
    catch (std::exception& e) { fps = 10; } // Standardwert

    // Buttons
    Fl_Button* btn_ende   = new Fl_Button(padding_a, Fenster::hoehe - 40 - padding_a, 160, 40, "Abbrechen");
    btn_ende->callback(abbrechen, this);

    // Labels
    lbl_progress = new Fl_Output(btn_ende->x() + 180, btn_ende->y(), btn_ende->w()*2, btn_ende->h());
    lbl_progress->box(FL_NO_BOX);
    lbl_progress->align(FL_ALIGN_CENTER);
    lbl_progress->clear_visible();

    // Fenster initialisieren.
    init_stil(fenster);
    fenster->fullscreen(); // TODO: Testen
    fenster->set_modal();
    fenster->end();
}

Fenster_Kamera::Fenster_Kamera(const std::string& nutzer) : Fenster_Kamera() {
    Fenster_Kamera::nutzer = nutzer;
    if (!nutzer.empty()) {
        lbl_progress->set_visible();
        lbl_progress->insert("Lerndaten übermittelt: 0%");
    }
}

void Fenster_Kamera::abbrechen(Fl_Widget* w, void* data) {
    (void) w;
    Fenster_Kamera* fk = (Fenster_Kamera*) data;
    if (fk) {
        fk->flag_abbrechen = true;
        delete fk;
        fk = nullptr;
    }
    else std::cerr << "Fenster_Kamera::" << __func__ << " data nullptr" << std::endl;
}

void Fenster_Kamera::bild_senden(Fl_Widget* w, void* data) {
    werkzeuge::log("Fenster_Kamera::bild_senden()");
    (void) w; // Welches Widget auslöser ist, ist uninteressant

    Fenster_Kamera* fk = (Fenster_Kamera*) data;
    if (fk && !fk->flag_abbrechen) {
        // Bilddaten beschaffen
        const auto& daten = fk->fl_kamerastream->get_bild();
        if (!daten.empty()) {
            std::stringstream body;
            body << fk->nutzer << ';';
            body << daten;

            // Anfrage senden
            auto& klient = Verbindung::klient();
            auto response = klient.Put("/neues_bild", body.str(), "text/plain");

            // Antwort auswerten; Format: x;y;width;height
            if (response) {
                werkzeuge::log("PUT /neues_bild Status = " + std::to_string(response->status));

                const auto& neue_detektionen = get_detections_from_string(response->body);
                if (!neue_detektionen.empty()) {
                    fk->fl_kamerastream->set_face_detections(neue_detektionen);
                    static std::string lbl_text; // Label aktualisieren
                    lbl_text.clear();
                    lbl_text.append("    Lerndaten übermittelt: ");
                    lbl_text.append(std::to_string((int) ((100 * fk->valide_lerndaten_gesammelt) / 20)) + "%");
                    fk->lbl_progress->replace(0, fk->lbl_progress->size(), lbl_text.c_str(), lbl_text.size());
                    fk->valide_lerndaten_gesammelt++; // zählen
                }
            }
            else { // Keine Antwort
                fl_alert("Keine Verbindung zum Server.");
                abbrechen(nullptr, data);
                return;
            }
        }
        else werkzeuge::log("Keine Bilddaten.");
    }
}

void Fenster_Kamera::bild_senden_zur_erkennung(Fl_Widget* w, void* data) {
    werkzeuge::log("Fenster_Kamera::bild_senden_zur_erkennung()");
    (void) w; // Welches Widget auslöser ist, ist uninteressant

    Fenster_Kamera* fk = (Fenster_Kamera*) data;
    if (fk && !fk->flag_abbrechen) {
        // Bilddaten beschaffen
        const auto& daten = fk->fl_kamerastream->get_bild();
        if (!daten.empty()) {

            // Anfrage senden
            auto& klient = Verbindung::klient();
            auto response = klient.Put("/recognize", daten, "text/plain");

            // Antwort auswerten; Format: x;y;width;height;label
            if (response) {
                werkzeuge::log("PUT /recognize Status = " + std::to_string(response->status));

                const auto& neue_detektionen = get_detections_from_string(response->body);
                if (!neue_detektionen.empty()) {
                    fk->fl_kamerastream->set_face_detections(neue_detektionen);
                }
            }
            else { // Keine Antwort
                fl_alert("Keine Verbindung zum Server.");
                fk->flag_abbrechen = true;
                abbrechen(nullptr, data);
                return;
            }
        }
        else werkzeuge::log("Keine Kameraverbindung.");
    }
}

void Fenster_Kamera::timer_starten(void* fenster) {
    Fenster_Kamera* fk = (Fenster_Kamera*) fenster;
    if (fk && !fk->flag_abbrechen) {
        static const float refresh = 1.f / fk->fps;

        // Automatisch senden?
        static std::string auto_send = werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "AUTO_SEND");
        if (auto_send == "true") {
            static const float send_intervall = 1.f; // Intervall in Sekunden
            static float zaehler = 0.f;
            zaehler += refresh;

            // Bild Senden
            if (zaehler > send_intervall) { // Intervall erreicht?

                // Erkennungsmodus
                if (fk->nutzer.empty()) bild_senden_zur_erkennung(nullptr, fenster);
                else {
                    // Lerndatensammelmodus
                    bild_senden(nullptr, fenster);

                    // Genügend valide Bilder geschickt? Dann bedanken + schließen.
                    if (fk->valide_lerndaten_gesammelt > 20) { // Anzahl potenzieller Lerndaten
                        fl_alert("Lerndatenerstellung abgeschlossen.\nVielen Dank für die Teilnahme!");
                        abbrechen(nullptr, fenster);
                        return;
                    }
                }
                zaehler = 0;
            }
        }

        // Methode wiederholt aufrufen
        if (!fk->flag_abbrechen) {
            fk->fl_kamerastream->redraw();
            Fl::repeat_timeout(refresh, timer_starten, fenster);
        }
    }
}

void Fenster_Kamera::start() {
    Fenster::start();
    timer_starten(this);
}

Fenster_Kamera::~Fenster_Kamera() {
    Fl::remove_timeout(timer_starten, nullptr);
    delete fl_kamerastream;
    fl_kamerastream = nullptr;
}

std::vector<std::pair<std::array<int, 4>, std::string>>
Fenster_Kamera::get_detections_from_string(const std::string& string) {

    std::vector<std::pair<std::array<int, 4>, std::string>> antwort;

    // Versuchen Werte der Detektionen zu ermitteln (=parsen):
    std::stringstream ss(string);
    std::vector<std::array<int, 4>> temp_detections;

    // 1 Zeile = 1 Detektion
    for (std::string zeile; std::getline(ss, zeile);) {
        try {
            auto tokens = werkzeuge::tokenize(zeile, ';');
            int x = std::stoi(tokens.at(0));
            int y = std::stoi(tokens.at(1));
            int width = std::stoi(tokens.at(2));
            int height = std::stoi(tokens.at(3));

            // Zusammenfassen
            std::array<int, 4> detection{x, y, width, height};
            temp_detections.emplace_back(detection);
            if (tokens.size() == 5) antwort.emplace_back(detection, tokens[4]);
            else antwort.emplace_back(detection, "");
        } catch (std::exception& e) { /* Zeile ungültig */ }
    }

    werkzeuge::log("\tAnzahl erkannter Gesichter = " + std::to_string(antwort.size()));
    return antwort;
}
