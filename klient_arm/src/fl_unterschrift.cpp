#include <FL/Fl.H>
#include <sstream>
#include <opencv2/opencv.hpp>
#include "fl_unterschrift.h"
#include "einstellungen.h"

Fl_Unterschrift::Fl_Unterschrift(int pos_x, int pos_y) : Fl_Widget(pos_x, pos_y, SIZE_X, SIZE_Y, "") {
    reset();
}

void Fl_Unterschrift::reset() {
    // Alle Pixel weiß initialisieren
    for (uint16_t x = 0; x < SIZE_X; ++x) {
        for (uint16_t y = 0; y < SIZE_Y; ++y) {
            px[y][x][0] = 0xFF;
        }
    }
}

void Fl_Unterschrift::draw() {
    fl_draw_image((const uint8_t*)&px, x(), y(), SIZE_X, SIZE_Y, 1, SIZE_X);
}

int Fl_Unterschrift::handle(int event) {

    // Fl Event abfragen
    switch (event) {
        case FL_PUSH:
            return 1;
        case FL_DRAG:
        {
            // Mausklick relativ zum Widget
            int x = Fl::event_x() - this->x();
            int y = Fl::event_y() - this->y();

            // Pixel färben
            static constexpr int d = 8; // Dicke
            if (x + d < SIZE_X && x - d >= 0 && y + d < SIZE_Y && y - d >= 0) {
                for (int i = -d; i < d; ++i) {
                    for (int j = -d; j < d; ++j) {
                        px[y + i][x + j][0] = 0x00;
                    }
                }
            }
        }
            redraw();
            return 1; // Event bearbeitet
        default: break;
    }

    // Event wurde nicht bearbeitet
    return 0;
}

std::string Fl_Unterschrift::get_data() const {
    // Bild in ein OpenCV Format bringen
    cv::Mat mat(SIZE_Y, SIZE_X, CV_8UC3);
    for (int y = 0; y < SIZE_Y; ++y) {
        for (int x = 0; x < SIZE_X; ++x) {
            auto& pixel = mat.at<cv::Vec3b>(y, x);
            pixel[0] = static_cast<uint8_t>(px[y][x][0]);
            pixel[1] = static_cast<uint8_t>(px[y][x][0]);
            pixel[2] = static_cast<uint8_t>(px[y][x][0]);
        }
    }

    // Bild zu std::string konvertieren
    std::vector<uint8_t> buffer;
    cv::imencode("." + werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT"), mat, buffer);
    std::stringstream ss;
    for (auto c : buffer) ss << c;
    return ss.str();
}
