#include "fenster_start.h"
#include "fenster_registrieren.h"
#include "fenster_kamera.h"

#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Pack.H>

Fenster_Start::Fenster_Start() : Fenster("Fenster_Start") {

    // Elemente aufbauen
    {
        // Layouthilfe
        Fl_Pack* pack = new Fl_Pack(padding_b, 2*padding_a, breite - 2*padding_b, hoehe - 2*padding_a);
        pack->begin();

        // Buttons erstellen
        Fl_Button* btn_test = new Fl_Button(padding_b, get_y(0), btn_size_x, btn_size_y, "Start");
        btn_test->labelsize(btn_fontsize);
        btn_test->labelfont(btn_font);
        btn_test->callback(test);

        if (werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "SHOW_LOGIN") == "true") {
            Fl_Button* btn_login = new Fl_Button(padding_b, get_y(1), btn_size_x, btn_size_y, "Login");
            btn_login->labelsize(btn_fontsize);
            btn_login->labelfont(btn_font);
            btn_login->callback(login);
        }

        Fl_Button* btn_register = new Fl_Button(padding_b, get_y(2), btn_size_x, btn_size_y, "Registrieren");
        btn_register->labelsize(btn_fontsize);
        btn_register->labelfont(btn_font);
        btn_register->callback(registrieren);
        pack->spacing(padding_a);
        pack->align(FL_ALIGN_CENTER);
        pack->end();
    }
    fenster->end();
}

void Fenster_Start::test(Fl_Widget* w) {
    log("Fenster_Start::test()");
    (void) w;

    // Kamerafenster starten
    Fenster_Kamera* fenster_kamera = new Fenster_Kamera(""); // leeres Login = Erkennungsmodus
    fenster_kamera->start();
}

void Fenster_Start::login(Fl_Widget* w) {
    log("Fenster_Start::login()");
    (void) w;
    const char* login_p = fl_input("Login", "");

    // Ok geklickt?
    if (login_p && !std::string(login_p).empty()) {
        const std::string login(login_p);
        log("\tLogin = <" + login + ">");

        // Sicherheitscheck Eingabe
        if (!werkzeuge::validieren(login)) {
            fl_alert("Login ungültig.");
            return;
        }

        // TODO: Check Login existiert?

        // Kamerafenster starten
        Fenster_Kamera* fenster_kamera = new Fenster_Kamera(login);
        fenster_kamera->start();
    }
}

void Fenster_Start::registrieren(Fl_Widget* w) {
    log("Fenster_Start::registrieren()");
    (void) w;
    Fenster_Registrierung* fenster_registrieren = new Fenster_Registrierung();
    fenster_registrieren->start();
}
