#include "fl_kamerastream.h"
#include "einstellungen.h"
#include <FL/fl_draw.H>

Fl_Kamerastream::Fl_Kamerastream(int pos_x, int pos_y) : Fl_Box(pos_x, pos_y, SIZE_X, SIZE_Y), cam(0) {
    // ctor
}

void Fl_Kamerastream::draw() {
    capture();
    if (cv_buffer.empty() || !cv_buffer.data) return;
    //fl_draw_image((const uint8_t*)&px, x(), y(), SIZE_X, SIZE_Y, 3); //Alternative
    Fl_RGB_Image fltk_img((const uint8_t*)&px, SIZE_X, SIZE_Y);
    image(fltk_img);
    Fl_Box::draw();
}

int Fl_Kamerastream::handle(int event) {
    switch (event) { default: break; } // ungenutzt
    return 0;
}

bool Fl_Kamerastream::capture() {
    // Zugriffscheck
    if (!cam.isOpened()) {
        cam.open(0); // 0 = Erste Kamera
        if (!cam.isOpened()) {
            std::cerr << "Kein Kamerazugriff" << std::endl;
            return false;
        }
    }
    // Bild in Rohdaten umwandeln
    cam.read(cv_buffer);
    if (cv_buffer.empty() || !cv_buffer.data) return false; // ungültige Bilddaten
    cv_buffer_orig = cv_buffer.clone(); // Kopie erstellen, die unbearbeitet bleibt

    // Erkennungen ins Bild eintragen
    for (size_t i = 0; i < face_detektionen.size() && i < face_namen.size(); ++i) {
        const auto& rect = face_detektionen[i];
        cv::rectangle(cv_buffer, rect, cv::Scalar(0xFF, 0xFF, 0x00));

        // Label schreiben
        if (!face_namen[i].empty()) {
            cv::putText(cv_buffer, face_namen[i], cv::Point(rect.tl().x, rect.br().y+16),
                        cv::FONT_HERSHEY_PLAIN,
                        1.5, // Schriftgröße
                        cvScalar(0x00,0xFF,0x00), 1, CV_AA
            );
        }
    }

    // Für FLTK umwandeln
    static cv::Mat temp;
    cv::resize(cv_buffer, temp, cv::Size(SIZE_X, SIZE_Y));
    //cv::flip(cv_buffer, temp, 1); // spiegeln
    for (int y = 0; y < SIZE_Y; ++y) {
        for (int x = 0; x < SIZE_X; ++x) {
            const auto& wert = temp.at<cv::Vec3b>(y,x);
            px[y][x][0] = wert[2]; // R
            px[y][x][1] = wert[1]; // G
            px[y][x][2] = wert[0]; // B
        }
    }
    return true;
}

std::string Fl_Kamerastream::get_bild() const {
    std::stringstream ss;
    if (!cv_buffer_orig.empty()) {
        try {
            std::vector<uint8_t> buffer;
            cv::imencode("." + werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT"), cv_buffer_orig, buffer);
            for (auto c : buffer) ss << c;
        } catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    }
    return ss.str();
}

Fl_Kamerastream::~Fl_Kamerastream() {
    cam.release();
}

void
Fl_Kamerastream::set_face_detections(const std::vector<std::pair<std::array<int, 4>, std::string>>& face_detections) {

    // Alten Speicher leeren
    face_namen.clear();
    face_detektionen.clear();

    // Rechtecke erstellen
    for (const auto& d : face_detections) {
        face_detektionen.emplace_back(d.first[0], d.first[1], d.first[2], d.first[3]);
        face_namen.push_back(d.second);
    }
}
