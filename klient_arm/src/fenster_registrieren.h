#pragma once

#include "fenster.h"
#include "fl_unterschrift.h"
#include "einstellungen.h"

#include <FL/Fl_Input.H>

class Fenster_Registrierung final : public Fenster {

public:

    Fenster_Registrierung();

    static constexpr unsigned LOGIN_LAENGE_MAX = Einstellungen::LOGIN_LAENGE_MAX;

private:

    static void registrieren(Fl_Widget* w, void* data);

    static void abbrechen(Fl_Widget* w, void* data);

    Fl_Unterschrift* unterschrift;

    Fl_Input* input_login;

    Fl_Input* input_vorname;

    Fl_Input* input_nachname;

};
