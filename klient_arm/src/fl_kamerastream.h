#pragma once

#undef Status // Workaround (am besten auskommentieren zum Nachvollziehen)

#include "fenster.h"
#include <opencv2/opencv.hpp>
#include <FL/Fl_Box.H>

class Fl_Kamerastream final : public Fl_Box {

public:

    Fl_Kamerastream(int pos_x, int pos_y);

    void draw() override;

    int handle(int event) override;

    std::string get_bild() const;

    /**
     * Nimmt sofort ein Bild auf in `cv_buffer`. Wird außerdem in das Feld
     * `px` kopiert (und konvertiert zu quasi-BMP).
     * @return true, falls erfolgreich ein Bild aufgenommen werden konnte
     *         false, falls die Bildaufnahme fehlschlug (siehe Ausgabe in `std::cerr`).
     */
    bool capture();

    /// Schließt den Kamerazugriff.
    virtual ~Fl_Kamerastream();

    /// Breite des Objekts.
    static constexpr int SIZE_X = Fenster::breite - 2 * Fenster::padding_a;

    /// Höhe des Objekts.
    static constexpr int SIZE_Y = static_cast<int>(SIZE_X * (9.f / 16.f));

    /// Setter: Erkannte Gesichter.
    void set_face_detections(const std::vector<std::pair<std::array<int, 4>, std::string>>& face_detections);

private:

    /// OpenCV Kamerazugriff.
    cv::VideoCapture cam;

    /**
     * Rohe Pixeldaten in Originalgröße, die als OpenCV-Buffer genutzt werden.
     * Wird überlagert mit Rechtecken, die die erkannten Gesichter beinhalten.
     */
    cv::Mat cv_buffer;

    /// Rohe Pixeldaten in Originalgröße, die uneditiert zum senden an den Server genutzt werden.
    cv::Mat cv_buffer_orig;

    /// Rohe Pixeldaten.
    uint8_t px[SIZE_Y][SIZE_X][3];

    /// Liste 'aktueller' erkannter Gesichter. Array-Format: x;y;width;height.
    std::vector<std::string> face_namen;

    /// Liste 'aktueller' erkannter Gesichter als (OpenCV) cv::Rect.
    std::vector<cv::Rect> face_detektionen;

};
