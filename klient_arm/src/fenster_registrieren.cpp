#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl.H>
#include <FL/fl_ask.H>

#include "fenster_registrieren.h"
#include "fl_unterschrift.h"
#include "einstellungen.h"
#include "fenster_kamera.h"
#include "verbindung.hpp"

Fenster_Registrierung::Fenster_Registrierung() : Fenster("Registrierung") {

    // Inputs
    input_login    = new Fl_Input(breite/3, padding_a,      200, 25, "Login:");
    input_vorname  = new Fl_Input(breite/3, padding_a + 40, 200, 25, "Vorname:");
    input_nachname = new Fl_Input(breite/3, padding_a + 80, 200, 25, "Nachname:");

    // Unterschrift
    unterschrift = new Fl_Unterschrift(padding_a, padding_a + 140);

    // Buttons
    Fl_Button* btn_registrieren = new Fl_Button(padding_b,       padding_a + 400, 160, 40, "Registrieren");
    Fl_Button* btn_abbrechen    = new Fl_Button(padding_b + 200, padding_a + 400, 160, 40, "Abbrechen");
    btn_registrieren->callback(registrieren, this);
    btn_abbrechen->callback(abbrechen, this);

    // Fenster initialisieren
    init_stil(fenster);
    fenster->fullscreen();
    fenster->set_modal();
    fenster->end();
}

void Fenster_Registrierung::registrieren(Fl_Widget* w, void* data) {
    (void) w;
    Fenster_Registrierung* fr = (Fenster_Registrierung*) data;
    if (fr) {
        // Strings aus Inputs sammeln
        std::string login(fr->input_login->value());
        std::string vorname(fr->input_vorname->value());
        std::string nachname(fr->input_nachname->value());
        werkzeuge::log("Registrieren: " + login + ", " + vorname + ", " + nachname);

        /* Sicherheitschecks */

        // Darf nicht leer sein
        if (login.empty() || vorname.empty() || nachname.empty()) {
            fl_alert("Eingaben dürfen nicht leer sein.");
            return;
        }

        // Ungültige Charaktere?
        using werkzeuge::validieren;
        if (!validieren(login) || !validieren(vorname) || !validieren(nachname)) {
            fl_alert("Eingaben enthalten ungültige Zeichen.");
            return;
        }

        // Länge überschritten?
        if (login.size() > LOGIN_LAENGE_MAX) {
            fl_alert("Länge des Nutzernamens (Login)\ndarf %d Zeichen nicht überschreiten.", LOGIN_LAENGE_MAX);
            return;
        }

        // Verbindung aufbauen
        auto& klient = Verbindung::klient();
        const std::string  name_hash = vorname + " " + nachname;
        const std::string& pseu_hash = login;
        const std::string  body = name_hash + ";" + pseu_hash + ";" + fr->unterschrift->get_data();
        const auto& response = klient.Put("/registrieren", body, "text/plain");

        // Verbindungsfehler?
        if (!response) {
            fl_alert("Keine Verbindung zum Server.");
            return;
        }

        // Erfolg -> Statusmeldung + Fenster schließen
        if (response->status == 200) {
            fl_alert("Registrierung erfolgreich.\nVielen Dank für die Teilnahme!");

            // Kamerafenster starten
            Fenster_Kamera* fenster_kamera = new Fenster_Kamera(login);
            fenster_kamera->start();
            abbrechen(nullptr, fr);
        }
        else fl_alert("Der Login ist leider schon vergeben."); // (oder Server-Kommunikation schlug fehl)
    }
}

void Fenster_Registrierung::abbrechen(Fl_Widget* w, void* data) {
    (void) w;
    Fenster_Registrierung* fr = (Fenster_Registrierung*) data;
    if (fr) delete fr;
}
