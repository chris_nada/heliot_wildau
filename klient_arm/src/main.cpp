#include "fenster_start.h"
#include <werkzeuge.hpp>
#include <new>
#include <FL/Fl.H>
#include <iostream>

/**
 * Gibt eine Rückmeldung, sollte das Programm wegen 
 * zu wenig freiem Arbeitsspeicher abstürzen.
 */
void speicherfehler() {
    std::cerr << "Kritischer Fehler: Kein freier Arbeitsspeicher." << std::endl;
    std::set_new_handler(nullptr);
}

/**
 * Programmeintrittspunkt.
 */
int main(int argc, char** argv) {
    werkzeuge::log("main()");
    std::set_new_handler(speicherfehler);

    (void) argc;
    (void) argv;

    // UI starten
    Fl::scheme("gtk+"); // base gtk+ gleam plastic
    Fenster_Start hauptfenster;
    hauptfenster.start();
    return Fl::run();
}
