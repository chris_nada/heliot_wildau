#pragma once

#include "fenster.h"
#include "fl_kamerastream.h"

#include <FL/Fl_Output.H>

/**
 * Fenster, das
 * + die Verwaltung des Kamerabilds,
 * + die Bildübertragung an den Server und
 * + die Gesichtserkennung
 * beinhaltet.
 */
class Fenster_Kamera final : public Fenster {

public:

    /**
     * Konstruiert das Fenster und meldet `nutzer` an zum Übertragen von Bilddaten.
     * @param nutzer Login des Nutzers, dessen Bilddaten übermittelt werden.
     */
    explicit Fenster_Kamera(const std::string& nutzer);

    /// Startet die Logis dieses Fensters.
    void start() override;

    /// Verwaltet das Schließen des Fensters.
    virtual ~Fenster_Kamera();

private:

    /// Konstruktor. Erzeugt Kind-UI-Elemente.
    Fenster_Kamera();

    /// Hilfsfunktion - Parst aus einer Serverantwort alle Detektionen
    static std::vector<std::pair<std::array<int, 4>, std::string>>
    get_detections_from_string(const std::string& string);

    /// Callback zum Schließen des Fensters.
    static void abbrechen(Fl_Widget* w, void* data);

    /// Sendet, wenn gültig, das aktuelle Frame an den Server.
    static void bild_senden(Fl_Widget* w, void* data);

    /// Sendet, wenn gültig, das aktuelle Frame an den Server zur Gesichtserkennung.
    static void bild_senden_zur_erkennung(Fl_Widget* w, void* data);

    /// Startet den Timer zum senden von Bilddaten in festen Intervallen.
    static void timer_starten(void* fenster);

    /// Stellt die Framerate ein für das aktualisieren des Kamerabildes.
    float fps;

    /// Angemeldeter Nutzer. Wenn leer, dann wird Gesichtserkennung ausgeführt ohne Datenübermittlung.
    std::string nutzer;

    /// Kamerabildverwaltung.
    Fl_Kamerastream* fl_kamerastream;

    /// Statustext.
    Fl_Output* lbl_progress;

    /// Zählt, wieviele Bilder an den Server gesendet wurden, die valide Gesichter enthalten.
    unsigned valide_lerndaten_gesammelt;

    /// Sicherheitscheck, ob das Fenster zu schließen ist.
    bool flag_abbrechen = false;

};
