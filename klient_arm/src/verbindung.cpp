#include <iostream>
#include "verbindung.hpp"
#include "einstellungen.h"

const std::string& Verbindung::IP() {
    static const std::string IP = std::get<std::string>(Einstellungen::server_ip());
    //werkzeuge::log("\tIP = " + IP);
    return IP;
}

uint16_t Verbindung::PORT() {
    static const uint16_t PORT = std::get<uint16_t>(Einstellungen::server_ip());
    //werkzeuge::log("\tPORT = " + std::to_string(PORT));
    return PORT;
}

httplib::Client& Verbindung::klient() {
    static httplib::Client* static_klient = nullptr;
    if (!static_klient) {
        werkzeuge::log("Verbindung wird erstellt...");
        if (werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "SSL") == "true") {
            static_klient = new httplib::SSLClient(IP().c_str(), PORT(), TIMEOUT);
            httplib::SSLClient* static_ssl = (httplib::SSLClient*) static_klient;
            static_ssl->enable_server_certificate_verification(false);
        }
        else {
            static_klient = new httplib::Client(IP().c_str(), PORT(), TIMEOUT);
        }
    }
    return *static_klient;
}
