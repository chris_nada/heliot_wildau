#pragma once

#include <FL/Fl_Widget.H>
#include <FL/fl_draw.H>
#include <cmath>

#include "fenster.h"

class Fl_Unterschrift final : public Fl_Widget {

public:

    /**
     *
     * @param pos_x
     * @param pos_y
     */
    Fl_Unterschrift(int pos_x, int pos_y);

    /**
     * Versetzt das Bild in den Ursprungszustand;
     */
    void reset();

    void draw() override;

    int handle(int event) override;

    /**
     * @brief Liefert die Bilddaten konvertiert in das Bildformat, das in der Konfiguration eingestellt ist.
     * Sie können so beispielsweise unverändert in einen `std::ofstream` geschrieben werden.
     * @return Bilddaten als `std::string`.
     */
    std::string get_data() const;

    /// Breite des Objekts.
    static constexpr int SIZE_X = Fenster::breite - 2 * Fenster::padding_a;

    /// Höhe des Objekts.
    static constexpr int SIZE_Y = 200;

private:

    /// Rohe Pixeldaten.
    uint8_t px[SIZE_Y][SIZE_X][1];

};
