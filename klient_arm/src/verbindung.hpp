#include <string>
#include <httplib.h>

class Verbindung final {

public:

    // IPv4.
    static const std::string& IP();

    // Port.
    static uint16_t PORT();

    // Verwendeter Verbindungsslot.
    static httplib::Client& klient();

    /// Timeout in Sekunden.
    static constexpr time_t TIMEOUT = 5;

};
