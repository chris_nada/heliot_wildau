#pragma once

#include <FL/Fl_Window.H>
#include <werkzeuge.hpp>

#include "fenster.h"

using werkzeuge::log;

class Fenster_Start final : public Fenster {

public:

    /// Standardkonstruktor.
    Fenster_Start();

private:

    static void test(Fl_Widget* w = nullptr);

    static void login(Fl_Widget* w = nullptr);

    static void registrieren(Fl_Widget* w = nullptr);

};
