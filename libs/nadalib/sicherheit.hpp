#pragma once

#include <string>
#include <cryptopp/secblock.h>

namespace sicherheit {

    /// Sicherer String, dessen Speicher bei der Freigabe überschrieben wird.
    using string = std::basic_string<char, std::char_traits<char>, CryptoPP::AllocatorWithCleanup<char>>;

    /// 'Sichere' String-Speicherverwaltung, die bei Freigabe den Speicher überschreibt.
    using string_alloc = CryptoPP::AllocatorWithCleanup<char>;

    /// Liefert den SHA3-224 Digest für gegebene Eingabe als Uppercase-Hex-String.
    std::string sha3_224(const char* text);

    /// Liefert den SHA3-512 Digest für gegebene Eingabe als Uppercase-Hex-String.
    std::string sha3_512(const char* text);

    /**
     * Liefert den ARGON-2 Digest für gegebene Eingabe als Uppercase-Hex-String.
     * Dabei wird folgende Konfiguration verwendet:
     * Version Argon2_id 13.
     * 2 Iterationen, 1 Thread, 1 << 16 KiBi Speicher.
     * @return Ausgabe: 32 Chars.
     */
    std::string argon2(const char* text, const char* salt);

    /**
     * Verschlüsselt gegebene Daten (als String) direkt in eine Datei.
     * @param daten Zu verschlüsselnde Daten.
     * @param key Zu verwendener Schlüssel.
     * @param pfad Pfad zur speichernden Datei.
     * @return Verschlüsselung erfolgreich?
     */
    bool aes_encrypt(const std::string& daten, const char* key, const std::string& pfad);

    /**
     * Entschlüsselt Daten aus einer Datei mit gegebenem Pfad.
     * @param daten Die entschlüsselten Daten werden in diesen String geschrieben.
     * @param key Zu verwendener Schlüssel.
     * @param pfad Pfad zur speichernden Datei.
     * @return Verschlüsselung erfolgreich?
     */
    bool aes_decrypt(std::string& daten, const char* key, const std::string& pfad);

    /**
     * @brief Eingaben über `std::cin` können hiermit 'stumm' geschaltet werden.
     * Alle nach Aktivierung folgenden `std::cin`s werden in der Konsole
     * nicht dargestellt, so wie es bspw. bei Linux-Passworteingaben üblich ist.
     * @param enable Bestimmt an / aus.
     */
    void console_echo(bool enable);
    
}
