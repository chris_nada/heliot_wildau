#pragma once

#include <string>
#include <vector>

/**
 * Hier befinden sich Hilfsfunktionen zum Umgang mit Strings, Zahlen, Dateien etc.
 */
namespace werkzeuge {

    /**
     * Loggt eine Nachricht im Debugmodus in die Konsole.
     * Bei Release-builds ist diese Funktion deaktiviert.
     * @param nachricht
     */
    void log(const std::string& nachricht);

    /**
     * Ueberprüft einen String auf (Kommunikationsschnittstelle) ungueltige `char`s.
     * @param str Zu validierende Eingabe.
     * @return Validierung erfolgreich?
     */
    bool validieren(const std::string& str);

    /**
     * Prueft, ob eine Datei existiert.
     * @param dateiname Pfad zur Datei. Darf relativ sein.
     */
    bool existiert_datei(const std::string& dateiname);

    /**
     * Prueft, ob ein Ordner existiert.
     * @param pfad Pfad zum Ordner. Darf relativ sein.
     */
    bool existiert_ordner(const std::string& pfad);

    /**
     * Liefert den Inhalt einer Datei als `std::string`.
     * @note Falls Datei nicht zugreifbar, wird ein `std::string::empty()` zurückgegeben.
     * @param pfad Relativer oder absoluter Pfad zur Datei.
     * @return Dateiinhalt oder `std::string::empty()`, also `""`.
     */
    std::string get_dateiinhalt(const std::string& pfad);

    /**
     * Liefert einen std::vector aus std::string, die aus einem Text durch einen Separator geteilt wurden.
     * @param text Aufzuteilender Text
     * @param token char, bei dem aufgeteilt wird
     * @return Textteile
     */
    std::vector<std::string> tokenize(const std::string& text, char token);

    /**
     * Entfernt aus gegebenem String Leerzeichen.
     * @param text Zu manipulierender String.
     */
    void remove_whitespace(std::string& text);

    /**
     * @brief Liest eine ini-Datei aus und liefert einen passenden Wert zu einem Schlüssel.
     * - Die Werte in der Datei müssen durch `=` getrennt sein.
     * - Es darf nur 1 = pro Zeile vorkommen.
     * - Aus Schlüssel und Wert werden ggf. Leerzeichen entfernt.
     * @param dateiname Pfad zur ini-Datei.
     * @param key Schlüssel (linke Seite vom `=`).
     * @return Wert (rechte Seite vom `=`).
     */
    std::string get_aus_ini(const std::string& dateiname, const std::string& key);

    /**
     * Liefert den 0-basierten n-ten Index eines `char` in einem String.
     * @warning Befinden sich < n Elemente von `c` im String, ist das Verhalten unspezifiziert.
     * @param text Zu untersuchender Text.
     * @param c Zu zaehlender `char`.
     * @param n Der Index dex n-ten Elements wird geliefert.
     * @return Index des n-ten Elements.
     */
    size_t get_index(const std::string& text, char c, size_t n);

    /**
     * Liefert das n(+1)-te Token aus einem String.
     * @param text Zu extrahierender Text.
     * @param c Token-Trennzeichen.
     * @param n 0-basierter Index der Tokens.
     * @return Das n(+1)-te Token.
     */
    std::string get_nth_token(const std::string& text, char c, size_t n);

    /**
     * Versucht eine Zahl aus einem String zu parsen.
     * @note Exception-Sicher. Bei Problemen wird `standard` geliefert.
     * @tparam T Datentyp. Darf Gleitkommazahl oder Ganzzahl sein.
     * @param string Zu parsender String.
     * @param standard Standardwert. `T()`, wenn nicht angegeben.
     * @return Geparste Zahl oder Standardwert.
     */
    template <typename T> T parse(const std::string& string, T standard = T());

    /**
     * Erstellt rekursiv ein Verzeichnis mithilfe des systemeigenen Win/Linux `mkdir` Befehls.
     * @param verzeichnis Pfad zum Verzeichnis.
     */
    void mkdir(const std::string& verzeichnis);

    /**
     * Konvertiert einen String zu Großbuchstaben.
     */
    void upper(std::string& string);

    /**
     * Konvertiert einen String zu Kleinbuchstaben.
     */
    void lower(std::string& string);

    /**
     * Konvertiert eine Zahl zu einem Hexadezimalstring.
     * @note Buchstaben werden durch `std::hex` als Kleinbuchstaben ausgegeben.
     */
    std::string hex(unsigned long long int nummer);

    /**
     * Liefert die ID des CPUs. Kann bspw. als maschinenabhaengiger Schluessel verwendet werden.
     * @return CPU-ID als Hex-String, Kleinbuchstaben.
     * @note CPU-ID = EDX + ECX.
     */
    std::string cpu_id();

    /**
     * Überprüft, ob es sich bei gegebenem String um eine valide IPv4-Addresse handelt.
     * @param ip IP-Addresse im Format 255.255.255.255.
     * @return Valide IP-Addresse?
     */
    bool is_valid_ipv4(const std::string& ip);

    /**
     * Eine 'alles ersetzen'-Methode für einen Text.
     * @param text Zu manipulierender Text.
     * @param alt Alter String.
     * @param neu Neuer String, der alle alten Strings ersetzt.
     */
    void replace_all(std::string& text, const std::string& alt, const std::string& neu);

    /**
     * Liefert die Namen aller Dateien, die sich in einem bestimmten Ordner befinden.
     * @param pfad Relativer oder absolter Pfad zum zu durchsuchenden Ordner.
     * @return Liste der Dateinamen (mit Endungen), die sich in dem Ordner befinden.
     */
    std::vector<std::string> alle_dateien(const std::string& pfad);

    /**
     * Liefert die Namen aller Unterverzeichnisse, die sich in einem bestimmten Ordner befinden.
     * @param pfad Relativer oder absolter Pfad zum zu durchsuchenden Ordner.
     * @return Liste der Unterverzeichnisnamen, die sich in dem Ordner befinden.
     */
    std::vector<std::string> alle_ordner(const std::string& pfad);

}
