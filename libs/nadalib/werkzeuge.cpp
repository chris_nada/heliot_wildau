#include "werkzeuge.hpp"

// Plattformabhängige Header
#ifdef __linux__
    #include <sys/stat.h>
    #ifndef __arm__
        #include <cpuid.h>
    #endif
#elif defined(WIN32)
    #include <windows.h>
#endif
//
#include <tinydir.h>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <regex>

void werkzeuge::log(const std::string& nachricht) {
    #ifndef NDEBUG
        std::cout << nachricht << std::endl;
    #else
        (void) nachricht;
    #endif
}

bool werkzeuge::validieren(const std::string& str) {
    static const std::string valide ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
    for (auto c : str) {
        if (valide.find(c) == std::string::npos) {
            std::stringstream ss;
            ss << c;
            log("\tIllegaler Char: " + ss.str());
            return false;
        }
    }
    return true;
}

bool werkzeuge::existiert_datei(const std::string& dateiname) {
    #ifdef __linux__
        struct stat puffer; // <sys/stat.h>
        return (stat (dateiname.c_str(), &puffer) == 0);
    #else
        std::ifstream out(dateiname);
        return out.good();
    #endif
}


bool werkzeuge::existiert_ordner(const std::string& pfad) {
    #ifdef __linux__
        struct stat puffer; // <sys/stat.h>
                // existiert ?                    && ist Ordner ?
        return (stat (pfad.c_str(), &puffer) == 0 && S_ISDIR(puffer.st_mode));
    #else
        DWORD stats = GetFileAttributesA(pfad.c_str());
        if (stats == INVALID_FILE_ATTRIBUTES) return false;
        return (stats & FILE_ATTRIBUTE_DIRECTORY);
    #endif

}

std::vector<std::string> werkzeuge::tokenize(const std::string& text, const char token) {
    std::vector<std::string> teile;
    unsigned long long anfang = 0;
    unsigned long long ende = 0;
    while ((ende = text.find(token, anfang)) != std::string::npos) {
        teile.push_back(text.substr(anfang, ende - anfang));
        anfang = ende;
        anfang++;
    }
    teile.push_back(text.substr(anfang));
    return teile;
}

void werkzeuge::remove_whitespace(std::string& text) {
    text.erase(std::remove(text.begin(), text.end(), ' '), text.end());
}

std::string werkzeuge::get_aus_ini(const std::string& dateiname, const std::string& key) {
    if (key.empty()) return ""; // ungültiger Key

    static std::map<std::string, std::map<std::string, std::string>> cache;
    try { return cache.at(dateiname).at(key); }
    catch (std::exception&) { /* Wert noch nicht gecacht*/ }

    // INI zeilenweise auslesen
    std::ifstream ini(dateiname);
    if (ini.good()) {
        for (std::string zeile; std::getline(ini, zeile);) {
            if (!zeile.empty() && zeile[0] == key[0] && zeile.find('=') != std::string::npos) {
                remove_whitespace(zeile);
                auto tokens = tokenize(zeile, '=');
                if (tokens[0] == key) {
                    std::stringstream ss; // Nur an Logger
                    ss << '\t' << dateiname << ": " << key << '=' << tokens[1];
                    log(ss.str());
                    cache[dateiname][key] = tokens[1];
                    return tokens[1]; // Schlüssel entspricht linker Seite vom `=`
                }
            }
        }
    }
    else {
        std::cerr << '\t' << dateiname << " nicht lesbar." << std::endl;
        return "";
    }
    std::cerr << key << " in " << dateiname << " nicht gefunden." << std::endl;
    return "";
}

size_t werkzeuge::get_index(const std::string& text, char c, size_t n) {
    auto pos = std::find(text.begin(), text.end(), c);
    for (size_t gefunden = 0; gefunden < n; ++gefunden) {
        pos = std::find(pos + 1, text.end(), c);
    }
    return std::distance(text.begin(), pos);
}

std::string werkzeuge::get_nth_token(const std::string& text, char c, size_t n) {
    if (n == 0) return text.substr(0, text.find(';'));
    const size_t i = get_index(text, c, n - 1) + 1;
    return text.substr(i, get_index(text, c, n) - i);
}

// Verwendete Template-Definitionen.
template unsigned short werkzeuge::parse(const std::string& string, unsigned short standard);
template<typename T>
T werkzeuge::parse(const std::string& string, T standard) {
    if (std::is_floating_point<T>::value) {
        try { return std::stold(string); }
        catch (std::exception& e) { return standard; }
    }
    if (std::is_integral<T>::value) {
        try { return std::stoll(string); }
        catch (std::exception& e) { return standard; }
    }
    log(string + " nicht parsbar");
    return standard;
}

void werkzeuge::mkdir(const std::string& verzeichnis) {
    #if defined(WIN32)
        std::string cmd(verzeichnis);
        std::replace(cmd.begin(), cmd.end(), '/', '\\');
        cmd = "mkdir " + cmd;
        system(cmd.c_str());
    #elif defined(__linux__)
        auto result = system(("mkdir -p " + verzeichnis).c_str());
        (void) result;
    #else
        log("mkdir: OS nicht unterstuetzt");
    #endif
}

void werkzeuge::upper(std::string& string) {
    for (size_t i = 0; i < string.size(); ++i) string[i] = std::toupper(string[i]);
}

void werkzeuge::lower(std::string& string) {
    for (size_t i = 0; i < string.size(); ++i) string[i] = std::tolower(string[i]);
}

std::string werkzeuge::hex(unsigned long long int nummer) {
    std::stringstream ss;
    ss << std::hex << nummer;
    return ss.str();
}

std::string werkzeuge::cpu_id() {
    #ifdef __linux__
        #ifndef __arm__
            unsigned int eax, ebx, ecx, edx;
            __get_cpuid(0, &eax, &ebx, &ecx, &edx);
            std::stringstream ss;
            ss << std::hex << edx << ecx;
            return ss.str();
        #endif
    #endif
    return ""; // nicht verfügbar
}

bool werkzeuge::is_valid_ipv4(const std::string& ip) {
    const auto& tokens = tokenize(ip, '.');
    if (tokens.size() == 4) {
        for (const auto& token : tokens) {
            int n = parse<int>(token, -1);
            if (n >= 0 && n <= 255) continue;
            else return false;
        }
        return true;
    }
    return false;
}

std::string werkzeuge::get_dateiinhalt(const std::string& pfad) {
    std::ifstream in(pfad, std::ios::binary);
    if (in.good()) {
        std::string inhalt;
        in.seekg(0, std::ios::end);
        inhalt.reserve(in.tellg());
        in.seekg(0, std::ios::beg);
        inhalt.assign((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
        return inhalt;
    }
    return "";
}

std::vector<std::string> werkzeuge::alle_dateien(const std::string& pfad) {
    std::vector<std::string> liste;
    tinydir_dir dir;
    tinydir_open(&dir, pfad.c_str());
    while (dir.has_next) {
        tinydir_file file;
        tinydir_readfile(&dir, &file);
        if (!file.is_dir) liste.emplace_back(file.name);
        tinydir_next(&dir);
    }
    return liste;
}

std::vector<std::string> werkzeuge::alle_ordner(const std::string& pfad) {
    std::vector<std::string> liste;
    tinydir_dir dir;
    tinydir_open(&dir, pfad.c_str());
    while (dir.has_next) {
        tinydir_file file;
        tinydir_readfile(&dir, &file);
        if (file.is_dir) {
            std::string ordner(file.name);
            if (ordner != "." && ordner != "..") liste.push_back(ordner);
        }
        tinydir_next(&dir);
    }
    return liste;
}

void werkzeuge::replace_all(std::string& text, const std::string& alt, const std::string& neu) {
    text = std::regex_replace(text, std::regex("\\" + alt), neu);
}
