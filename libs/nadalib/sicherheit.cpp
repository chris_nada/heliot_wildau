#include "sicherheit.hpp"
#include "werkzeuge.hpp"

#include <cryptopp/cryptlib.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/modes.h>
#include <cryptopp/sha3.h>
#include <cryptopp/aes.h>
#include <cryptopp/osrng.h>
#include <argon2.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <sstream>

#ifdef WIN32
    #include <windows.h>
#else
    #include <termios.h>
    #include <unistd.h>
#endif

std::string sicherheit::sha3_224(const char* text) {
    std::string digest;
    CryptoPP::SHA3_224 hash;
    CryptoPP::StringSource(text, true,
            new CryptoPP::HashFilter(hash,
            new CryptoPP::HexEncoder(
            new CryptoPP::StringSink(digest)))
    );
    return digest;
}

std::string sicherheit::sha3_512(const char* text) {
    std::string digest;
    CryptoPP::SHA3_512 hash;
    CryptoPP::StringSource(text, true,
            new CryptoPP::HashFilter(hash,
            new CryptoPP::HexEncoder(
            new CryptoPP::StringSink(digest)))
    );
    return digest;
}

bool sicherheit::aes_encrypt(const std::string& daten, const char* key, const std::string& pfad) {
    std::ofstream out(pfad, std::ios::binary);
    if (out.good()) {

        // AES initialisieren
        using namespace CryptoPP;
        AutoSeededRandomPool asrp;
        SecByteBlock aes_key((const byte*) key, AES::MAX_KEYLENGTH);
        SecByteBlock aes_iv(AES::BLOCKSIZE);
        asrp.GenerateBlock(aes_iv, aes_iv.size());
        CFB_Mode<AES>::Encryption aes(aes_key, sizeof(aes_key), aes_iv);

        // Verschlüsseln
        sicherheit::string encrypted;
        StringSource ss(
                daten,
                true,
                new StreamTransformationFilter(aes,
                new StringSinkTemplate<decltype(encrypted)>(encrypted))
        );

        // Schreiben
        for (const auto& c : aes_iv) out << c; // IV
        out << encrypted; // Blöcke
        return true;
    }
    return false;
}

bool sicherheit::aes_decrypt(std::string& daten, const char* key, const std::string& pfad) {

    // Dateinhalt laden
    std::string inhalt(werkzeuge::get_dateiinhalt(pfad));
    if (!inhalt.empty()) {

        // AES initialisieren
        using namespace CryptoPP;
        auto iv = sicherheit::sha3_512(key);
        SecByteBlock aes_key((const byte*) key, AES::MAX_KEYLENGTH);
        SecByteBlock aes_iv(AES::BLOCKSIZE);
        if (inhalt.size() < aes_iv.size()) return false; // Inhalt ungültig / kein IV
        for (size_t i = 0; i < aes_iv.size(); ++i) aes_iv[i] = inhalt[i];

        // Entschlüsseln
        CFB_Mode<AES>::Decryption aes(aes_key, sizeof(aes_key), aes_iv);
        StringSource(
                inhalt.substr(AES::BLOCKSIZE),
                true,
                new StreamTransformationFilter(aes,
                new StringSink(daten))
        );
        return true;
    }
    return false;
}

void sicherheit::console_echo(bool enable) {
    #if defined(_MSC_VER)
        HANDLE handle = GetStdHandle(STD_INPUT_HANDLE);
        DWORD modus;
        GetConsoleMode(handle, &modus);
        if (enable) mode |= ENABLE_ECHO_INPUT;
        else mode &= ~ENABLE_ECHO_INPUT;
        SetConsoleMode(handle, modus);
    #elif defined(__linux__)
        struct termios termios;
        tcgetattr(STDIN_FILENO, &termios);
        if (enable) termios.c_lflag |= ECHO;
        else termios.c_lflag &= ~ECHO;
        auto result = tcsetattr(STDIN_FILENO, TCSANOW, &termios);
        (void) result;
    #else
        (void) enable;
    #endif
}

std::string sicherheit::argon2(const char* text, const char* salt) {

    // Konfiguration
    constexpr unsigned OUT_LEN = 32;        // Byte Länge
    //constexpr unsigned ENCODED_LEN = 108;   // Byte Länge (zeichenkodiert)
    constexpr uint32_t m = 1u << 16u;           // Arbeitsspeicherbedarf
    constexpr uint32_t t = 2;                   // Iterationen
    constexpr uint32_t p = 1;                   // Parallelisierung
    constexpr auto version = ARGON2_VERSION_13; // Version
    constexpr argon2_type type = Argon2_id;     // Argon2 Typ

    // Ausgabe
    unsigned char out[OUT_LEN];
    //char encoded[OUT_LEN];

    // Hashen
    const auto result = argon2_hash(
            t, m, p,
            text, strlen(text),
            salt, strlen(salt),
            out,  OUT_LEN,
            nullptr, 0, type, version
    );
    if (result != ARGON2_OK) std::cerr << "Fehler beim Hashen: Kode " << result << std::endl;

    // C-Hex-Konvertierung aus dem Originalquelltext:
    //unsigned char hex_out[OUT_LEN * 2 + 4];
    //for (uint32_t i = 0; i < OUT_LEN; ++i) sprintf((char *)(hex_out + i * 2), "%02x", out[i]);

    // In Hex konvertieren
    std::stringstream ss;
    ss << std::setw(2) << std::setfill('0') << std::hex << std::uppercase;
    for (auto c : out) ss << (int) c;
    return ss.str();
}
