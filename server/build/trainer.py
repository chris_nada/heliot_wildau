# -*- coding: utf-8 -*-
# Timer
import time

time_start = time.time()

import sys
import os
import configparser
import caffe
import numpy
import pickle
import PIL

"""
Konfiguration
"""
pfad = "data"
datei_lerndaten_txt = pfad + os.sep + "lerndaten.txt"
datei_test_txt = pfad + os.sep + "lerndaten_test.txt"
#datei_labels_txt = pfad + os.sep + "labels.txt" # nicht mehr genutzt
lerndaten_map = {}  # Enthält { <datei1>: <label1>, ... }
datei_lerndaten_map = pfad + os.sep + "cache_lerndaten_map.pickle"
datei_label_v_map = pfad + os.sep + "cache_label_v_map.pickle"
netz_pfad = pfad + os.sep + "net_deploy.prototxt"
net = None
test_limit = 10000 # Anzahl Trainingsdaten zum Vergleich mit 10% Test

"""
Liefert das am weitesten Trainierte Netz aus dem Ordner 'pfad'.
"""


def get_caffemodel():
    _caffemodels = []
    for datei in os.listdir(pfad):
        if ".caffemodel" in datei and os.path.isfile(pfad + os.sep + datei):
            _caffemodels.append(pfad + os.sep + datei)
    if len(_caffemodels) > 0:
        _caffemodels.sort(reverse=True)
        return _caffemodels[0]
    else:
        print("Fehler: Kein trainiertes 'Caffemodel' in " + pfad + " gefunden.")


"""
Lerndaten-Liste für Cafee erstellen
"""


def create_trainingsdatenliste(create_test):  # true = Lerndaten aufteilen in train & test
    config = configparser.ConfigParser()
    config.read("config.ini")
    lerndatenpfad = config["server"]["ORDNER_LERNDATEN"]
    zaehler = 0
    labels = []
    print("Lerndatenpfad =", lerndatenpfad)

    # Output-Dateien erzeugen
    f = open(datei_lerndaten_txt, "w")
    if create_test:
        f_test = open(datei_test_txt, "w")

    # Alle Nutzerordner nach Lerndaten durchforsten
    for root, dirs, files in os.walk(lerndatenpfad + os.sep):
        for datei in files:
            if "_face." in datei: # in datei.lower() and (datei.lower().endswith(".png") or datei.lower().endswith(".jpg"))
                zaehler += 1
                if zaehler > test_limit:
                    break
                datei = root + os.sep + datei
                label = os.path.dirname(datei).split(os.sep)[-1]  # Label entspricht Login-Name
                if label not in labels:
                    labels.append(label)

                if create_test and zaehler % 10 == 0:  # 90/10
                    f_test.write(datei + " " + str(labels.index(label)) + "\n")  # "." + os.sep +
                else:
                    f.write(datei + " " + str(labels.index(label)) + "\n")  # "." + os.sep +
                lerndaten_map[datei] = label

    print("Erzeuge", datei_lerndaten_txt)
    try:
        f.close()
        f_test.close()
    except:
	    pass

    # save_labels(labels)
    with open(datei_lerndaten_map, "wb") as handle:
        pickle.dump(lerndaten_map, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print(datei_lerndaten_txt, "erstellt.")


"""
Cache erzeugen
"""


def create_classifier_cache():
    print("Cache wird erzeugt (datei_label_v_map)...")

    # Lerndaten-Map laden
    with open(datei_lerndaten_map, "rb") as handle:
        lerndaten_map = pickle.load(handle)

    # Netzkonfiguration
    net = caffe.Classifier(netz_pfad, get_caffemodel())

    # Classifier Cache schreiben
    label_v_map = {}
    zaehler = 0
    for datei, label in lerndaten_map.items():
        bild = caffe.io.load_image(datei)
        v2 = net.predict([bild])
        label_v_map[datei] = v2
        zaehler += 1
        print(zaehler, "/", len(lerndaten_map.keys()))

    # Cache schreiben
    with open(datei_label_v_map, "wb") as handle:
        pickle.dump(label_v_map, handle, protocol=pickle.HIGHEST_PROTOCOL)


"""
Gibt erkanntes Label für Datei `dateiname`. Caches müssen erstellt worden sein.
"""


def predict(dateiname, write_file):
    # Netzkonfiguration
    global net
    if net is None:
        net = caffe.Classifier(netz_pfad, get_caffemodel())

    # Ergebnis
    print("Netz =", net)
    best_match = "unbekannt"
    kosinus_score = -1

    # Cache vorhanden
    if os.path.isfile(datei_lerndaten_map) and os.path.isfile(datei_label_v_map):
        with open(datei_lerndaten_map, "rb") as handle:
            lerndaten_map = pickle.load(handle)
        with open(datei_label_v_map, "rb") as handle:
            label_v_map = pickle.load(handle)

        # Vorhersage
        bild = caffe.io.load_image(dateiname)
        v1 = net.predict([bild])

        # Vergleich
        for datei, vektor in label_v_map.items():
            cos_sim = numpy.dot(v1[0], vektor[0]) / (numpy.linalg.norm(v1[0]) * numpy.linalg.norm(vektor[0]))
            if cos_sim > kosinus_score:
                kosinus_score = cos_sim
                best_match = lerndaten_map[datei]

    # Kein Cache vorhaden
    else:
        print("Kein Cache vorhanden. Es muss zuerst trainieren() ausgeführt werden.")

    # Ergebnis schreiben
    print("\tLabel =", best_match, "; Kosinus = ", kosinus_score)
    if write_file:
        txt = open(pfad + os.sep + "temp.txt", "w")  # Auslesbar von externem Programm
        txt.write(best_match)
        txt.close()
    else:
        return [best_match, kosinus_score]  # Ergebnis direkt zurückgeben


"""
Veranschaulicht zu gegebener Datei die Featurematrix in features.png
"""


def features_extrahieren(dateiname):
    # Netzkonfiguration
    net = caffe.Classifier(netz_pfad, get_caffemodel())

    # Vorhersage
    bild = caffe.io.load_image(dateiname)
    v1 = net.predict([bild])
    print("Vorhersage, len() =", len(v1[0]), ", min() =", min(v1[0]), ", max() =", max(v1[0]), "Werte:\n", v1[0])

    # Bild initialisieren
    x, y = 0, 0
    h, w = 4, 40
    v_out = numpy.zeros((h, w, 3), dtype=numpy.uint8)  # 40x4

    # Konvertieren
    for wert in v1[0]:
        v_out[y, x] = max(0, min(wert * 170, 255))
        x += 1
        if x >= w:
            x = 0
            y += 1
    PIL.Image.fromarray(v_out).convert("RGB").save("features.png")


"""
Extrahierte Gesichter automatisch Löschen
"""


def delete_alle(token):  # token = 'jpg', 'dat', 'png' usw...
    print("Alle", token, "werden entfernt...")
    config = configparser.ConfigParser()
    config.read("config.ini")
    lerndatenpfad = config["server"]["ORDNER_LERNDATEN"]
    print("Lerndatenpfad =", lerndatenpfad)
    for root, dirs, files in os.walk(lerndatenpfad + os.sep):
        for datei in files:
            datei = root + os.sep + datei
            if token in datei:  # Ist jpg o.Ä.? Dann Löschen.
                os.remove(datei)


"""
Erstellt eine Statistik für das trainiertes Netz. 
    + Ausgabe: validate.csv.
    + Lerndaten-Map (datei_lerndaten_map) muss erstellt worden sein
    + datei_test_txt muss auf Lerndaten angepasst worden sein
    + nur jpg
"""


def validate():
    print("Trainiertes Netz validieren...")

    # Lerndaten-Map laden
    with open(datei_lerndaten_map, "rb") as handle:
        lerndaten_map = pickle.load(handle)

    # Testdaten laden
    with open(datei_test_txt, "r") as f:
        zeilen = f.readlines()
    print("Anzahl Testdaten =", len(zeilen))

    # Output CSV
    f = open("validate.csv", "w")
    f.write('"Label";"Vorhersage";"Score";"Korrekt"\n')

    # Jede Datei testen
    for i in range(len(zeilen)):
        print(i, "/", len(zeilen))
        zeile = zeilen[i]
        zeile = zeile[0:zeile.index(".jpg") + 4]
        ergebnis = predict(zeile, False)

        # Auswerten
        label = ergebnis[0]
        score = ergebnis[1]
        wahr  = lerndaten_map[zeile]
        erfolg = str(label) == str(wahr)

        # In CSV eintragen
        f.write('"' + wahr + '";"' + label + '";' + str(score) + ";" + str(int(erfolg)) + "\n")
        print("\t", wahr, label, score, erfolg) # Wahr, Vorhersage, Score
    f.close()


"""
Parameter zum Ausführen des Skripts
"""
try:
    if "--predict" in sys.argv:
        predict(dateiname="data/temp.jpg", write_file=True)
    elif "--create_train" in sys.argv:
        create_trainingsdatenliste(False)  # Ohne Test
    elif "--create_test" in sys.argv:
        create_trainingsdatenliste(True)  # Mit Test
    elif "--create_cache" in sys.argv:
        create_classifier_cache()
    elif "--delete_faces" in sys.argv:  # Alle erzeugten _face. Dateien löschen
        delete_alle("_face.")
    elif "--validate" in sys.argv:  # Testdaten müssen vorher erzeugt worden sein + Netz trainiert
        validate()
    elif "--delete_unencrypted" in sys.argv:  # Alle unentschlüsselten .jpg / .png löschen
        delete_alle(".jpg")
        delete_alle(".png")
    elif "--extract" in sys.argv:
        features_extrahieren("data/temp.jpg")
    else:
        print("-------------------------------------------------------------------")
        print("  Parameter    |     Beschreibung")
        print("  --predict       Gibt vorhergesagte Klasse für data/temp.jpg\n"
              "                  in data/temp.txt aus.")
        print("  --create_train  Schreibt alle Lerndaten ins Lerndatenverzeichnis.")
        print("  --create_test   Schreibt Lerndaten ins Lerndaten- und\n"
              "                  Testdatenverzeichnis.")
        print("  --create_cache  Erzeugt einen Cache von Merkmalvektoren aus\n"
              "                  dem Lerndatenverzeichnis.")
        print("  --delete_faces  Lösche alls extrahierten Gesichter\n"
              "                  aus den Lerndaten.")
        print("  --validate      Validierung des trainierten Modells.\n"
              "                  Es muss Test- und Lerndatenverzeichnis erstellt\n"
              "                  wordensein sowie der Merkmalvektoren-Cache.")
        print("  --delete_unencrypted\n"
              "                  Lösche alle nicht verschlüsselten Dateien aus\n"
              "                  den Lerndaten.")
        print("  --extract       Schreibt die Feature-Matrix mit Veranschaulichung\n"
              "                  zu gegebenem Bild (data/temp.jpg).")
        print("-------------------------------------------------------------------")
except:
    pass
vergangen = time.time() - time_start
print(vergangen, "s Python-Aufruf")
