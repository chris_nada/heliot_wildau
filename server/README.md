# Kurzanleitung Gesichtsdetektor

## Ausführen aus der Konsole

`heliot_server.exe bilddatei.jpg` oder `heliot_server.exe bilddatei.png`

Pfade können relativ in Unix-Schreibweise angegeben werden z.B.:
`heliot_server.exe verzeichnis/bilddatei.jpg` oder z.B. 
`heliot_server.exe ../bilddatei.jpg`

## Konfiguration

Standard ist der HOG-Detektor, ein Kompromiss aus Erkennungsrate und Rechenzeit.
```ini
GESICHTSDETEKTOR = HOG
```

In der Datei `config.ini` kann mit Änderung des Parameters `GESICHTSDETEKTOR`
der Algorithmus auf `HAAR` (aus OpenCV), `HOG` (aus DLIB) oder `KNN` (aus DLIB)
eingestellt werden. Im Allgemeinen haben die Algorithmen in der genannten
Reihenfolge aufsteigend höhere Erkennungsraten, dafür langsamere Rechenzeiten 
(genaue Ergebnisse vgl. Thesis)

+ Für **HAAR** lassen sich von den OpenCV-Autoren vortrainierte Detektoren aus
  einer Datei auswählen. Der Pfad wird in `config.ini` von `HAAR_DETEKTOR` bestimmt.
  
+ Der **HOG**-Detektor ist von DLib vortrainiert und hartkodiert.

+ Das vortrainierte **KNN** liegt in `data/mmod_human_face_detector.dat`.
  Soll das Modell ersetzt werden, muss diese Datei ersetzt werden.

Es gelten je nach Algorithmus Mindestgrößen für die Detektion, diese sind
der entsprechenden Dokumentation (OpenCV/DLIB) zu entnehmen.

## Ausgabe

+ Der Detektor gibt ein ausgeschnittenes Gesichtsbild aus, wenn (mindestens) ein Gesicht erkannt wurde.
+ Werden mehr als 1 Gesicht erkannt, wird nur das erste gespeichert.
+ Das Gesicht wird in die Datei `output.jpg` oder `output.png` geschrieben, je nachdem
  welches Format in der `config.ini` als Bildformat angegeben ist. 
  
  Standard:
  
  ```ini
  BILDFORMAT = jpg
  ```
