#include <httplib.h>
#include <werkzeuge.hpp>
#include <opencv2/imgcodecs.hpp>
#include "server.hpp"
#include "einstellungen.hpp"
#include "datenschutz/db_csv.hpp"

heliot::Server::Server(uint16_t PORT, const char* HASH, const char* SALT) :
        Facereco(werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "ORDNER_LERNDATEN"),
                 werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT"),
                 HASH, SALT), PORT(PORT) {

    // Ordner für Lerndaten prüfen / erzeugen
    std::cout << "Server startet..." << std::endl;

    // DB Konfiguration
    const std::string CONFIG        = Einstellungen::CONFIG_INI;
    const std::string DB_TYP        = werkzeuge::get_aus_ini(CONFIG, "DB_TYP");
    const std::string DB_PFAD       = werkzeuge::get_aus_ini(CONFIG, "DB_PFAD");
    const std::string DB_PFAD_DATEN = werkzeuge::get_aus_ini(CONFIG, "DB_PFAD_DATEN");

    // Datenbank je nach Typ initialisieren
    if ("CSV" == DB_TYP) db = new DB_CSV(DB_PFAD_DATEN, DB_PFAD);
    else /* Default */   db = new DB_CSV(DB_PFAD_DATEN, DB_PFAD);
}

heliot::Server::~Server() {
    delete db; // Datenbankverbindung schließen.
}

void heliot::Server::debug(const std::string& titel, const httplib::Request& request) {
#ifndef NDEBUG
    // Request in die Konsole schreiben (falls Debug-Modus)
    std::cout << '\n' << titel << '\n';
    for (const auto& h : request.headers) std::cout << "\tHeader: " << h.first << " = " << h.second << '\n';
    for (const auto& p : request.params) std::cout << "\tParam " << p.first << " = " << p.second << '\n';
    std::cout << "\tPath = " << request.path << '\n';
    std::cout << "\tVersion = " << request.version << '\n';
    std::cout << "\tTarget = " << request.target << '\n';
    if (!request.body.empty()) std::cout << "\tBody = " << request.body.substr(0, 250ul) << "[...]\n";
    std::cout << "\tFiles:\n";
    for (const auto& file : request.files) {
        std::cout << file.first << " = " << file.second.content_type << '\n';
    }
#endif
}

int heliot::Server::start() {
    // SSL Zertifikate vorhanden?
    if (!werkzeuge::existiert_datei(CERT_PEM)) std::cerr << "SSL Zertifikat fehlt: " << CERT_PEM << std::endl;
    if (!werkzeuge::existiert_datei(KEY_PEM))  std::cerr << "SSL Schluessel fehlt: " << KEY_PEM  << std::endl;

    httplib::Server* server;

    // HTTP(S) Server initialisieren
    if (werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "SSL") == "true") {
        std::cout << "SSL wird geladen..." << std::flush;
        server = new httplib::SSLServer(CERT_PEM.c_str(), KEY_PEM.c_str());
        std::cout << " erfolg." << std::endl;
    }
    else server = new httplib::Server();

    // SIGPIPEs ignorieren
    #ifndef _WIN32
        signal(SIGPIPE, SIG_IGN);
    #endif

    // GET info (aufrufbar auch aus dem Browser)
    server->Get("/info", [](const httplib::Request& request, httplib::Response& response) {
        debug("DEBUG GET /info", request);
        response.set_content(
                "Request = GET /existiert;    Format = Parameter 'login'\n"
                    "\tResponse: 200 = Login existiert; 404 = Login existiert nicht\n"
                "Request = PUT /neues_bild;   Format = <NUTZERNAME>;<BILDDATEN>\n"
                    "\tResponse: 200 = Bild erfolgreich hinzugefuegt\n"
                    "\tJe eine Zeile fuer eine Detektion <X>;<Y>;<W>;<H>\n"
                "Request = PUT /registrieren; Format = <NUTZERNAME>;<PSEUDONYM>;<BILDDATEN>\n"
                    "\tResponse: 200 = Nutzer erfolgreich angelegt\n"
                "Request = PUT /recognize;    Format = <BILDDATEN>\n"
                    "\tResponse: <X>;<Y>;<W>;<H>;<PSEUDONYM>\n",
                "text/plain"
        );
    });

    // GET Existiert Nutzer?
    server->Get("/existiert", [&](const httplib::Request& request, httplib::Response& response) {
        debug("DEBUG GET /existiert", request);
        std::string login(request.get_param_value("login"));

        // Regel: 'Ein Nutzer existiert, wenn sein Lerndatenordner existiert'.
        if (!login.empty() && werkzeuge::existiert_ordner(ORDNER_LERNDATEN + "/" + login)) {
            response.set_content("Nutzer <" + login + "> bereits registriert.", "text/plain");
            response.status = 200;
        }
        else {
            response.set_content("Nutzer <" + login + "> nicht registriert.", "text/plain");
            response.status = 404;
        }
    });

    // PUT Nutzerbild hinzufügen
    server->Put("/neues_bild", [&](const httplib::Request& request, httplib::Response& response) {
        debug("DEBUG PUT /neues_bild", request);
        (void) bild_hinzufuegen(request, response);
    });

    // PUT neuen Nutzer hinzufügen
    server->Put("/registrieren", [&](const httplib::Request& request, httplib::Response& response) {
        debug("DEBUG PUT /registrieren", request);
        (void) registrieren(request, response, db);
    });

    // GET recognize
    server->Put("/recognize", [&](const httplib::Request& request, httplib::Response& response) {
        debug("DEBUG PUT /recognize", request);
        (void) recognize(request, response, true);
    });

    // Server starten
    std::cout << "Server gestartet." << std::endl;
    server->listen("0.0.0.0", PORT);
    delete server;
    std::cout << "Server gestoppt." << std::endl;
    return 0;
}

bool heliot::Server::registrieren(const httplib::Request& request, httplib::Response& response, DB* db_ptr) {

    // Wird bei Fehlern angezeigt
    auto invalid = [&]() {
        werkzeuge::log("Request-Body ungueltig oder keine Unterschriftendatei (unterschrift.png) angehaengt.");
        response.set_content("Request-Body ungueltig oder keine Unterschriftendatei (unterschrift.png) angehaengt.",
                             "text/plain");
        response.status = 400;
    };

    // Request-Body gültig?
    if (std::count(request.body.begin(), request.body.end(), ';') >= 2) {

        // Daten auslesen // TODO Testen
        const std::string& name         = werkzeuge::get_nth_token(request.body, ';', 0);
        const std::string& pseudonym    = werkzeuge::get_nth_token(request.body, ';', 1);
        const std::string& unterschrift = request.body.substr(1 + werkzeuge::get_index(request.body, ';', 1));

        // Daten gültig?
        if (name.empty() || pseudonym.empty() || unterschrift.empty()) {
            invalid();
            return false;
        }

        if (werkzeuge::existiert_ordner(ORDNER_LERNDATEN + "/" + pseudonym)) {
            response.set_content("Nutzer existiert bereits.", "text/plain");
            response.status = 409;
            return false;
        }

        // Neuen Nutzer hinzufügen
        if (db_ptr->add_person(name, pseudonym, unterschrift, HASH.c_str())) {

            // Verzeichnis für Bilder erstellen
            werkzeuge::mkdir(ORDNER_LERNDATEN + "/" + pseudonym);

            // Antworten
            werkzeuge::log(pseudonym + " hinzugefuegt.");
            response.set_content("Nutzer hinzugefuegt!", "text/plain");
            response.status = 200;
            return true;
        }
        else {
            // Nutzer möglicherweise schon vorhanden / sonstiger Dateizugriffsfehler
            werkzeuge::log(pseudonym + " konnte nicht hinzugefuegt werden.");
            response.set_content(pseudonym + " konnte nicht hinzugefuegt werden.", "text/plain");
            response.status = 409;
            return false;
        }
    }
    invalid();
    return false;
}

bool heliot::Server::bild_hinzufuegen(const httplib::Request& request, httplib::Response& response) {

    // Request auslesen
    if (request.body.find(';') != std::string::npos) {
        std::string nutzer = request.body.substr(0, request.body.find(';'));

        // Daten vorhanden?
        if (request.body.size() < nutzer.size() + 1) {
            response.status = 400;
            response.set_content("Request enthaelt keine Daten.", "text/plain");
            return false;
        }

        // Daten auslesen
        std::string daten = request.body.substr(request.body.find(';') + 1); // Daten beginnen nach ;
        #ifndef NDEBUG
            if (daten.size() >= 30) {
                std::cout << "\tNutzer = " << nutzer << '\n';
                std::cout << "\tDaten  = " << daten.substr(0, 30) << "[...]\n";
            }
        #endif

        // Ordner ermitteln
        std::string nutzerordner(ORDNER_LERNDATEN + "/" + nutzer + "/");
        if (ORDNER_LERNDATEN.empty()) nutzerordner = nutzer + "/";

        // Zieldatei ermitteln (durchzählen 1.png bis N.png)
        std::string zieldatei;
        for (uint16_t i = 1; i < UINT16_MAX - 1; ++i) { // TODO effizienter machen

            // Erste freie Datei nutzen
            zieldatei = nutzerordner + std::to_string(i) + ".dat";
            if (werkzeuge::existiert_datei(zieldatei)) continue;

            // Schreibbar?
            if (std::ofstream out(zieldatei, std::ios::binary); out.good()) {
                out.close(); // wird von Verschlüsselung verwendet
                sicherheit::aes_encrypt(daten, HASH.c_str(), zieldatei);
                response.status = 200;
                werkzeuge::log("\tPUT neues_bild/ erfolgreich (200).");

                // Gesicht(er) erkennen, zurücksenden
                std::vector<byte> buffer(daten.begin(), daten.end());
                cv::Mat bild(buffer, true);
                bild = cv::imdecode(buffer, cv::IMREAD_COLOR);
                if (bild.empty()) std::cerr << "\tPUT /neues_bild Bild konnte nicht geladen werden.\n";
                else {
                    // Detektionen in Antwort eintragen
                    const auto& detektionen = face_detect(bild);
                    for (const auto& detektion : detektionen) {
                        response.body.append(std::to_string(detektion.x));
                        response.body.append(";");
                        response.body.append(std::to_string(detektion.y));
                        response.body.append(";");
                        response.body.append(std::to_string(detektion.width));
                        response.body.append(";");
                        response.body.append(std::to_string(detektion.height));
                        response.body.append("\n");
                    }
                }
                return true; // Erfolg
            }
            else {
                std::cerr << "\tKein Schreibzugriff auf Pfad = " << zieldatei << std::endl;
                break;
            }
        }
    }
    // Request ungültig: kein ';' gefunden
    else response.set_content("Request ungueltig. Format: <NUTZERNAME>;<BILDDATEN>", "text/plain");

    // Antwort senden
    response.status = 400;
    return false;
}

bool heliot::Server::recognize(
        const httplib::Request& request,
        httplib::Response& response,
        bool recognize) const {

    // Enthält Request Daten?
    if (const auto& pic_data = request.body; !pic_data.empty()) {

        // Bild konvertieren
        std::vector<byte> buffer(pic_data.begin(), pic_data.end());
        cv::Mat bild(buffer, true);
        bild = cv::imdecode(buffer, cv::IMREAD_COLOR);
        if (bild.empty()) std::cerr << "\tPUT /recognize Bild konnte nicht geladen werden.\n";

        // Gesicht(er) erkennen & identifizieren
        auto detektionen = face_detect(bild);
        for (auto& detektion : detektionen) {

            // Detektionen mit Clipping am Rand abschneiden
            if (detektion.x < 0) { // Links
                detektion.width = detektion.width + detektion.x;
                detektion.x = 0;
            }
            if (detektion.y < 0) { // Oben
                detektion.height = detektion.height + detektion.y;
                detektion.y = 0;
            }
            // Rechts, Unten werden von try/catch entfernt

            // Erkennung aufrufen
            std::string label;
            try {
                if (recognize) label = Facereco::recognize(bild(detektion));
                if (label.empty()) label = "Unbekannt";
            } catch (std::exception& e) { continue; }

            response.body.append(std::to_string(detektion.x));
            response.body.append(";");
            response.body.append(std::to_string(detektion.y));
            response.body.append(";");
            response.body.append(std::to_string(detektion.width));
            response.body.append(";");
            response.body.append(std::to_string(detektion.height));
            response.body.append(";");
            response.body.append(label + "\n");
        }
        return !detektionen.empty();
    }
    return false;
}
