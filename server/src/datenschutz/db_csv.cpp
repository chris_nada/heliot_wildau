#include "db_csv.hpp"

#include <werkzeuge.hpp>
#include <sicherheit.hpp>
#include <iostream>
#include <fstream>

DB_CSV::DB_CSV(const std::string& ordner_unterschriften, const std::string& ordner_zuordnungen) :
        ordner_unterschriften(ordner_unterschriften),
        ordner_zuordnungen(ordner_zuordnungen) {

    // Schreibzugriffscheck
    werkzeuge::mkdir(ordner_zuordnungen);
    werkzeuge::mkdir(ordner_unterschriften);

    // Header schreiben, wenn CSV noch nicht vorhanden
    if (std::string csv = ordner_zuordnungen + "/" + std::string(datei_nutzer); !werkzeuge::existiert_datei(csv)) {
        if (std::ofstream out(csv); out.good()) {
            out << "NAME;PSEUDONYM\n";
            std::cout << csv << " erstellt." << std::endl;
        }
        else std::cerr << "Kein Schreibzugriff auf " << csv << std::endl;
    }

    std::cout << "CSV-Datenbank initialisiert.\n";
    std::cout << "\tDIR Aliaszuordnungen = " << (ordner_zuordnungen.empty()    ? "." : ordner_zuordnungen) << '\n';
    std::cout << "\tDIR Unterschriften   = " << (ordner_unterschriften.empty() ? "." : ordner_unterschriften) << '\n';
    std::cout << std::endl;
}

bool DB_CSV::add_person(const std::string& name,
                        const std::string& pseudunym,
                        const std::string& unterschrift,
                        const char* key) {
    // Dateien schreibbar?
    std::string dateiname_unterschrift(sicherheit::sha3_224(name.c_str()) + ".png");
    dateiname_unterschrift = ordner_unterschriften + "/" + dateiname_unterschrift;
    std::ofstream out_img(dateiname_unterschrift);
    std::ofstream out_csv(ordner_zuordnungen + "/" + std::string(datei_nutzer), std::ios::app);

    // Dateien schreiben
    if (out_img.good() && out_csv.good()) {
        out_csv << name << ';' << pseudunym << '\n';
        out_img.close();
        sicherheit::aes_encrypt(unterschrift, key, dateiname_unterschrift);
        return true;
    }
    werkzeuge::log("Kein Dateizugriff in" + ordner_zuordnungen + " oder " + ordner_unterschriften);
    return false;
}
