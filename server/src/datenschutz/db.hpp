#pragma once

#include <string>

/**
 * Basisklasse zum Implementieren verschiedener Datenbasisversionen.
 * Datenbasis meint die Verwaltung von:
 * + Zuordnung Real-Name / Login.
 * + Speicherung von Unterschriften in einem digitalen Bildformat.
 */
class DB {

public:

    /**
     * @brief Für die Registrierung neuer Teilnehmer.
     * Fügt der DB eine neue Person hinzu.
     * @param name Voller Name. (Format (man beachte Leerzeichen): <Vorname> <Nachname>).
     * @param pseudunym Login.
     * @param unterschrift PNG-Daten, die die Nutzereinwilligung über den Datengebrauch enthalten.
     * @param key Key für die verwendete Verschlüsselung.
     * @return War das Anlegen des neuen Nutzers erfolgreich?
     */
    virtual bool add_person(const std::string& hash_name,
                            const std::string& hash_pseudunym,
                            const std::string& unterschrift,
                            const char* key) = 0;

    /// Hat zur Aufgabe, die DB-Verbindung zu schließen / aufzuräumen.
    virtual ~DB() = default;

};
