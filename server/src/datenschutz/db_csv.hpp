#pragma once

#include "db.hpp"

/**
 * Pflegt eine Datenbasis in Form einer CSV Datei.
 * Speicherort der CSV wird im Konstruktor gewählt.
 * Ebenfalls bestimmt wird dort der Ordner, in dem Unterschriften als Bilddaten abgelegt werden.
 * Die CSV enthält als ersten Hash den Realnamen (vorname_nachname), dann den gehashten Pseudonym.
 */
class DB_CSV final : public DB {

public:

    /// Standardkonstruktor.
    DB_CSV() = default;

    /**
     * Konstruktor, der gleichzeitig die verwendeten Pfade
     * (Unterschriftenordner & Ordner mit CSV für Zuordnungen Person - Nutzer) initialisiert.
     * @param ordner_unterschriften Hier werden verschlüsselte Bilddaten der Teilnehmerunterschriften gespeichert.
     * @param ordner_zuordnungen Hier befindet sich die CSV-Datei mit den Zuordnungen Person - Nutzer.
     */
    DB_CSV(const std::string& ordner_unterschriften, const std::string& ordner_zuordnungen);

    /// Destruktor.
    virtual ~DB_CSV() = default;

    /**
     * @brief Registriert im System einen neuen Nutzer.
     * @param name Voller Name. (Format (man beachte Leerzeichen): <Vorname> <Nachname>).
     * @param pseudunym Login.
     * @param unterschrift PNG-Daten, die die Nutzereinwilligung über den Datengebrauch enthalten.
     * @param key Key für die verwendete Verschlüsselung.
     * @return War das Anlegen des neuen Nutzers erfolgreich?
     */
    bool add_person(const std::string& name,
                    const std::string& pseudunym,
                    const std::string& unterschrift,
                    const char* key) override;

protected:

    /// Dateiname der CSV-Datei, die die Zurdnungen Personen - Nutzernamen enthält.
    static constexpr auto datei_nutzer = "nutzer.csv";

    /// Ordner, in dem die verschlüsselten Bilder mit Unterschriften gespeichert werden.
    std::string ordner_unterschriften;

    /// Ordner, in dem sich die CSV befindet, die für die Zuordnung von Personen zu Pseudonymen ist.
    std::string ordner_zuordnungen;

};
