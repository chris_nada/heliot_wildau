#pragma once

#include "einstellungen.hpp"

#include <werkzeuge.hpp>
#include <iostream>

namespace sonstiges {

    /**
     * Alle Bilder, die in den Lerndatenordnern gefunden werden, werden mit der vom
     * Programm verwendeten Verschlüsselung verschlüsselt.
     * Die verschlüsselten Dateien erhalten denselben Dateinamen, jedoch als
     * Dateiendung `dat`.
     * @param hash Gehashtes Masterpasswort.
     */
    void debug_encrypt(const char* hash);

    /**
     * Alle dat-Dateien, die in den Lerndatenordnern gefunden werden, werden mit der vom
     * Programm verwendeten Verschlüsselung entschlüsselt.
     * Die entschlüsselten Dateien erhalten denselben Dateinamen, jedoch als
     * Dateiendung die des spezifizierten Bildformats (siehe `config.ini`).
     * @param hash Gehashtes Masterpasswort.
     */
    void debug_decrypt(const char* hash);

    /**
     * Stellt fest, welches Passwort gesetzt wurde, mit dem die `cache.dat` erstellt wurde.
     * @param pwd Zu verifizierendes Passwort.
     * @return `true`, wenn Passwort korrekt *oder* neu angelegt (in `cache.dat`).
     */
    bool verify_pwd(const char* pwd);

}
