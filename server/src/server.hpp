#pragma once

#include "facereco/facereco.hpp"
#include "datenschutz/db.hpp"

#include <sicherheit.hpp>
#include <iostream>
#include <httplib.h>

namespace heliot {

    /**
     * Server, der einen Webservice zur Gesichtserkennung bereitstellt.
     * @warning Für die SSL Verbindung müssen die Schlüsseldateien
     *          CERT_PEM und KEY_PEM vorhanden sein.
     */
    class Server final : public Facereco {

    public:

        /// Konstruktor, der den HTTP(S)-Port des Servers bestimmt.
        Server(uint16_t PORT, const char* HASH, const char* SALT);

        /// Startet den Server. Diese Methode blockiert, bis der Server beendet wird. Gibt im Normalfall 0 zurück.
        int start();

        /// Destruktor. Kümmert sich ums Aufräumen.
        virtual ~Server();

        /// Maxmiale Länge von Login-Benamungen.
        static constexpr unsigned LOGIN_LAENGE_MAX = 20;

    private:

        /// Gibt ganze Requests in der Konsole aus; nur im Debug-Modus aktiv.
        static void debug(const std::string& titel, const httplib::Request& request);

        /// Bearbeitet Anfragen für das Hinzufügen neuer Bilder. Gibt 200 / 400 als Antwort.
        bool bild_hinzufuegen(const httplib::Request& request, httplib::Response& response);

        /**
         * Bearbeitet Anfragen für das Registrieren neuer Teilnehmer.
         * Syntax: <body>[name];[login];[unterschrift/daten]</body>
         * Gibt 200 bei erfolgreicher Registrierung.
         * Gibt 400 bei sonstigem Fehler.
         * Gibt 409 bei Login bereits vergeben.
         */
        bool registrieren(const httplib::Request& request, httplib::Response& response, DB* db_ptr);

        /**
         * Gibt erkannte Gesichter in einem Bild wieder.
         * Format der Anfrage: <body>[bilddaten]</body>
         * Format der Antwort, [login nur bei `recognize = true`]:
         * <body>
         *      <x1>;<x2>;<y1>;<y2>;<login>\n // Erkanntes Gesicht 1
         *      <x1>;<x2>;<y1>;<y2>;<login>\n // Erkanntes Gesicht 2
         *      ...
         * </body>
         * @return Anfrage erfolgreich bearbeitet?
         */
        bool recognize(const httplib::Request& request, httplib::Response& response, bool recognize) const;

        /// Verwendete Datenbank.
        DB* db;

        /// Server-Port für HTTP(S)-Anfragen.
        const uint16_t PORT;

        /// Verwendetes SSL Zertifikat.
        const std::string CERT_PEM = "./data/cert.pem";

        /// Verwendeter SSL Schlüssel.
        const std::string KEY_PEM = "./data/key.pem";

    };

}
