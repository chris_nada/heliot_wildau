#pragma once

#include <opencv2/objdetect.hpp>
#include <sicherheit.hpp>
#include <opencv2/imgproc.hpp>
#include <dlib/dnn.h>
#include <dlib/image_processing/frontal_face_detector.h>

class Facereco {

public:

    /**
     * Default Konstruktor.
     * @param bildformat Format (als Dateiendung) wie z.B. `jpg` oder `png`.
     * @warning Erlaubt nicht den Betrieb des Servers (Lerndaten, Hash, Salt fehlen).
     */
    Facereco(const std::string& bildformat) :
            ORDNER_LERNDATEN(""), BILDFORMAT(bildformat), HASH(""), SALT("")
    {
        // Konstruktor erlaubt Serverbetrieb nicht.
    }

    /**
     *
     * @param ordner_lerndaten
     * @param bildformat
     */
    Facereco(
            const std::string& ordner_lerndaten, const std::string& bildformat,
            const char* HASH, const char* SALT
            );

    /**
     * Wendet auf alle vorhandenen 'rohen' Bilder eine Gesichtsextraktion an.
     * @param true Sollen erkannte Gesichter gespeichert werden?
     */
    void extraktion(bool schreiben = true);

    /**
     * Liefert über eine Schnittstelle zu Python das vorhergesagte Label zu gegebenem Bild.
     * @param bild Zu untersuchendes Bild.
     * @return Label (Login-Name aus den Lerndaten) als gewöhnlicher String.
     */
    std::string recognize(const cv::Mat& bild) const;

    /**
     * Wendet auf das gegebene Bild (`cv::Mat`) eine Gesichtsdetektion an.
     * @param bild OpenCV-Matrix.
     * @return Liste erkannter Gesichtskoordinaten als `cv::Rect`.
     */
    std::vector<cv::Rect> face_detect(const cv::Mat& bild) const;

protected:

    /// Wurzelverzeichnis, in dem Lerndaten abzuspeichern sind. Ohne abschließendes '/'.
    const std::string ORDNER_LERNDATEN;

    /// Zu verwendenes Dateiformat für Bilder. Standard: 'png'.
    const std::string BILDFORMAT;

    /// HASH zur Nutzung bei Verschlüsselungen.
    const sicherheit::string HASH;

    /// Salt zur Nutzung zusammen mit `HASH`.
    const sicherheit::string SALT;

private:

    /// Neuronales Netz zur Gesichtsdetektion.
    template <long num_filters, typename SUBNET> using con5d = dlib::con<num_filters,5,5,2,2,SUBNET>;
    template <long num_filters, typename SUBNET> using con5  = dlib::con<num_filters,5,5,1,1,SUBNET>;
    template <typename SUBNET> using downsampler  = dlib::relu<dlib::affine<con5d<32, dlib::relu<dlib::affine<con5d<32, dlib::relu<dlib::affine<con5d<16,SUBNET>>>>>>>>>;
    template <typename SUBNET> using rcon5  = dlib::relu<dlib::affine<con5<45,SUBNET>>>;
    using net_type = dlib::loss_mmod<dlib::con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<dlib::input_rgb_image_pyramid<dlib::pyramid_down<6>>>>>>>>;

    /// DLib HOG Gesichtsdetektor.
    using hog_detektor = decltype(dlib::get_frontal_face_detector());

    /// Thread-sicherer Gesichtsdetektor, der HAAR-Cascade nutzt.
    std::vector<cv::Rect> face_detect_haar(const cv::Mat& bild) const;

    /// Thread-sicherer Gesichtsdetektor, der DLibs FHOG nutzt.
    std::vector<cv::Rect> face_detect_fhog(const cv::Mat& bild) const;

    /// Thread-sicherer Gesichtsdetektor, der DLibs KNN nutzt.
    std::vector<cv::Rect> face_detect_knn(const cv::Mat& bild) const;

    /// Haardetektor aus OpenCV zur Gesichtsdetektion.
    mutable cv::CascadeClassifier haar_detektor;

    /// DLibs KNN basierter Gesichtsdetektor.
    mutable net_type knn_detektor;

};
