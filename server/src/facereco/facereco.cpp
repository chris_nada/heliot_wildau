#include "facereco.hpp"
#include "../einstellungen.hpp"

#include <iostream>
#include <sicherheit.hpp>
#include <werkzeuge.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

Facereco::Facereco(const std::string& ordner_lerndaten, const std::string& bildformat,
                   const char* HASH, const char* SALT) :
        ORDNER_LERNDATEN(ordner_lerndaten),
        BILDFORMAT(bildformat),
        HASH(HASH), SALT(SALT) {

    // Lerndatenordner erstellen
    werkzeuge::mkdir(ORDNER_LERNDATEN);

    // Gesichtsdetektor laden
    const std::string face_detector_xml = werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "HAAR_DETEKTOR");
    if (!haar_detektor.load(face_detector_xml)) {
        std::cerr << "\tHAAR Gesichtsdetektor ["  << face_detector_xml << "] konnte nicht geladen werden.\n";
    } else std::cout << "\tHAAR Gesichtsdetektor [" << face_detector_xml << "] geladen.\n";

    // KNN Gesichtsdetektor laden
    const std::string face_detector_dlib = "data/mmod_human_face_detector.dat";
    try {
        dlib::deserialize(face_detector_dlib) >> knn_detektor;
        std::cout << "\tCNN Detektor aus " << face_detector_dlib << " geladen.\n";
    } catch (std::exception& e) {
        std::cerr << "\tCNN Gesichstdetektor konnte nicht geladen werden aus " << face_detector_dlib << ".\n";
    }

    // HOG Gesichtsdetektor
    std::cout << "\tHOG Gesichtsdetektor geladen.\n"; // automatisch von DLib geladen.
}

void Facereco::extraktion(bool schreiben) {

    // Statistik (gesamt)
    std::ofstream out("statistik_extraktion.csv");
    if (!out.good()) {
        std::cerr << "Fehler: Kein Schreibzugriff auf statistik_extraktion.csv\n";
        return; // Abbruch.
    }
    out << "\"Erkannte Gesichter\";\"Bilder\";\"Login\";\"s/Bild\";\"s Gesamt\"\n";
    unsigned int zaehler_nicht_lesbar = 0;

    // 1. erkanntes Gesicht in Datei abspeichern
    auto verarbeiten = [&] (const cv::Mat& bild, const std::string& datei) {

        // Valide Daten?
        if (bild.empty()) {
            std::cerr << "\tBild konnte nicht geladen werden aus " << datei << '\n';
            return false;
        }

        // Gesicht(er) erkennen
        const std::vector<cv::Rect>& detektionen = face_detect(bild);

        // Speichern
        if (detektionen.empty()) return false;
        if (schreiben) cv::imwrite(datei.substr(0, datei.size() - 3) + "_face." + BILDFORMAT, bild(detektionen[0]));
        return true; // Gesicht gefunden (+ gespeichert)
    };

    // Alle Lerndatenordner betrachten
    const std::vector<std::string>& alle_ordner = werkzeuge::alle_ordner(ORDNER_LERNDATEN);
    dlib::mutex mutex;
    std::atomic_uint erledigt = 0;
    //dlib::parallel_for (0, alle_ordner.size(), [&](size_t i) {
    for (size_t i = 0; i < alle_ordner.size(); ++i) {
        const std::string& login = alle_ordner[i];
        std::string ordner(ORDNER_LERNDATEN + '/' + login + "/");
        std::cout << "\tOrdner " << (++erledigt) << '/' << alle_ordner.size() << " Login: " << login << '\n';

        // Statistik
        unsigned int zaehler_gesichter = 0;
        unsigned int zaehler_bilder    = 0; // Nur lesbare Dateien werden gezählt
        const auto timer_start = std::chrono::steady_clock::now();

        // Alle Dateien betrachten
        dlib::directory dlib_dir(ordner);
        std::vector<std::string> dateien;
        for (const auto& dlib_file : dlib_dir.get_files()) dateien.push_back(dlib_file.full_name());
        for (const std::string& datei_dat : dateien) { // werkzeuge::alle_dateien(ordner)
            //std::string datei_dat(ordner + datei); // Vollständiger Pfad

            // Verschlüsselte Datei gefunden (.dat)
            if (datei_dat.find(".dat") != std::string::npos) {
                werkzeuge::log("\tWird bearbeitet: " + datei_dat);
                try {
                    // Entschlüsseln
                    std::string puffer;
                    sicherheit::aes_decrypt(puffer, HASH.c_str(), datei_dat);

                    // Konvertieren
                    std::vector<byte> pic_data(puffer.begin(), puffer.end());
                    cv::Mat bild(pic_data, true);
                    bild = cv::imdecode(bild, cv::IMREAD_COLOR);
                    if (verarbeiten(bild, datei_dat)) zaehler_gesichter++;
                    zaehler_bilder++;
                } catch (std::exception& e) {
                    std::cerr << '\t' << e.what() << std::endl;
                    zaehler_nicht_lesbar++;
                }
            }
            // Reguläres Bild gefunden (muss Dateiendung in config.ini entsprechen)
            else if (datei_dat.find(BILDFORMAT) != std::string::npos) {
                try {
                    cv::Mat bild = cv::imread(datei_dat, cv::IMREAD_COLOR);
                    if (verarbeiten(bild, datei_dat)) zaehler_gesichter++;
                    zaehler_bilder++;
                } catch (std::exception& e) {
                    std::cerr << '\t' << e.what() << std::endl;
                    zaehler_nicht_lesbar++;
                }
            }
        }
        // Statistik schreiben
        const auto timer_ende        = std::chrono::steady_clock::now();
        const double dauer_ms_gesamt = std::chrono::duration_cast<std::chrono::milliseconds>(timer_ende - timer_start).count();
        const double dauer_ms_bild   = dauer_ms_gesamt / (double) zaehler_bilder; // Durschnittliche Dauer pro Bild
        mutex.lock();
        out << zaehler_gesichter << ';' << zaehler_bilder << ";\"" << login << "\";";
        out << (dauer_ms_bild / 1000.0) << ';' << (dauer_ms_gesamt / 1000.0) << std::endl;
        mutex.unlock();
    } //); // parallel_for
    if (zaehler_nicht_lesbar) std::cerr << zaehler_nicht_lesbar << " Dateien waren unlesbar (& nicht in der Statisik).\n";
}

std::string Facereco::recognize(const cv::Mat& bild) const {

    // Mit Python kommunizieren (langsam)
    cv::imwrite("data/temp.jpg", bild);
    if (system("python3 trainer.py --predict") == 0) { // Blockender Aufruf
        std::ifstream in("data/temp.txt");
        if (in.good()) {
            std::stringstream ss;
            ss << in.rdbuf();
            return ss.str();
        }
    }
    return "";
}

std::vector<cv::Rect> Facereco::face_detect(const cv::Mat& bild) const {
    static const std::string detektor = werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "GESICHTSDETEKTOR");
    if      (detektor == "HAAR") return face_detect_haar(bild);
    else if (detektor == "KNN")  return face_detect_knn(bild);
    else if (detektor == "HOG")  return face_detect_fhog(bild);

    // Kein gültiger Wert => default:
    return face_detect_fhog(bild);
}

std::vector<cv::Rect> Facereco::face_detect_haar(const cv::Mat& bild) const {

    // Setup
    cv::Mat bild_sw;
    std::vector<cv::Rect> detektionen;

    try {
        cv::cvtColor(bild, bild_sw, cv::COLOR_BGR2GRAY);
        cv::equalizeHist(bild_sw, bild_sw);
        static int min_size = 60;
        try { min_size = std::stoi(werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "MIN_SIZE")); }
        catch (std::exception& e) { /* */ }

        // Gesicht erkennen
        haar_detektor.detectMultiScale(
                bild_sw, detektionen,
                1.3, 5, 0u | cv::CASCADE_SCALE_IMAGE,
                cv::Size(min_size, min_size)  // min-Größe (quadratisch)
                //cv::Size(400, 400) // max-Größe
        );
    } catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    return detektionen;
}

std::vector<cv::Rect> Facereco::face_detect_knn(const cv::Mat& bild) const {
    std::vector<cv::Rect> detektionen;

    // Bild konvertieren
    const dlib::cv_image<dlib::bgr_pixel> temp(bild);
    dlib::matrix<dlib::rgb_pixel> matrix;
    dlib::assign_image(matrix, temp);

    // Erkennung durchführen
    std::vector<dlib::mmod_rect> dlib_detektionen;
    #ifdef NDEBUG // Nur im Release-Modus aktiviert
        dlib_detektionen = knn_detektor(matrix);
    #endif

    // DLib rects zu CV rects konvertieren
    for (const dlib::mmod_rect& mmod_r : dlib_detektionen) {
        const auto& r = mmod_r.rect;
        detektionen.emplace_back(r.left(), r.top(), r.width(), r.height());
    }
    return detektionen;
}

std::vector<cv::Rect> Facereco::face_detect_fhog(const cv::Mat& bild) const {
    static hog_detektor detektor = dlib::get_frontal_face_detector();
    static dlib::mutex mutex;
    std::vector<cv::Rect> detektionen;
    try {
        // Konvertieren + HOG Detektion ausführen
        const dlib::cv_image<dlib::bgr_pixel> temp(bild); // cv::Mat für dlib lesbar machen
        mutex.lock();
        const std::vector<dlib::rectangle> dlib_rects = detektor(temp);
        mutex.unlock();
        for (const auto& r : dlib_rects) {
            detektionen.emplace_back(r.left(), r.top(), r.width(), r.height());
        }
    } catch (std::exception& e) { std::cerr << e.what() << std::endl; }
    return detektionen;
}
