#pragma once

/**
 * Einstellungen.
 * Dient zum auslagern globaler Variablen.
 */
class Einstellungen final {

public:

    /// Pfad zur Konfigurationdatei.
    static constexpr auto CONFIG_INI = "config.ini";

};
