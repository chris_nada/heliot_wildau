#ifdef _WIN32
    #include <winsock2.h> // Windows Dreck
#endif

#include <memory>
#include <werkzeuge.hpp>
#include <sicherheit.hpp>
#include <opencv2/imgcodecs.hpp>
#include "server.hpp"
#include "einstellungen.hpp"
#include "sonstiges.hpp"

/**
 * Zeigt eine Übersicht möglicher Konsolenbefehle an.
 * @return Wurde Hilfe angezeigt?
 */
bool hilfe(int argc, char** argv) {

    // Hilfe der Konsolenkommandos.
    for (int i = 1; i < argc; ++i) {
        if (strncmp(argv[i], "--h", 3) == 0 || strncmp(argv[i], "--help", 6) == 0) {
            std::cout << "\n======================================="
                         "\n Zusammenfassung der Befehlsparameter: "
                         "\n=======================================\n"
                         "\n  <diese_anwendung> <dateiname.dat>   Entschluesselt eine einzelne Datei"
                         "\n                                      und legt sie in dieses Verzeichnis ab"
                         "\n                                      unter dem Namen 'ausgabe'.\n"
                         "\n  <diese_anwendung> <input.jpg>       Wendet auf gegebene Bilddatei die in"
                         "\n  <diese_anwendung> <input.png>       der Datei 'config.ini' festgelegte"
                         "\n                                      Gesichtsdetektion an, schneidet"
                         "\n                                      das erste gefundene Gesicht aus und speichert"
                         "\n                                      es als 'output.jpg'.\n"
                         "\n  Operationen fuer die Serveradministration:"
                         "\n  Option  | Operation"
                         "\n  --------|----------------------------------------------------------"
                         "\n  --help"
                         "\n  --h       Zeigt diese Hilfe an."
                         "\n  --start   Startet den Server."
                         "\n  --dd      Debug-Decrypt-Modus: Entschluesselt alle"
                         "\n            Lerndaten zur Inspektion in ihren Ordnern."
                         "\n            Die verschluesselten Daten werden dabei nicht geloescht."
                         "\n  --de      Debug-Encrypt-Modus: Verschluesselt alle in"
                         "\n            Lerndatenverzeichnissen gefundene Bilder."
                         "\n            Die urspruenglichen Bilder werden dabei nicht geloescht."
                         "\n  --train   Bereitet die Lerndaten zum Training vor."
                         "\n            'Vorbereiten' bedeutet hier Gesichts-"
                         "\n            extraktion (und Entschluesselung, wenn noetig)."
                         "\n            Fuehrt das Training selbst nicht durch."
                         "\n  --test    Testet den in der config.ini eingestellten"
                         "\n            Gesichtsdetektor und schreibt die Ergebnisse"
                         "\n            in statistik_extraktion.csv."
                         "\n            "
                         "\n            ";
            std::cout << std::endl;
            return true;
        }
    }
    return false;
}

/**
 * Führt aus der Kommandozeile eine einzelne Gesichtsdetektion durch.
 * @return Wurde Gesichtsdetektion ausgeführt?
 */
bool detect_face(int argc, char** argv) {
    // Dateiname für Bild gegeben?
    for (int i = 1; i < argc; ++i) {
        std::string arg(argv[i]);
        werkzeuge::lower(arg);
        if (arg.find(".jpg") != std::string::npos || arg.find(".png") != std::string::npos) {
            std::cout << "Gesichtsdetektion wird durchgefuehrt an " << arg << "...\n";
            cv::Mat orig = cv::imread(arg);

            // Bild geladen und gültig?
            if (!orig.empty()) {
                const std::string BILDFORMAT = werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT");
                Facereco detektor(BILDFORMAT);
                const std::vector<cv::Rect>& gesichter = detektor.face_detect(orig);
                std::cout << gesichter.size() << " Gesicht(er) gefunden.\n";

                // 1. Gesicht schreiben
                if (!gesichter.empty()) cv::imwrite("output.jpg", orig(gesichter[0]));
            }
            else std::cerr << "FEHLER: OpenCV konnte die Datei nicht lesen.\n";
            return true;
        }
    }
    return false;
}

int main(int argc, char** argv) {

    // Hilfe
    if (hilfe(argc, argv)) return 0;
    std::cout << "INFO: --h oder --help zeigt eine Befehlsuebersicht an" << '\n';

    // Gesichtsdetektion?
    if (detect_face(argc, argv)) return 0;

    // Server-Init überspringen für Projekt 2
    //return 0; // TODO entfernen um Server zu reaktivieren
    std::unique_ptr<heliot::Server> server;
    {
        // Pass abfragen
        sicherheit::console_echo(false);
        sicherheit::string pwd_hash;
        std::cout << (werkzeuge::existiert_datei("cache.dat") ? "Passwort>>" : "Neues Passwort>>") << std::flush;
        std::cin >> pwd_hash;
        sicherheit::string pwd_salt(sicherheit::sha3_512("heliot"));
        pwd_hash = sicherheit::argon2(sicherheit::sha3_512(pwd_hash.c_str()).c_str(), pwd_salt.c_str());
        sicherheit::console_echo(true);

        // Passwort verifizieren
        if (!sonstiges::verify_pwd(sicherheit::argon2(pwd_hash.c_str(), pwd_salt.c_str()).c_str())) {
            std::cout << "Falsches Passwort.\n"
                         "Passwort kann zurueckgesetzt werden durch loeschen von cache.dat "
                         "(alle derzeit verschluesselten Daten waeren dadurch verloren.)\n";
            return 0;
        }

        // Konfiguration auslesen
        const std::string CONFIG = Einstellungen::CONFIG_INI;
        const std::string PORT = werkzeuge::get_aus_ini(CONFIG, "PORT");

        // Port parsen
        const uint16_t PORT_INT = werkzeuge::parse<uint16_t>(PORT, 8787);
        werkzeuge::log("PORT_INT = " + std::to_string(PORT_INT));

        // Parameter verarbeiten
        for (int i = 1; i < argc; ++i) {

            // Server starten
            if (strncmp(argv[i], "--start", 7) == 0) {
                server = std::make_unique<heliot::Server>(PORT_INT, pwd_hash.c_str(), pwd_salt.c_str());
            }

            // Debug-Decrypt-Modus: Entschlüsselt alle Lerndaten zur Inspektion.
            if (strncmp(argv[i], "--dd", 4) == 0) {
                sonstiges::debug_decrypt(pwd_hash.c_str());
                return 0;
            }

            // Debug-Encrypt-Modus: Verschlüsselt alle in Lerndatenverzeichnissen gefundene Bilder.
            if (strncmp(argv[i], "--de", 4) == 0) {
                sonstiges::debug_encrypt(pwd_hash.c_str());
                return 0;
            }

            // Bereitet das Training vor.
            if (strncmp(argv[i], "--train", 7) == 0) {
                server->extraktion(true);
                return 0;
            }

            // Testet den eingestellten Gesichtsdetektor
            if (strncmp(argv[i], "--test", 6) == 0) {
                server->extraktion(false);
                return 0;
            }

            // Entschlüsselt eine einzelne Datei.
            const std::string arg(argv[i]);
            if (arg.find(".dat") != std::string::npos) {
                std::string inhalt;
                sicherheit::aes_decrypt(inhalt, pwd_hash.c_str(), arg);
                if (inhalt.empty()) {
                    std::cerr << "Datei " << arg << " nicht lesbar." << std::endl;
                    return 0;
                }
                const std::string& dateiendung = werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT");
                if (std::ofstream out("ausgabe." + dateiendung, std::ios::binary); out.good()) out << inhalt;
                else std::cerr << "Unzureichende Schreibrechte." << std::endl;
                return 0;
            }
        }
    }
    // Server starten
    if (server) return server->start();
}
