#include "sonstiges.hpp"

#include <sicherheit.hpp>
#include <fstream>

void sonstiges::debug_encrypt(const char* hash) {
    std::string ordner_lerndaten(werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "ORDNER_LERNDATEN"));
    std::string bildformat("." + werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT"));

    // Alle Lerndatenordner betrachten
    for (const auto& login : werkzeuge::alle_ordner(ordner_lerndaten)) {
        std::string ordner(ordner_lerndaten + '/' + login + "/");

        // Alle Dateien (BILDFORMAT aus config.ini) betrachten
        for (auto& bilddatei: werkzeuge::alle_dateien(ordner)) {
            if (bilddatei.find(bildformat) != std::string::npos) {
                bilddatei = ordner + bilddatei;
                std::cout << "Wird verarbeitet: " << bilddatei << std::endl;

                // Auslesen
                std::string puffer = werkzeuge::get_dateiinhalt(bilddatei);
                if (!puffer.empty()) {

                    // Verschlüsseln; Neuer Dateiname = [alter Dateiname].dat
                    std::string bild_dat = bilddatei.substr(0, bilddatei.size() - 4) + ".dat";
                    sicherheit::aes_encrypt(puffer, hash, bild_dat);
                }
                else std::cerr << "\tKonnte " << bilddatei << " nicht lesen.\n";
            }
        }
    }
}

void sonstiges::debug_decrypt(const char* hash) {
    std::string ordner_lerndaten(werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "ORDNER_LERNDATEN"));
    std::string bildformat("." + werkzeuge::get_aus_ini(Einstellungen::CONFIG_INI, "BILDFORMAT"));

    // Alle Lerndatenordner betrachten
    for (const auto& login : werkzeuge::alle_ordner(ordner_lerndaten)) {
        std::string ordner(ordner_lerndaten + '/' + login + "/");

        // Alle Dateien (.dat) betrachten
        for (auto& datei_dat: werkzeuge::alle_dateien(ordner)) {
            if (datei_dat.find(".dat") != std::string::npos) {
                datei_dat = ordner + datei_dat;
                std::cout << "Wird verarbeitet: " << datei_dat << std::endl;

                // Entschlüsseln
                std::string puffer;
                sicherheit::aes_decrypt(puffer, hash, datei_dat);
                std::ofstream out(datei_dat.substr(0, datei_dat.size() - 4) + bildformat, std::ios::binary);
                out << puffer;
            }
        }
    }
}

bool sonstiges::verify_pwd(const char* pwd) {
    const std::string pwd_datei("cache.dat");
    sicherheit::string pwd_sha3_512(sicherheit::sha3_512(pwd));

    // Passwort anlegen?
    if (!werkzeuge::existiert_datei(pwd_datei)) {
        sicherheit::aes_encrypt(pwd, pwd_sha3_512.c_str(), pwd_datei);
        std::cout << "Passwort gespeichert." << '\n';
        return true;
    }

    // Passwort checken
    std::string puffer;
    sicherheit::aes_decrypt(puffer, pwd_sha3_512.c_str(), pwd_datei);
    return (strcmp(pwd, puffer.c_str()) == 0);
}
