# Hardwareanforderungen

*(Die Links dienen lediglich als Referenz, der Autor ist mit den Firmen in keiner Weise affiliiert.)*

+ **Beliebiger Raspberry Pi 1, 2 oder 3**<br/>
  Wenn aber eine kabellose Netzwerkverbindung gewünscht ist, wird Modell 3 oder 3+ benötigt. 
  Neuere Modelle sind außerdem rechenstärker. Preislich bestehen keine gravierenden Unterschiede.

+ **Omnivision 5647 Kamera**<br/>
  Der Roboter Pepper verwendet die Omnivision 5640. Die 5647 hat sehr ähnliche Spezifikationen und ist für Raspberry Pi erhältlich.
  
+ **Touchscreen**<br/>
  Da Nutzerinteraktion in Form von Bildanzeige und eine Oberfläche mit Unterschrifteingabe erforderlich ist, 
  eignet sich ein größeres Modell besser. Falls der Aufbau über Akku betrieben wird, 
  hat der Display (negativen) großen Einfluss auf den Gesamtstromverbrauch des Aufbaus.

+ **Touchpen**(Optional)<br/>
  Für nutzerfreundlicheres Bedienen. Muss mit dem Display kompatibel sein (Stichwort resistiv / kapazitiv).

+ **MicroSD Karte mit 4GB (oder mehr)**<br/>
  Für das Betriebssystem Raspbian werden etwa 3GB benötigt. Mit benötigten Bibliotheken / Software ist man bei 4GB oder mehr.

+ **Tastatur**<br/>
  Das Betriebssystem _Raspbian_ enthält keine so leicht bedienbare Bildschirmtastatur, die mit der aus dem Android-Betriebssystem für Smartphones vergleichbar ist.
  Eine physische USB-Tastatur ist daher nützlich.

+ **Gehäuse**<br/>
  Da der Raspberry Pi im Rahmen der Studie im öffentlchen Versuchsaufbau zur Verfügung gestellt wird, bietet es sich an,
  die offen ausgelieferten Geräte (Raspberry Pi, Display) mit Gehäusen zu versehen.

# Ordnerübersicht

+ `klient_arm` beinhaltet das Kodeprojekt für die ARM-Version des Gesichtserkennungsklienten. Zuständig ist dieser für:
    + Die Aufnahme von Bildern via Videokamera.
    + Die Nutzerinteraktion, d.hoehe. a) Anmeldung neuer Versuchsteilnehmer per UI mit Touchscreen und b) der Anzeige von Erkennungsergebnissen.
    + Die Weiterleitung der aufgenommenen Daten a) bei Anmeldung an den DS-Server (Stichwort Datenschutz) und die Weiterleitung von Bilddaten zur Erkennung an den Reco-Server zur (Wieder-) Erkennung.

+ `klient_avr` beinhaltet den Quelltext und Planschemata für einen AVR-Chip basierten Klienten. Aufgrund der Resourcenbeschränkungen diese Plattform kann dieser Klient nur an den Server per Kamera aufgenommene Bilddaten senden und Erkennungsergebnisse als Text darstellen. *Umsetzung momentan fraglich.*

+ `server_ds` enthält den Quelltext des Datenschutz-Servers, der zur Aufgabe hat, durch Unterschrift ausgedrückte Einverständniserklärungen zu speichern zusammen mit dem Realnamen des Teilnehmers und Zuordnung zum im Reco-Server verwendeten Pseudonym des Nutzers. Eine Schnittstelle besteht zum Zugriff durch den ARM-Klienten.

+ `server_reco` enthält den Quelltext des Server-basierten Gesichtserkennungsalgorithmus. Gespeichert werden Pseudonymen zugeordnete Bilddaten.

+ Im `thesis` Ordner befindet sich der Latex-Quelltext der Masterthesis des Projekts.


# Verwendete Bibliotheken

+ Crypto++ https://cryptopp.com/
+ OpenCV https://opencv.org/
+ FLTK https://fltk.org/
+ Caffe https://caffe.berkeleyvision.org/
+ DLib https://dlib.net/